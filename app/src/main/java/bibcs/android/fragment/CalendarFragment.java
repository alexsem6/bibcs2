package bibcs.android.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.alexsem.android.widget.OneViewFlipper;
import org.alexsem.android.widget.OneViewFlipper.OnViewChangeListener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Gallery;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import bibcs.android.activity.R;
import bibcs.android.adapter.CalendarMonthAdapter;
import bibcs.android.model.CalendarEntry;
import bibcs.android.model.CalendarItem;
import bibcs.android.model.DailyReading;
import bibcs.android.transfer.FileOperations;
import bibcs.android.transfer.FileOperations.Callback;
import bibcs.android.transfer.ServerConnector;
import bibcs.android.util.CalendarCalculation;

/**
 * Fragment used to display calendar data
 * @author Semeniuk A.D.
 */
public class CalendarFragment extends Fragment {

  /**
   * Instantiate fragment
   * @param storagePath Path to SD storage
   * @return new CalendarFragment instance
   */
  public static CalendarFragment newInstance(String storagePath) {
    CalendarFragment f = new CalendarFragment();
    Bundle args = new Bundle();
    args.putString("storage", storagePath);
    f.setArguments(args);
    return f;
  }

  /**
   * Interface used for communication with Activity
   * @author Semeniuk A.D.
   */
  public interface OnCalendarListener {
    public void onSingleLink(String link);

    public void onMultipleLinks(List<String> links);
  }

  private final int CALENDAR_YEAR_MIN = 2000;
  private final int CALENDAR_YEAR_MAX = 2100;
  private final MovementMethod MM_LINK = LinkMovementMethod.getInstance();

  private LayoutInflater mInflater;

  private Animation mAnimFromRight;
  private Animation mAnimFromLeft;
  private Animation mAnimToRight;
  private Animation mAnimToLeft;

  private View mYearProgressBar;
  private OneViewFlipper mYearFlipper;
  private ImageView mYearPrev;
  private ImageView mYearNext;
  private View mYearNotFound;
  private TextView mYearAbsent;
  private View mYearData;
  private TableLayout mGrid;
  private Gallery mMonthSelector;
  private View mDayProgressBar;
  private ScrollView mDayScroll;
  private HorizontalScrollView mDayTitleScroll;
  private TextView mDayTitle;
  private TextView mDayFasting;
  private LinearLayout mDayReadingsOld;
  private LinearLayout mDayReadingsApostle;
  private LinearLayout mDayReadingsGospel;
  private TextView mDayReadingsOldAll;
  private TextView mDayReadingsApostleAll;
  private TextView mDayReadingsGospelAll;

  private ViewGroup[][] mGridCells = new ViewGroup[6][7];
  private ViewGroup mCurrentGridCell = null;
  private List<String> mYearInfo;
  private int mCurrentMonth = -1;
  private int mCurrentDay = -1;

  private OnCalendarListener mListener = null;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View content = inflater.inflate(R.layout.calendar, container, false);
    mInflater = inflater;

    //--- Touch listener to prevent weird presses ---
    content.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
      }
    });

    //--- Regular animations ---
    mAnimFromRight = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_from_right);
    mAnimFromLeft = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_from_left);
    mAnimToRight = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_to_right);
    mAnimToLeft = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_to_left);

    //--- Calendar main data ---
    mYearProgressBar = content.findViewById(R.id.calendar_year_loader);
    mYearFlipper = (OneViewFlipper) content.findViewById(R.id.calendar_year);
    mYearFlipper.setPrevOldAnimation(mAnimToRight);
    mYearFlipper.setPrevNewAnimation(mAnimFromLeft);
    mYearFlipper.setNextOldAnimation(mAnimToLeft);
    mYearFlipper.setNextNewAnimation(mAnimFromRight);
    mYearFlipper.setOnViewChangeListener(mYearFlipperListener);

    return content;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mYearFlipper.setData(CALENDAR_YEAR_MAX - CALENDAR_YEAR_MIN, Calendar.getInstance().get(Calendar.YEAR) - CALENDAR_YEAR_MIN);
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    if (mMonthSelector != null) {
      if (mMonthSelector.getSelectedItemPosition() > 0) {
        setCalendarMonthSelection(mMonthSelector.getSelectedItemPosition() + 12);
      }
      if (mMonthSelector.getSelectedView() != null) {
        mMonthSelector.postDelayed(new Runnable() {
          public void run() {
            mMonthSelector.getSelectedView().setSelected(true);
          }
        }, 1000);
      }
    }
  }

  public void setOnCalendarListener(OnCalendarListener listener) {
    this.mListener = listener;
  }

  /**
   * Selects specific month in month selector
   * @param position Position of new selected item
   */
  private void setCalendarMonthSelection(int position) {
    mMonthSelector.setSelection(position);
    mMonthSelector.postDelayed(new Runnable() {
      public void run() {
        View v = mMonthSelector.getSelectedView();
        if (v != null) {
          v.setSelected(true);
        }
      }
    }, 0);
  }

  /**
   * Selects specified cell in calendar grid
   * @param selection Cell to select
   */
  private void setCalendarGridSelection(ViewGroup selection) {
    if (selection.equals(mCurrentGridCell)) { //Clicked the same cell
      return;
    }
    if (mCurrentGridCell != null) {
      TextView text = ((TextView) mCurrentGridCell.getChildAt(1));
      CalendarItem item = (CalendarItem) mCurrentGridCell.getTag();
      switch (item.getType()) {
        case REGULAR:
          text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular);
          mCurrentGridCell.getBackground().setLevel(/*item.getFasting() == 0 || item.getFasting() == 8 ? */0/* : 2*/);
          break;
        case NEXT:
        case PREVIOUS:
          text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular_Other);
          mCurrentGridCell.getBackground().setLevel(0);
          break;
        case HOLIDAY:
          text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular_Holiday);
          mCurrentGridCell.getBackground().setLevel(1);
          break;
        default:
          break;
      }
    }
    TextView text = ((TextView) selection.getChildAt(1));
    text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular_Selected);
    selection.getBackground().setLevel(3);
    selection.setEnabled(true);
    mCurrentGridCell = selection;
    new DayLoaderTask().execute(mYearFlipper.getDisplayedChild() + CALENDAR_YEAR_MIN, mMonthSelector.getSelectedItemPosition() % 12, Integer.valueOf(((CalendarItem) mCurrentGridCell.getTag()).getValue()));
  }

  /**
   * Inflates and adds daily reading data to specified table
   * @param layout  TableLayout in which new reading should be displayed
   * @param reading Reading to add
   */
  private void addCalendarDailyReading(LinearLayout layout, DailyReading reading) {
    View row = mInflater.inflate(R.layout.calendar_day_reading, null);
    TextView link = (TextView) row.findViewById(R.id.calendar_day_reading_link);
    SpannableString s = new SpannableString(reading.getLink());
    s.setSpan(mLinkClickableSpan, 0, s.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
    link.setText(s);
    link.setMovementMethod(MM_LINK);
    if (reading.getComment().length() > 0) {
      TextView comment = (TextView) row.findViewById(R.id.calendar_day_reading_comment);
      comment.setText(String.format("(%s)", reading.getComment()));
    }
    layout.addView(row);
  }

  /**
   * Format "All" text of calendar readings as ClickableSpan
   * @param type Type of readings (0 - Old, 1 - Apostle, 2 - Gospel)
   * @param text TextView to populate
   */
  private void transformCalendarReadingsAllTitle(int type, TextView text) {
    SpannableString s = new SpannableString(text.getText());
    s.setSpan(new ClickableSpan() {
      @SuppressWarnings("unchecked")
      @Override
      public void onClick(View view) {
        if (view.getTag() != null && mListener != null) {
          List<DailyReading> readings = (List<DailyReading>) view.getTag();
          List<String> links = new ArrayList<String>();
          for (DailyReading reading : readings) {
            links.add(reading.getLink());
          }
          mListener.onMultipleLinks(links);
        }
      }
    }, 0, s.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
    text.setText(s);
    text.setMovementMethod(MM_LINK);
  }

  //-------------------------------------------------------------------------------------------------------

  /**
   * Listener for chapter flipper view change
   */
  private OnViewChangeListener mYearFlipperListener = new OnViewChangeListener() {
    @Override
    public View viewChanged(Integer index) {
      final int year = index + CALENDAR_YEAR_MIN;
      View vCalendarYear = mInflater.inflate(R.layout.calendar_year, null);
      ((TextView) vCalendarYear.findViewById(R.id.calendar_year_title)).setText(String.valueOf(year));
      mYearPrev = (ImageView) vCalendarYear.findViewById(R.id.calendar_year_prev);
      mYearPrev.setOnClickListener(new OnClickListener() { //"Previous" button
        @Override
        public void onClick(View v) {
          mYearFlipper.showPrevious();
        }
      });
      mYearNext = (ImageView) vCalendarYear.findViewById(R.id.calendar_year_next);
      mYearNext.setOnClickListener(new OnClickListener() { //"Next" button
        @Override
        public void onClick(View v) {
          mYearFlipper.showNext();
        }
      });
      mYearNotFound = vCalendarYear.findViewById(R.id.calendar_year_notfound);
      ((TextView) vCalendarYear.findViewById(R.id.calendar_year_notfound_text)).setText(String.format(getString(R.string.calendar_year_notfound), year));
      vCalendarYear.findViewById(R.id.calendar_year_download).setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          new DownloadTask().execute(year);
        }
      });
      mYearAbsent = (TextView) vCalendarYear.findViewById(R.id.calendar_year_absent);
      mYearAbsent.setText(String.format(getString(R.string.calendar_year_absent), year));
      mYearData = vCalendarYear.findViewById(R.id.calendar_year_data);
      mGrid = (TableLayout) vCalendarYear.findViewById(R.id.calendar_grid);
      for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 7; j++) {
          mGridCells[i][j] = (ViewGroup) ((TableRow) mGrid.getChildAt(i + 1)).getChildAt(j);
          mGridCells[i][j].setOnClickListener(mGridCellClickListener);
        }
      }
      mMonthSelector = (Gallery) vCalendarYear.findViewById(R.id.calendar_month);
      mMonthSelector.setAdapter(new CalendarMonthAdapter(getActivity(), getResources().getStringArray(R.array.calendar_month)));
      mMonthSelector.setCallbackDuringFling(false);
      mMonthSelector.setOnItemSelectedListener(mMonthSelectorListener);
      mDayProgressBar = vCalendarYear.findViewById(R.id.calendar_day_loader);
      mDayScroll = (ScrollView) vCalendarYear.findViewById(R.id.calendar_day_scroll);
      mDayTitleScroll = (HorizontalScrollView) vCalendarYear.findViewById(R.id.calendar_day_title_scroll);
      mDayTitle = (TextView) vCalendarYear.findViewById(R.id.calendar_day_title);
      mDayFasting = (TextView) vCalendarYear.findViewById(R.id.calendar_day_fasting);
      mDayReadingsOld = (LinearLayout) vCalendarYear.findViewById(R.id.calendar_day_readings_old);
      mDayReadingsApostle = (LinearLayout) vCalendarYear.findViewById(R.id.calendar_day_readings_apostle);
      mDayReadingsGospel = (LinearLayout) vCalendarYear.findViewById(R.id.calendar_day_readings_gospel);
      mDayReadingsOldAll = (TextView) vCalendarYear.findViewById(R.id.calendar_day_readings_old_all);
      transformCalendarReadingsAllTitle(0, mDayReadingsOldAll);
      mDayReadingsApostleAll = (TextView) vCalendarYear.findViewById(R.id.calendar_day_readings_apostle_all);
      transformCalendarReadingsAllTitle(1, mDayReadingsApostleAll);
      mDayReadingsGospelAll = (TextView) vCalendarYear.findViewById(R.id.calendar_day_readings_gospel_all);
      transformCalendarReadingsAllTitle(2, mDayReadingsGospelAll);
      mMonthSelector.postDelayed(new Runnable() {
        public void run() {
          new YearLoaderTask().execute(year);
        }
      }, 0);
      return vCalendarYear;
    }
  };

  /**
   * Listener for month selection in Calendar gallery
   */
  private OnItemSelectedListener mMonthSelectorListener = new OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
      int year = mYearFlipper.getDisplayedChild() + CALENDAR_YEAR_MIN;
      int month = position % 12;
      Calendar now = Calendar.getInstance();
      mCurrentMonth = month;
      List<CalendarItem> items = CalendarCalculation.getCalendarList(year, month, mYearInfo.get(month));
      mCurrentGridCell = null;
      mCurrentDay = CalendarCalculation.fixCurrentDay(year, month, mCurrentDay);
      for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 7; j++) {
          CalendarItem item = items.get(i * 7 + j);
          ViewGroup cell = mGridCells[i][j];
          ImageView image = ((ImageView) cell.getChildAt(0));
          TextView text = ((TextView) cell.getChildAt(1));
//          image.getDrawable().setLevel(item.getFasting());
          text.setText(item.getValue());
          text.getBackground().setLevel(0);
          cell.setTag(item);
          switch (item.getType()) {
            case REGULAR:
              image.setVisibility(View.VISIBLE);
              if (year == now.get(Calendar.YEAR) && month == now.get(Calendar.MONTH) && Integer.valueOf(item.getValue()) == now.get(Calendar.DAY_OF_MONTH)) { //Today
                text.getBackground().setLevel(1);
              }
              if (Integer.valueOf(item.getValue()) == mCurrentDay) { //Current date
                setCalendarGridSelection(cell);
              } else { //Other (regular) date
                text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular);
                cell.getBackground().setLevel(/*item.getFasting() == 0 || item.getFasting() == 8 ?*/ 0 /*: 2*/);
                cell.setEnabled(true);
              }
              break;
            case NEXT:
            case PREVIOUS:
              text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular_Other);
              image.setVisibility(View.INVISIBLE);
              cell.getBackground().setLevel(0);
              cell.setEnabled(item.getValue().length() > 0);
              break;
            case HOLIDAY:
              image.setVisibility(View.VISIBLE);
              if (year == now.get(Calendar.YEAR) && month == now.get(Calendar.MONTH) && Integer.valueOf(item.getValue()) == now.get(Calendar.DAY_OF_MONTH)) { //Today
                text.getBackground().setLevel(2);
              }
              if (Integer.valueOf(item.getValue()) == mCurrentDay) { //Current date
                setCalendarGridSelection(cell);
              } else { //Other (regular) date
                text.setTextAppearance(getActivity(), R.style.Calendar_Item_Grid_Regular_Holiday);
                cell.getBackground().setLevel(1);
                cell.setEnabled(true);
              }
              break;
            default:
              break;
          }
          image.setVisibility(View.GONE); //New
        }
      }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }
  };

  /**
   * Event handler for calendar cell click
   * @param v Cell that was clicked
   */
  private OnClickListener mGridCellClickListener = new OnClickListener() {
    public void onClick(View v) {
      CalendarItem item = (CalendarItem) v.getTag();
      switch (item.getType()) {
        case REGULAR:
        case HOLIDAY:
          setCalendarGridSelection((ViewGroup) v);
          mCurrentDay = Integer.valueOf(item.getValue());
          break;
        case PREVIOUS:
          if (mCurrentMonth == Calendar.JANUARY) { //Switch to previous year
            mCurrentMonth = Calendar.DECEMBER;
            mCurrentDay = Integer.valueOf(item.getValue());
            mYearFlipper.showPrevious();
          } else { //Switch to previous month
            mCurrentDay = Integer.valueOf(item.getValue());
            setCalendarMonthSelection(mMonthSelector.getSelectedItemPosition() - 1);
          }
          break;
        case NEXT:
          if (mCurrentMonth == Calendar.DECEMBER) { //Switch to next year
            mCurrentMonth = Calendar.JANUARY;
            mCurrentDay = Integer.valueOf(item.getValue());
            mYearFlipper.showNext();
          } else { //Switch to next month
            mCurrentDay = Integer.valueOf(item.getValue());
            setCalendarMonthSelection(mMonthSelector.getSelectedItemPosition() + 1);
          }
          break;
        default:
          break;
      }
    }
  };

  /**
   * Span which is used for calendar daily readings links
   */
  private ClickableSpan mLinkClickableSpan = new ClickableSpan() {
    @Override
    public void onClick(View view) {
      if (mListener != null) {
        mListener.onSingleLink(((TextView) view).getText().toString());
      }
    }
  };

  //-------------------------------------------------------------------------------------------------------

  /**
   * Codes used to report BookLoaderTask result
   */
  private enum LoaderResultCode {
    SUCCESS, CLEAR, ERROR, NOT_FOUND
  }

  /**
   * Task which is used for calendar year data loading
   * @author Semeniuk A.D.
   */
  private class YearLoaderTask extends AsyncTask<Integer, Void, LoaderResultCode> {

    @Override
    protected void onPreExecute() {
      mYearProgressBar.setVisibility(View.VISIBLE);
      if (mYearPrev != null) {
        mYearPrev.setVisibility(View.GONE);
      }
      if (mYearNext != null) {
        mYearNext.setVisibility(View.GONE);
      }
    }

    @Override
    protected LoaderResultCode doInBackground(Integer... params) {
      try {
        int year = params[0];
        File info = new File(new File(new File(getArguments().getString("storage"), "calendar"), String.format("%04d", year)), "info.csv");
        if (!info.exists()) {
          return LoaderResultCode.NOT_FOUND;
        }
        mYearInfo = FileOperations.loadCalendarYearInfo(info);
        if (mYearInfo.size() < 12) {
          return LoaderResultCode.ERROR;
        }
      } catch (Exception ex) {
        return LoaderResultCode.ERROR;
      }
      return LoaderResultCode.SUCCESS;
    }

    @Override
    protected void onPostExecute(LoaderResultCode result) {
      mYearProgressBar.setVisibility(View.GONE);
      if (mYearPrev != null) {
        mYearPrev.setVisibility(mYearFlipper.getDisplayedChild() > 0 ? View.VISIBLE : View.INVISIBLE);
      }
      if (mYearNext != null) {
        mYearNext.setVisibility(mYearFlipper.getDisplayedChild() > 0 ? View.VISIBLE : View.INVISIBLE);
      }
      switch (result) {
        case SUCCESS:
          if (mCurrentMonth < 0) {
            Calendar now = Calendar.getInstance();
            mCurrentMonth = now.get(Calendar.MONTH);
            mCurrentDay = now.get(Calendar.DAY_OF_MONTH);
          }
          int position = Integer.MAX_VALUE / 2 - (Integer.MAX_VALUE / 2) % 12 + mCurrentMonth;
          setCalendarMonthSelection(position);
          mYearData.setVisibility(View.VISIBLE);
          break;
        case NOT_FOUND:
          mYearAbsent.setVisibility(View.GONE);
          mYearNotFound.setVisibility(View.VISIBLE);
          mYearData.setVisibility(View.GONE);
          mCurrentMonth = -1;
          mCurrentDay = -1;
          break;
        case ERROR: //Calendar files not found on SD card
          mYearNotFound.setVisibility(View.GONE);
          mYearAbsent.setVisibility(View.VISIBLE);
          mYearAbsent.setText(R.string.calendar_year_error);
          mYearData.setVisibility(View.GONE);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Task which is used for calendar data downloading
   * @author Semeniuk A.D.
   */
  private class DownloadTask extends AsyncTask<Integer, Integer, LoaderResultCode> {

    private ProgressDialog progress;

    @Override
    protected void onPreExecute() {
      progress = new ProgressDialog(getActivity());
      progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
      progress.setMessage(getString(R.string.calendar_year_downloading));
      progress.setMax(367);
      progress.setOnCancelListener(new OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          DownloadTask.this.cancel(true);
        }
      });
      progress.show();
    }

    @Override
    protected void onCancelled() {
      if (getActivity() != null) {
        Toast.makeText(getActivity(), R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
      }
    }

    @Override
    protected LoaderResultCode doInBackground(Integer... params) {
      int year = params[0];
      try {
        if (!ServerConnector.checkCalendarExists(year)) { //Data not available on server
          return LoaderResultCode.NOT_FOUND;
        }
        File path = new File(new File(getArguments().getString("storage"), "calendar"), String.format("%04d", year));
        path.delete();
        path.mkdirs();
        File zip = new File(path, "data.zip");
        FileOperations.saveFile(ServerConnector.getCalendarZip(year), zip);
        FileOperations.extractZip(zip, path, new Callback() {
          @Override
          public void onEvent(int value) {
            publishProgress(value);
          }
        });
        FileOperations.deleteFile(zip);
      } catch (Exception ex) {
        return LoaderResultCode.ERROR;
      }
      return LoaderResultCode.SUCCESS;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      progress.setProgress(-values[0]);
    }

    @Override
    protected void onPostExecute(LoaderResultCode result) {
      if (progress != null) {
        progress.dismiss();
      }
      switch (result) {
        case SUCCESS:
          mYearFlipper.setData(CALENDAR_YEAR_MAX - CALENDAR_YEAR_MIN, mYearFlipper.getDisplayedChild());
          break;
        case NOT_FOUND: //Calendar files not found on server
          mYearAbsent.setVisibility(View.VISIBLE);
          mYearNotFound.setVisibility(View.GONE);
          mYearData.setVisibility(View.GONE);
          break;
        case ERROR: //Error loading or saving files
          new AlertDialog.Builder(getActivity()).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert).setMessage(R.string.calendar_year_download_error).setPositiveButton(R.string.dialog_ok, null).show();
          break;
        default:
          break;
      }
    }
  }

  /**
   * Task which is used to load data for particular day
   * @author Semeniuk A.D.
   */
  private class DayLoaderTask extends AsyncTask<Integer, Void, CalendarEntry> {

    @Override
    protected void onPreExecute() {
      mDayProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected CalendarEntry doInBackground(Integer... params) {
      try {
        Calendar day = Calendar.getInstance();
        day.set(params[0], params[1], params[2]);
        File data = new File(new File(new File(getArguments().getString("storage"), "calendar"), String.format("%04d", params[0])), String.format("%03d.xml", day.get(Calendar.DAY_OF_YEAR)));

        return FileOperations.loadCalendarDay(data);
      } catch (Exception ex) {
        return null;
      }
    }

    @Override
    protected void onPostExecute(CalendarEntry result) {
      mDayProgressBar.setVisibility(View.GONE);
      if (result == null) { //Exception happened
        mDayScroll.setVisibility(View.GONE);
        Toast.makeText(getActivity(), R.string.calendar_day_error, Toast.LENGTH_SHORT).show();
        return;
      }
      mDayScroll.scrollTo(0, 0);
      mDayScroll.setVisibility(View.VISIBLE);
      mDayTitleScroll.scrollTo(0, 0);
      mDayTitle.setText(result.getTitle());
      mDayTitle.setTextColor(getResources().getColor(result.isHoliday() ? R.color.calendar_day_title_holiday : R.color.calendar_day_title_regular));
      mDayFasting.setText(result.getFasting());
//      mDayFasting.getCompoundDrawables()[0].setLevel(result.getFastIndex());
      if (result.getReadingsOld().size() > 0) {
        mDayReadingsOld.setVisibility(View.VISIBLE);
        mDayReadingsOld.removeViews(1, mDayReadingsOld.getChildCount() - 1);
        for (DailyReading r : result.getReadingsOld()) {
          addCalendarDailyReading(mDayReadingsOld, r);
        }
        mDayReadingsOldAll.setTag(result.getReadingsOld());
      } else {
        mDayReadingsOld.setVisibility(View.GONE);
        mDayReadingsOldAll.setTag(null);
      }
      if (result.getReadingsApostle().size() > 0) {
        mDayReadingsApostle.setVisibility(View.VISIBLE);
        mDayReadingsApostle.removeViews(1, mDayReadingsApostle.getChildCount() - 1);
        for (DailyReading r : result.getReadingsApostle()) {
          addCalendarDailyReading(mDayReadingsApostle, r);
        }
        mDayReadingsApostleAll.setTag(result.getReadingsApostle());
      } else {
        mDayReadingsApostle.setVisibility(View.GONE);
        mDayReadingsApostleAll.setTag(null);
      }
      if (result.getReadingsGospel().size() > 0) {
        mDayReadingsGospel.setVisibility(View.VISIBLE);
        mDayReadingsGospel.removeViews(1, mDayReadingsGospel.getChildCount() - 1);
        for (DailyReading r : result.getReadingsGospel()) {
          addCalendarDailyReading(mDayReadingsGospel, r);
        }
        mDayReadingsGospelAll.setTag(result.getReadingsGospel());
      } else {
        mDayReadingsGospel.setVisibility(View.GONE);
        mDayReadingsGospelAll.setTag(null);
      }
    }
  }

}
