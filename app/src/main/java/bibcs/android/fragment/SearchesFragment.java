package bibcs.android.fragment;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.alexsem.android.widget.HidingPanel;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import bibcs.android.activity.R;
import bibcs.android.adapter.SearchHistoryAdapter;
import bibcs.android.adapter.SearchHistoryAdapter.OnSearchItemClickListener;
import bibcs.android.transfer.DatabaseAdapter;
import bibcs.android.util.CalculationLogic;

/**
 * Fragment used to search for books and view history
 * @author Semeniuk A.D.
 */
public class SearchesFragment extends Fragment {

	/**
	 * Interface used for communication with Activity
	 * @author Semeniuk A.D.
	 */
	public interface OnSearchesListener {
		public void onSearch(String text);

		public void onCancel();
	}

	private DatabaseAdapter mDatabase;
	private LayoutInflater mInflater;

	private EditText mQuery;
	private ImageButton mSearch;
	private ImageView mClear;
	private HidingPanel mEim;
	private ListView mHistory;
	private LinearLayout mBooksOld;
	private LinearLayout mBooksNew;

	private Pattern mInputValidator;

	private OnSearchesListener mListener = null;
	private int mHistorySize = 1000;
	private boolean isNeedToSetOptions = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View content = inflater.inflate(R.layout.search, container, false);
		mInflater = inflater;

		//--- Search main items ---
		mQuery = (EditText) content.findViewById(R.id.search_edit);
		mQuery.setInputType(0);
		mQuery.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				mEim.show();
				return false;
			}
		});
		mQuery.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0) { //Search string is not empty
					mClear.setVisibility(View.VISIBLE);
					if (checkSearchString(s.toString())) { //Search string is valid 
						mQuery.getBackground().setLevel(1);
						mSearch.setEnabled(true);
					} else { //Search string is invalid
						mQuery.getBackground().setLevel(2);
						mSearch.setEnabled(false);
					}
				} else { //Nothing entered
					mClear.setVisibility(View.INVISIBLE);
					mQuery.getBackground().setLevel(0);
					mSearch.setEnabled(true);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		mSearch = (ImageButton) content.findViewById(R.id.search_button);
		mSearch.setOnClickListener(mSearchClickListener);
		mClear = (ImageView) content.findViewById(R.id.search_clear);
		mClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mQuery.getText().clear();
				mEim.show();
			}
		});

		//--- Search history ---
		mHistory = (ListView) content.findViewById(R.id.search_history);
		mHistory.setOnTouchListener(mEimHideTouchListener);
		mHistory.setSelectionFromTop(0, 0);

		//--- Search book EIM ---
		mEim = (HidingPanel) content.findViewById(R.id.search_eim);
		mBooksOld = (LinearLayout) content.findViewById(R.id.search_books_old);
		mBooksNew = (LinearLayout) content.findViewById(R.id.search_books_new);
		LinearLayout global = (LinearLayout) content.findViewById(R.id.search_eim_keys);
		for (int i = 0; i < global.getChildCount(); i++) {
			LinearLayout pane = (LinearLayout) global.getChildAt(i);
			for (int j = 0; j < pane.getChildCount(); j++) {
				LinearLayout row = (LinearLayout) pane.getChildAt(j);
				for (int k = 0; k < row.getChildCount(); k++) {
					row.getChildAt(k).setOnClickListener(mEimKeyListener);
				}
			}
		}

		return content;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mEim.requestLayout();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mDatabase = DatabaseAdapter.getInstance(getActivity());
		mDatabase.openRead();
		//--- History ---
		mHistory.setAdapter(new SearchHistoryAdapter(getActivity(), mDatabase.selectHistory()));
		((SearchHistoryAdapter) mHistory.getAdapter()).setOnItemClickListener(mHistoryItemClickListener);
		//--- Validator ---
		mInputValidator = CalculationLogic.generateValidationPattern(mDatabase);
		//--- EIM ---
		for (String name : mDatabase.selectBookShortestNames(1, 50)) {
			Button b = (Button) mInflater.inflate(R.layout.search_book, mBooksOld, false);
			b.setOnClickListener(mEimKeyListener);
			b.setText(name);
			mBooksOld.addView(b);
		}
		for (String name : mDatabase.selectBookShortestNames(51, 77)) {
			Button b = (Button) mInflater.inflate(R.layout.search_book, mBooksNew, false);
			b.setOnClickListener(mEimKeyListener);
			b.setText(name);
			mBooksNew.addView(b);
		}
		mDatabase.close();
	}

	public void setOnSearchesListener(OnSearchesListener listener) {
		this.mListener = listener;
	}

	/**
	 * Sets history size to be used for titles
	 * @param options Options to set
	 */
	public void setHistorySize(int historySize) {
		this.mHistorySize = historySize;
		if (isVisible() && mHistory.getAdapter() != null) {
			((SearchHistoryAdapter) mHistory.getAdapter()).trimToSize(mHistorySize);
			isNeedToSetOptions = false;
		} else {
			isNeedToSetOptions = true;
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (hidden) {
			mEim.show();
			mQuery.requestFocus();
			if (isNeedToSetOptions) {
				setHistorySize(mHistorySize);
			}
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		mEim.hide();
		//--- Save history ---
		if (mHistory.getAdapter() != null) {
			mDatabase.openWrite();
			mDatabase.deleteHistory();
			for (int i = 0; i < mHistory.getAdapter().getCount(); i++) {
				mDatabase.insertHistory(((SearchHistoryAdapter) mHistory.getAdapter()).getItem(i));
			}
			mDatabase.close();
		}
	}

	/**
	 * Adds text to the search edit box
	 * @param text Text to add
	 */
	private void commitSearchText(CharSequence text) {
		int position = mQuery.getSelectionStart();
		mQuery.getText().insert(position, text);
	}

	/**
	 * Defines whether given string can be parsed
	 * @param input String in question
	 * @return true if string matches global regular expression, false otherwise
	 */
	public boolean checkSearchString(String input) {
		Matcher matcher = mInputValidator.matcher(input.toLowerCase(Locale.getDefault()) + ";");
		return matcher.matches();
	}

	/**
	 * Simulate search button press
	 */
	public void performSearch() {
		mSearch.performClick();
	}

	/**
	 * Attempt to hide EIM
	 * @return true if EIM was opened, false otherwise
	 */
	public boolean hideEim() {
		if (mEim.isOpened()) {
			mEim.hide();
			return true;
		}
		return false;
	}

	/**
	 * Listener for search button clicks
	 */
	private OnClickListener mSearchClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String searchString = (mQuery.getText().toString());
			if (mQuery.getText().length() > 0) {
				((SearchHistoryAdapter) mHistory.getAdapter()).addHistoryItem(searchString);
				((SearchHistoryAdapter) mHistory.getAdapter()).trimToSize(mHistorySize);
				mHistory.setSelectionFromTop(0, 0);
				mEim.hide();
			} else {
				mEim.show();
			}
			mQuery.getText().clear();
			if (mListener != null) {
				mListener.onSearch(searchString);
			}
		}
	};

	/**
	 * Listener for search history item clicks
	 */
	private OnSearchItemClickListener mHistoryItemClickListener = new OnSearchItemClickListener() {
		@Override
		public void onItemClick(View v, int position) {
			mQuery.setText(((SearchHistoryAdapter) mHistory.getAdapter()).getItem(position));
			mQuery.setSelection(mQuery.getText().length());
			//btnSearch.performClick();
		}
	};

	/**
	 * Listener for EIM buttons clicks
	 */
	private OnClickListener mEimKeyListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			mQuery.requestFocus();
			switch (v.getId()) {
				case R.id.search_key_1:
				case R.id.search_key_2:
				case R.id.search_key_3:
				case R.id.search_key_4:
				case R.id.search_key_5:
				case R.id.search_key_6:
				case R.id.search_key_7:
				case R.id.search_key_8:
				case R.id.search_key_9:
				case R.id.search_key_0:
				case R.id.search_key_space:
				case R.id.search_key_colon:
				case R.id.search_key_minus:
				case R.id.search_key_semic:
				case R.id.search_key_comma:
					commitSearchText(((Button) v).getText());
					break;
				case R.id.search_key_enter:
					if (mSearch.isEnabled()) {
						mSearch.performClick();
					}
					break;
				case R.id.search_key_delete:
					int position = mQuery.getSelectionStart();
					String text = mQuery.getText().toString();
					int n = -999;
					if (position > 1 && text.subSequence(position - 2, position).equals(". ")) {
						n = position - 2;
					} else if (position > 0 && text.subSequence(position - 1, position).equals(".")) {
						n = position - 1;
					}
					if (n != -999) { //Need to delete whole book name
						while (n > -1 && " ;,-:".indexOf(text.charAt(n)) == -1) {
							n--;
						}
						mQuery.getText().delete(n + 1, position);
					} else if (position > 0) { //Delete only 1 character
						mQuery.getText().delete(position - 1, position);
					}
					break;
				case R.id.search_key_hide:
					mEim.hide();
					break;
				case R.id.search_key_close:
					mEim.hide();
					if (mListener != null) {
						mListener.onCancel();
					}
					break;
				default: //Names
					commitSearchText(((Button) v).getText() + ". ");
					break;
			}
		}
	};

	private OnTouchListener mEimHideTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			mEim.hide();
			return false;
		}
	};

}