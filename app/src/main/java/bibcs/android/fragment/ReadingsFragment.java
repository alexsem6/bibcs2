package bibcs.android.fragment;

import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher.ViewFactory;
import bibcs.android.activity.R;
import bibcs.android.adapter.ReadingsSetupBookAdapter;
import bibcs.android.model.Readings;
import bibcs.android.transfer.DatabaseAdapter;
import bibcs.android.util.CalculationLogic;

/**
 * Fragment used to setup daily readings
 * @author Semeniuk A.D.
 */
public class ReadingsFragment extends Fragment {

	/**
	 * Interface used for communication with Activity
	 * @author Semeniuk A.D.
	 */
	public interface OnReadingsListener {
		public void onSearch(String text);
	}

	private final int READINGS_MAX_LENGTH = 5;

	private DatabaseAdapter mDatabase;
	private LayoutInflater mInflater;

	private Animation mAnimFromRight;
	private Animation mAnimFromLeft;
	private Animation mAnimToRight;
	private Animation mAnimToLeft;

	private TextView mTxtToday;
	private TextSwitcher mGospelIndex;
	private TextSwitcher mApostleIndex;
	private ToggleButton mBtnGospelSetup;
	private TableLayout mTblGospelSetup;
	private Spinner mGospelSetupBook;
	private EditText mGospelSetupChapter;
	private EditText mGospelSetupLength;
	private ToggleButton mBtnApostleSetup;
	private TableLayout mTblApostleSetup;
	private Spinner mApostleSetupBook;
	private EditText mApostleSetupChapter;
	private EditText mApostleSetupLength;

	private Readings mGospelReadings;
	private Readings mApostleReadings;
	private int mGospelSetupBookSize = 1;
	private int mApostleSetupBookSize = 1;

	private OnReadingsListener mListener = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View content = inflater.inflate(R.layout.readings, container, false);
		mInflater = inflater;

		//--- Regular animations ---
		mAnimFromRight = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_from_right);
		mAnimFromLeft = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_from_left);
		mAnimToRight = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_to_right);
		mAnimToLeft = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.fly_to_left);

		//--- Readings Drawer ---
		mTxtToday = (TextView) content.findViewById(R.id.readings_today);
		mGospelIndex = (TextSwitcher) content.findViewById(R.id.readings_gospel_text);
		mGospelIndex.setFactory(mIndexViewFactory);
		mGospelIndex.setOnClickListener(mIndexClickListener);
		mApostleIndex = (TextSwitcher) content.findViewById(R.id.readings_apostle_text);
		mApostleIndex.setFactory(mIndexViewFactory);
		mApostleIndex.setOnClickListener(mIndexClickListener);

		//--- Readings arrows ---
		content.findViewById(R.id.readings_gospel_prev).setOnClickListener(new OnClickListener() { //Previous Gospel
					@Override
					public void onClick(View v) {
						mGospelIndex.setInAnimation(mAnimFromLeft);
						mGospelIndex.setOutAnimation(mAnimToRight);
						mDatabase.openRead();
						mGospelReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mGospelReadings, -mGospelReadings.getLength());
						mGospelIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mGospelReadings));
						mDatabase.close();
					}
				});
		content.findViewById(R.id.readings_gospel_next).setOnClickListener(new OnClickListener() { //Next Gospel
					@Override
					public void onClick(View v) {
						mGospelIndex.setInAnimation(mAnimFromRight);
						mGospelIndex.setOutAnimation(mAnimToLeft);
						mDatabase.openRead();
						mGospelReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mGospelReadings, mGospelReadings.getLength());
						mGospelIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mGospelReadings));
						mDatabase.close();
					}
				});
		content.findViewById(R.id.readings_apostle_prev).setOnClickListener(new OnClickListener() { //Previous Apostle
					@Override
					public void onClick(View v) {
						mApostleIndex.setInAnimation(mAnimFromLeft);
						mApostleIndex.setOutAnimation(mAnimToRight);
						mDatabase.openRead();
						mApostleReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mApostleReadings, -mApostleReadings.getLength());
						mApostleIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mApostleReadings));
						mDatabase.close();
					}
				});
		content.findViewById(R.id.readings_apostle_next).setOnClickListener(new OnClickListener() { //Next Apostle
					@Override
					public void onClick(View v) {
						mApostleIndex.setInAnimation(mAnimFromRight);
						mApostleIndex.setOutAnimation(mAnimToLeft);
						mDatabase.openRead();
						mApostleReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mApostleReadings, mApostleReadings.getLength());
						mApostleIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mApostleReadings));
						mDatabase.close();
					}
				});

		//--- Readings Setup (Gospel) ---
		mTblGospelSetup = (TableLayout) content.findViewById(R.id.readings_gospel_setup_panel);
		mBtnGospelSetup = (ToggleButton) content.findViewById(R.id.readings_gospel_setup);
		mBtnGospelSetup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mTblGospelSetup.setVisibility(isChecked ? View.VISIBLE : View.GONE);
			}
		});
		mGospelSetupChapter = (EditText) content.findViewById(R.id.readings_gospel_setup_chapter);
		mGospelSetupChapter.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == 0) { //Nothing entered yet
					return;
				}
				int value = Integer.valueOf(s.toString());
				if (value < 1) {
					s.clear();
					s.append(String.valueOf(1));
				} else if (value > mGospelSetupBookSize) {
					s.clear();
					s.append(String.valueOf(mGospelSetupBookSize));
				}
			}
		});
		mGospelSetupLength = (EditText) content.findViewById(R.id.readings_gospel_setup_length);
		mGospelSetupLength.addTextChangedListener(mSetupLengthTextListener);
		mGospelSetupBook = (Spinner) content.findViewById(R.id.readings_gospel_setup_book);
		mGospelSetupBook.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				mGospelSetupChapter.setText("1");
				mDatabase.openRead();
				mGospelSetupBookSize = mDatabase.selectBookSize(position + 51);
				mDatabase.close();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		content.findViewById(R.id.readings_gospel_setup_chapter_next).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_gospel_setup_chapter_prev).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_gospel_setup_length_next).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_gospel_setup_length_prev).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_gospel_setup_commit).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int chap = 1;
				try {
					chap = Integer.valueOf(mGospelSetupChapter.getText().toString());
				} catch (NumberFormatException ex) {
				}
				mGospelSetupChapter.setText(String.valueOf(chap));
				int length = 1;
				try {
					length = Integer.valueOf(mGospelSetupLength.getText().toString());
				} catch (NumberFormatException ex) {
				}
				mGospelSetupLength.setText(String.valueOf(length));
				mGospelReadings.clearIndex();
				mGospelReadings.addIndex(mGospelSetupBook.getSelectedItemPosition() + 51, chap);
				mGospelReadings.setLength(length);
				mGospelReadings.resetDate();
				mDatabase.openWrite();
				mDatabase.updateReadings(0, mGospelReadings);
				mGospelReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mGospelReadings, 0);
				mGospelIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mGospelReadings));
				mDatabase.close();
				mBtnGospelSetup.setChecked(false);
			}
		});

		//--- Readings Setup (Apostle) ---
		mTblApostleSetup = (TableLayout) content.findViewById(R.id.readings_apostle_setup_panel);
		mBtnApostleSetup = (ToggleButton) content.findViewById(R.id.readings_apostle_setup);
		mBtnApostleSetup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mTblApostleSetup.setVisibility(isChecked ? View.VISIBLE : View.GONE);
			}
		});
		mApostleSetupChapter = (EditText) content.findViewById(R.id.readings_apostle_setup_chapter);
		mApostleSetupChapter.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == 0) { //Nothing entered yet
					return;
				}
				int value = Integer.valueOf(s.toString());
				if (value < 1) {
					s.clear();
					s.append(String.valueOf(1));
				} else if (value > mApostleSetupBookSize) {
					s.clear();
					s.append(String.valueOf(mApostleSetupBookSize));
				}
			}
		});
		mApostleSetupLength = (EditText) content.findViewById(R.id.readings_apostle_setup_length);
		mApostleSetupLength.addTextChangedListener(mSetupLengthTextListener);
		mApostleSetupBook = (Spinner) content.findViewById(R.id.readings_apostle_setup_book);
		mApostleSetupBook.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				mApostleSetupChapter.setText("1");
				mDatabase.openRead();
				mApostleSetupBookSize = mDatabase.selectBookSize(position + 55);
				mDatabase.close();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		content.findViewById(R.id.readings_apostle_setup_chapter_next).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_apostle_setup_chapter_prev).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_apostle_setup_length_next).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_apostle_setup_length_prev).setOnClickListener(mSetupArrowListener);
		content.findViewById(R.id.readings_apostle_setup_commit).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int chap = 1;
				try {
					chap = Integer.valueOf(mApostleSetupChapter.getText().toString());
				} catch (NumberFormatException ex) {
				}
				mApostleSetupChapter.setText(String.valueOf(chap));
				int length = 1;
				try {
					length = Integer.valueOf(mApostleSetupLength.getText().toString());
				} catch (NumberFormatException ex) {
				}
				mApostleSetupLength.setText(String.valueOf(length));
				mApostleReadings.clearIndex();
				mApostleReadings.addIndex(mApostleSetupBook.getSelectedItemPosition() + 55, chap);
				mApostleReadings.setLength(length);
				mApostleReadings.resetDate();
				mDatabase.openWrite();
				mDatabase.updateReadings(1, mApostleReadings);
				mApostleReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mApostleReadings, 0);
				mApostleIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mApostleReadings));
				mDatabase.close();
				mBtnApostleSetup.setChecked(false);

			}
		});

		return content;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Calendar now = Calendar.getInstance();
		mTxtToday.setText(String.format("%1$te %1$tb %1$tY", now));
		mDatabase = DatabaseAdapter.getInstance(getActivity());
		mDatabase.openRead();
		mGospelSetupBook.setAdapter(new ReadingsSetupBookAdapter(getActivity(), mDatabase.selectBookNames(51, 54)));
		mApostleSetupBook.setAdapter(new ReadingsSetupBookAdapter(getActivity(), mDatabase.selectBookNames(55, 76)));
		mGospelReadings = mDatabase.selectReadings(0);
		mGospelReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mGospelReadings, 0);
		if (now.compareTo(mGospelReadings.getDate()) > 0 && mGospelReadings.isDateSet()) { //Need to increment readings
			mGospelReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mGospelReadings, mGospelReadings.getLength());
		}
		mGospelIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mGospelReadings));
		mApostleReadings = mDatabase.selectReadings(1);
		mApostleReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mApostleReadings, 0);
		if (now.compareTo(mApostleReadings.getDate()) > 0 && mApostleReadings.isDateSet()) { //Need to increment readings 
			mApostleReadings = CalculationLogic.generateReadingsIndexList(mDatabase, mApostleReadings, mApostleReadings.getLength());
		}
		mApostleIndex.setText(CalculationLogic.generateReadingsIndexText(mDatabase, mApostleReadings));
		mDatabase.close();
		mGospelSetupBook.setSelection(mGospelReadings.getIndex(0).book - 51);
		mGospelSetupChapter.setText(String.valueOf(mGospelReadings.getIndex(0).chapter));
		mGospelSetupLength.setText(String.valueOf(mGospelReadings.getLength()));
		mApostleSetupBook.setSelection(mApostleReadings.getIndex(0).book - 55);
		mApostleSetupChapter.setText(String.valueOf(mApostleReadings.getIndex(0).chapter));
		mApostleSetupLength.setText(String.valueOf(mApostleReadings.getLength()));
		mBtnGospelSetup.setChecked(false);
		mBtnApostleSetup.setChecked(false);
	}

	public void setOnReadingsListener(OnReadingsListener listener) {
		this.mListener = listener;
	}

	/**
	 * Callback for Readings setup arrow clicks
	 */
	private OnClickListener mSetupArrowListener = new OnClickListener() {

		public void onClick(View v) {
			int value = 1;
			switch (v.getId()) {
				case R.id.readings_gospel_setup_chapter_prev:
					try {
						value = Integer.valueOf(mGospelSetupChapter.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value--;
					if (value < 1) {
						value = mGospelSetupBookSize;
					}
					mGospelSetupChapter.setText(String.valueOf(value));
					break;
				case R.id.readings_gospel_setup_chapter_next:
					try {
						value = Integer.valueOf(mGospelSetupChapter.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value++;
					if (value > mGospelSetupBookSize) {
						value = 1;
					}
					mGospelSetupChapter.setText(String.valueOf(value));
					break;
				case R.id.readings_gospel_setup_length_prev:
					try {
						value = Integer.valueOf(mGospelSetupLength.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value--;
					if (value < 1) {
						value = READINGS_MAX_LENGTH;
					}
					mGospelSetupLength.setText(String.valueOf(value));
					break;
				case R.id.readings_gospel_setup_length_next:
					try {
						value = Integer.valueOf(mGospelSetupLength.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value++;
					if (value > READINGS_MAX_LENGTH) {
						value = 1;
					}
					mGospelSetupLength.setText(String.valueOf(value));
					break;
				case R.id.readings_apostle_setup_chapter_prev:
					try {
						value = Integer.valueOf(mApostleSetupChapter.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value--;
					if (value < 1) {
						value = mApostleSetupBookSize;
					}
					mApostleSetupChapter.setText(String.valueOf(value));
					break;
				case R.id.readings_apostle_setup_chapter_next:
					try {
						value = Integer.valueOf(mApostleSetupChapter.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value++;
					if (value > mApostleSetupBookSize) {
						value = 1;
					}
					mApostleSetupChapter.setText(String.valueOf(value));
					break;
				case R.id.readings_apostle_setup_length_prev:
					try {
						value = Integer.valueOf(mApostleSetupLength.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value--;
					if (value < 1) {
						value = READINGS_MAX_LENGTH;
					}
					mApostleSetupLength.setText(String.valueOf(value));
					break;
				case R.id.readings_apostle_setup_length_next:
					try {
						value = Integer.valueOf(mApostleSetupLength.getText().toString());
					} catch (NumberFormatException ex) {
					}
					value++;
					if (value > READINGS_MAX_LENGTH) {
						value = 1;
					}
					mApostleSetupLength.setText(String.valueOf(value));
					break;
			}
		}
	};

	/**
	 * Factory for index TextView construction
	 */
	private ViewFactory mIndexViewFactory = new ViewFactory() {
		@Override
		public View makeView() {
			return mInflater.inflate(R.layout.readings_index, null);
		}
	};

	/**
	 * Listener for readings index clicks (initiate reading)
	 */
	private OnClickListener mIndexClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.readings_gospel_text: //Gospel
					mGospelReadings.setDate(Calendar.getInstance());
					mDatabase.openWrite();
					mDatabase.updateReadings(0, mGospelReadings);
					mDatabase.close();
					break;
				case R.id.readings_apostle_text: //Apostle
					mApostleReadings.setDate(Calendar.getInstance());
					mDatabase.openWrite();
					mDatabase.updateReadings(1, mApostleReadings);
					mDatabase.close();
					break;
			}
			if (mListener != null) { //Callback specified
				String text = ((TextView) ((TextSwitcher) v).getCurrentView()).getText().toString();
				mListener.onSearch(text);
			}
		}
	};

	/**
	 * Listener for setup text input
	 */
	private TextWatcher mSetupLengthTextListener = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (s.length() == 0) { //Nothing entered yet
				return;
			}
			int value = Integer.valueOf(s.toString());
			if (value < 1) {
				s.clear();
				s.append(String.valueOf(1));
			} else if (value > READINGS_MAX_LENGTH) {
				s.clear();
				s.append(String.valueOf(READINGS_MAX_LENGTH));
			}
		}
	};

}