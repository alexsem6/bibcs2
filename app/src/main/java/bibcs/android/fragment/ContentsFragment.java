package bibcs.android.fragment;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import bibcs.android.activity.R;
import bibcs.android.adapter.ContentsAdapter;
import bibcs.android.model.Book;
import bibcs.android.model.Chapter;
import bibcs.android.transfer.DatabaseAdapter;

/**
 * Fragment used to display contents of books
 * @author Semeniuk A.D.
 */
public class ContentsFragment extends Fragment {

  /**
   * Interface used for communication with Activity
   * @author Semeniuk A.D.
   */
  public interface OnContentsListener {
    public void onChapterClick(int book, int chapter);

    public void onCancelClick();
  }

  private DatabaseAdapter mDatabase;
  private LayoutInflater mInflater;

  private TextView mTitleOld;
  private TextView mTitleNew;
  private TabHost mTabs;
  private ListView mContentsOld;
  private ListView mContentsNew;

  private OnContentsListener mListener = null;
  private float mFontSize = 14f;
  private boolean isNeedToSetOptions = false;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View content = inflater.inflate(R.layout.contents, container, false);
    mInflater = inflater;

    //--- Contents main data ---
    mTabs = (TabHost) content.findViewById(android.R.id.tabhost);
    mTabs.setup();
    mTabs.addTab(mTabs.newTabSpec("old").setIndicator(generateTabIndicator(R.string.contents_header_old, R.drawable.contents_header_old))
        .setContent(R.id.contents_list_old));
    mTabs.addTab(mTabs.newTabSpec("new").setIndicator(generateTabIndicator(R.string.contents_header_new, R.drawable.contents_header_new))
        .setContent(R.id.contents_list_new));
    mTabs.setCurrentTab(1);

    //--- Contents lists ---
    mContentsOld = (ListView) content.findViewById(R.id.contents_list_old);
    mContentsOld.setOnItemClickListener(mItemClickListener);
    mContentsNew = (ListView) content.findViewById(R.id.contents_list_new);
    mContentsNew.setOnItemClickListener(mItemClickListener);

    //--- Contents buttons ---
    content.findViewById(R.id.contents_expand).setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        ListView w = mTabs.getCurrentTab() == 1 ? mContentsNew : mContentsOld;
        ((ContentsAdapter) w.getAdapter()).expandAll();
      }
    });
    content.findViewById(R.id.contents_collapse).setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        ListView w = mTabs.getCurrentTab() == 1 ? mContentsNew : mContentsOld;
        ((ContentsAdapter) w.getAdapter()).collapseAll();
      }
    });
    content.findViewById(R.id.contents_close).setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mListener != null) {
          mListener.onCancelClick();
        }
      }
    });

    return content;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mDatabase = DatabaseAdapter.getInstance(getActivity());
    if (savedInstanceState == null) {
      new ContentsLoaderTask().execute();
    }
  }

  public void setOnContentsListener(OnContentsListener listener) {
    this.mListener = listener;
  }

  /**
   * Sets font size to be used for titles
   * @param options Options to set
   */
  public void setFontSize(float fontSize) {
    this.mFontSize = fontSize;
    if (isVisible()) {
      ((ContentsAdapter) mContentsOld.getAdapter()).setFontSize(fontSize);
      ((ContentsAdapter) mContentsNew.getAdapter()).setFontSize(fontSize);
      mTitleOld.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
      mTitleNew.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
      isNeedToSetOptions = false;
    } else {
      isNeedToSetOptions = true;
    }
  }

  @Override
  public void onHiddenChanged(boolean hidden) {
    super.onHiddenChanged(hidden);
    if (!hidden && isNeedToSetOptions) {
      setFontSize(mFontSize);
    }
  }

  /**
   * Generates indicator for the tab in tab widget
   * @param text Text to show
   * @param icon Icon above the text
   * @return Generated view
   */
  private View generateTabIndicator(int text, int icon) {
    TextView result = (TextView) mInflater.inflate(R.layout.contents_header, null);
    result.setText(getString(text));
    if (mTitleOld == null) { //Old Testament title
      mTitleOld = result;
      mTitleOld.setTextSize(TypedValue.COMPLEX_UNIT_SP, mFontSize);
    } else { //New Testament title
      mTitleNew = result;
      mTitleNew.setTextSize(TypedValue.COMPLEX_UNIT_SP, mFontSize);
    }
    result.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(icon), null, null, null);
    return result;
  }

  /**
   * Generates map structure which can be used for displaying contents
   * @param books List of books
   * @return Map between book and chapter list
   */
  private Map<Book, List<Chapter>> convertBooksListToMap(List<Book> books) {
    Map<Book, List<Chapter>> result = new LinkedHashMap<Book, List<Chapter>>();
    for (Book book : books) {
      result.put(book, book.getChapters());
    }
    return result;
  }

  /**
   * Listener for clicked contents items
   */
  private OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
      Object object = parent.getAdapter().getItem(position);
      if (object instanceof Chapter) { //Chapter name clicked
        int book = ((ContentsAdapter) parent.getAdapter()).getParentOfChild((Chapter) object).getOrd();
        int chapter = ((Chapter) object).getOrd();
        if (mListener != null) {
          mListener.onChapterClick(book, chapter);
        }
      }
    }
  };

  //----------------------------------------------------------------------------

  /**
   * Task which is used for contents generation
   * @author Semeniuk A.D.
   */
  private class ContentsLoaderTask extends AsyncTask<Void, Map<Book, List<Chapter>>, Boolean> {

    private ProgressDialog mDialog;

    @Override
    protected void onPreExecute() {
      mDialog = new ProgressDialog(getActivity());
      mDialog.setMessage(getString(R.string.contents_loader_message));
      mDialog.show();
    }

    @Override
    protected void onProgressUpdate(Map<Book, List<Chapter>>... values) {
      ContentsAdapter adapterOld = new ContentsAdapter(getActivity(), values[0]);
      adapterOld.setFontSize(mFontSize);
      mContentsOld.setAdapter(adapterOld);
      ContentsAdapter adapterNew = new ContentsAdapter(getActivity(), values[1]);
      adapterNew.setFontSize(mFontSize);
      mContentsNew.setAdapter(adapterNew);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Boolean doInBackground(Void... params) {
      try {
        mDatabase.openRead();
        Map<Book, List<Chapter>> oldData = convertBooksListToMap(mDatabase.selectContents(1, 50));
        Map<Book, List<Chapter>> newData = convertBooksListToMap(mDatabase.selectContents(51, 77));
        mDatabase.close();
        publishProgress(oldData, newData);
        return true;
      } catch (Exception ex) {
        return false;
      }
    }

    @Override
    protected void onPostExecute(Boolean result) {
      if (mDialog != null && mDialog.isShowing()) {
        mDialog.dismiss();
      }
    }
  }

}
