package bibcs.android.adapter;

import java.util.List;
import java.util.Map;

import org.alexsem.android.adapter.Tree2SimpleAdapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import bibcs.android.activity.R;
import bibcs.android.model.Book;
import bibcs.android.model.Chapter;

/**
 * Tree adapter implementation which is used for contents representation
 * @author Semeniuk A.D.
 */
public class ContentsAdapter extends Tree2SimpleAdapter<Book, Chapter> {
	private LayoutInflater inflater = null;
	private float fontSize = 14f;

	public ContentsAdapter(Context context, Map<Book, List<Chapter>> data) {
		super(context, R.layout.contents_book, R.layout.contents_chapter, data);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setFontSize(float fontSize) {
		this.fontSize = fontSize;
		notifyDataSetChanged();
	}

	@Override
	public View getParentView(int parentIndex, View convertView, ViewGroup parent) {
		View row = convertView;
		BookWrapper wrapper = null;

		if (row == null || !row.getTag().getClass().equals(BookWrapper.class)) {
			row = inflater.inflate(R.layout.contents_book, parent, false);
			wrapper = new BookWrapper(row);
			row.setTag(wrapper);
		} else {
			wrapper = (BookWrapper) row.getTag();
		}
		final int pi = parentIndex;
		wrapper.getExpand().getDrawable().setLevel(this.isParentExpanded(parentIndex) ? 1 : 0);

		wrapper.getTitle().setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		wrapper.getTitle().setText(this.getParentItem(parentIndex).getName());
		row.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ContentsAdapter.this.toggleParentExpansion(pi);
			}
		});
		row.setFocusable(false);
		return row;
	}

	@Override
	public View getChildView(int parentIndex, int childIndex, View convertView, ViewGroup parent) {
		View row = convertView;
		ChapterWrapper wrapper = null;

		if (row == null || !row.getTag().getClass().equals(ChapterWrapper.class)) {
			row = inflater.inflate(R.layout.contents_chapter, parent, false);
			wrapper = new ChapterWrapper(row);
			row.setTag(wrapper);
		} else {
			wrapper = (ChapterWrapper) row.getTag();
		}

		wrapper.getTitle().setText(String.format("%s %d", this.getParentItem(parentIndex).getChapterName(), childIndex + 1));
		wrapper.getTitle().setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		return row;
	}

	//------------------------------------------------------------------------------------------------------

	/**
	 * Holder for book title views
	 * @author Semeniuk A.D.
	 */
	private static class BookWrapper {
		private View base;
		private ImageView expand = null;
		private TextView title = null;

		/**
		 * Constructor
		 * @param base Parent view
		 */
		public BookWrapper(View base) {
			this.base = base;
		}

		public ImageView getExpand() {
			if (expand == null) {
				expand = (ImageView) base.findViewById(R.id.contents_book_expand);
			}
			return (expand);
		}

		public TextView getTitle() {
			if (title == null) {
				title = (TextView) base.findViewById(R.id.contents_book_title);
			}
			return (title);
		}

	}

	/**
	 * Holder for chapter title views
	 * @author Semeniuk A.D.
	 */
	private static class ChapterWrapper {
		private View base;
		private TextView title = null;

		/**
		 * Constructor
		 * @param base Parent view
		 */
		public ChapterWrapper(View base) {
			this.base = base;
		}

		public TextView getTitle() {
			if (title == null) {
				title = (TextView) base.findViewById(R.id.contents_chapter_title);
			}
			return (title);
		}

	}

}