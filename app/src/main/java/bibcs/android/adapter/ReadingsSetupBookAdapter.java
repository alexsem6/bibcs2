package bibcs.android.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import bibcs.android.activity.R;

public class ReadingsSetupBookAdapter extends ArrayAdapter<String> {

	private LayoutInflater inflater = null;

	public ReadingsSetupBookAdapter(Context context, List<String> data) {
		super(context, R.layout.readings_setup_book, data);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView row = (TextView) convertView;
		
		if (row == null) {
			row = (TextView) inflater.inflate(R.layout.readings_setup_book, parent, false);
		}
		row.setText(getItem(position));
		return row;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {

		TextView row = (TextView) convertView;
		
		if (row == null) {
			row = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
			row.setPadding(5, 5, 5, 5);
		}
		
		row.setText(getItem(position));
		return row;
	}	

}
