package bibcs.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import bibcs.android.activity.R;

public class CalendarMonthAdapter extends BaseAdapter {
	private LayoutInflater inflater = null;
	private String[] data;

	public CalendarMonthAdapter(Context context, String[] data) {
		this.data = data;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	private int getPosition(int position) {
		if (position >= 12) {
			position = position % 12;
		}
		return position;
	}

	@Override
	public long getItemId(int position) {
		return getPosition(position);
	}

	@Override
	public Object getItem(int position) {
		return getPosition(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView row = (TextView) convertView;

		if (row == null) {
			row = (TextView) inflater.inflate(R.layout.calendar_month_item, parent, false);
		}

		row.setText(data[position % 12]);
		return row;
	}

}