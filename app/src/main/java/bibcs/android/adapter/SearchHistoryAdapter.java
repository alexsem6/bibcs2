package bibcs.android.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import bibcs.android.activity.R;

public class SearchHistoryAdapter extends ArrayAdapter<String> {
	private LayoutInflater inflater = null;
	private OnSearchItemClickListener onItemClickListener = null;

	public SearchHistoryAdapter(Context context, List<String> data) {
		super(context, R.layout.search_history_item, data);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setOnItemClickListener(OnSearchItemClickListener l) {
		this.onItemClickListener = l;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		SearchHistoryWrapper wrapper = null;

		if (row == null) {
			row = inflater.inflate(R.layout.search_history_item, parent, false);
			wrapper = new SearchHistoryWrapper(row);
			row.setTag(R.id.search_history_record, wrapper);
		} else {
			wrapper = (SearchHistoryWrapper) row.getTag(R.id.search_history_record);
		}

		wrapper.getRecord().setText(this.getItem(position));
		wrapper.getRemove().setTag(position);
		wrapper.getRemove().setOnClickListener(deleteClickListener);
		row.setOnClickListener(rowClickListener);
		row.setTag(R.id.search_history_remove, position);
		row.setFocusable(false);
		return row;
	}

	/**
	 * Adds new history item
	 * @param spell Item to add
	 */
	public void addHistoryItem(String spell) {
		this.remove(spell);
		this.insert(spell, 0);
		this.notifyDataSetChanged();
	}

	/**
	 * Trims list to the specified number of elements
	 * @param size Size in question
	 */
	public void trimToSize(int size) {
		ArrayList<String> toDelete = new ArrayList<String>();
		for (int i = size; i < this.getCount(); i++) {
			toDelete.add(getItem(i));
		}
		for (String item : toDelete) {
			this.remove(item);
		}
		this.notifyDataSetChanged();
	}

	@Override
	public void clear() {
		super.clear();
		this.notifyDataSetChanged();
	}

	private OnClickListener rowClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (onItemClickListener != null) {
				onItemClickListener.onItemClick(v, (Integer) v.getTag(R.id.search_history_remove));
			}
		}
	};

	private OnClickListener deleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			SearchHistoryAdapter.this.remove(SearchHistoryAdapter.this.getItem((Integer) v.getTag()));
			SearchHistoryAdapter.this.notifyDataSetChanged();
		}
	};

	/**
	 * Interface for item click listener
	 * @author Semeniuk A.D.
	 */
	public interface OnSearchItemClickListener {
		public void onItemClick(View v, int position);
	}

	//----------------------------------------------------------------------------------------------
	
	/**
	 * Temporary view holder
	 * @author Semeniuk A.D.
	 */
	private class SearchHistoryWrapper {
		private View base;
		private TextView record = null;
		private ImageButton remove = null;

		/**
		 * Constructor
		 * @param base Parent view
		 */
		public SearchHistoryWrapper(View base) {
			this.base = base;
		}

		public ImageButton getRemove() {
			if (remove == null) {
				remove = (ImageButton) base.findViewById(R.id.search_history_remove);
			}
			return (remove);
		}

		public TextView getRecord() {
			if (record == null) {
				record = (TextView) base.findViewById(R.id.search_history_record);
			}
			return (record);
		}

	}
}