package bibcs.android.adapter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import bibcs.android.activity.R;
import bibcs.android.model.Line;
import bibcs.android.model.Options;
import bibcs.android.widget.ReaderLinkedTextView;
import bibcs.android.widget.ReaderTextView;

public class ReaderAdapter extends ArrayAdapter<Line> {
    private LayoutInflater inflater = null;
    private Options options;

    private final Typeface FONT_REGULAR;
    private final Pattern PATTERN_PERICOPE;
    private final Pattern PATTERN_DICT;
    private final int COLOR_LINE_GHOST;
    private final int COLOR_LINE_GHOST_NIGHT;
    private final int COLOR_LINE_NUMBER;
    private final int COLOR_LINE_PERICOPE;
    private final int COLOR_LINE_TEXT;
    private final int COLOR_LINE_TEXT_NIGHT;

    private int colorLineText;
    private int colorLineGhost;

    private final OnTouchListener otl;

    private boolean isRusTextPresent = true;

    public ReaderAdapter(Context context, List<Line> data, Options options, Typeface font, OnTouchListener otl) {
        super(context, R.layout.reader_line, data);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.FONT_REGULAR = font;
        this.PATTERN_PERICOPE = Pattern.compile("\\(За\\?.*?\\.\\)\\ ");
        this.PATTERN_DICT = Pattern.compile("[^!\\(\\),\\./\\[\\]:;‚„‘’“”–—•«¬­»·\\n\\r\\t ]+");
        this.COLOR_LINE_GHOST = context.getResources().getColor(R.color.reader_line_ghost);
        this.COLOR_LINE_GHOST_NIGHT = context.getResources().getColor(R.color.reader_line_ghost_night);
        this.COLOR_LINE_NUMBER = context.getResources().getColor(R.color.reader_line_number);
        this.COLOR_LINE_PERICOPE = context.getResources().getColor(R.color.reader_line_pericope);
        this.COLOR_LINE_TEXT = context.getResources().getColor(R.color.reader_line_text);
        this.COLOR_LINE_TEXT_NIGHT = context.getResources().getColor(R.color.reader_line_text_night);
        this.otl = otl;
        setOptions(options);
    }

    public void setOptions(Options options) {
        this.options = options;
        colorLineGhost = (options.fullscreen && options.daytime < 0) ? COLOR_LINE_GHOST_NIGHT : COLOR_LINE_GHOST;
        colorLineText = (options.fullscreen && options.daytime < 0) ? COLOR_LINE_TEXT_NIGHT : COLOR_LINE_TEXT;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SimpleHolder holder = null;

        if (options.dictmode) { //Dictionary mode enabled
            if (convertView != null && convertView.getTag() instanceof LinkedHolder) {
                holder = (LinkedHolder) row.getTag();
            } else {
                row = inflater.inflate(R.layout.reader_linkedline, parent, false);
                holder = new LinkedHolder(row);
                row.setTag(holder);
            }
            row.setOnTouchListener(otl);
        } else {
            if (convertView != null && convertView.getTag() instanceof SimpleHolder) {
                holder = (SimpleHolder) row.getTag();
            } else {
                row = inflater.inflate(R.layout.reader_line, parent, false);
                holder = new SimpleHolder(row);
                row.setTag(holder);
            }
        }

        Line model = this.getItem(position);
        if (model.isGhost() && !options.fullChapter) { //Do not need to show current line
            return new ViewStub(getContext());
        }

        int rusmode = isRusTextPresent ? options.rusmode : -1;

        if (rusmode != -1 && model.getRustext() == null) { //Russian text not found
            rusmode = -1;
            isRusTextPresent = false;
            if (onRustextNotFoundListener != null) {
                onRustextNotFoundListener.rustextNotFound();
            }
        }

        if (options.lineNumbers && position < getCount() - 1) { //Show line numbers
            holder.getNumber().setVisibility(View.VISIBLE);
            holder.getNumber().setTextColor(model.isGhost() ? colorLineGhost : COLOR_LINE_NUMBER);
            if (rusmode != 1) { //CS numbers
                holder.getNumber().setTypeface(FONT_REGULAR);
                holder.getNumber().setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontText);
                holder.getNumber().setText(model.getNumber());
            } else { //Russian numbers
                holder.getNumber().setTypeface(Typeface.DEFAULT);
                holder.getNumber().setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontRustext);
                holder.getNumber().setText(String.valueOf(position + 1));
            }
        } else { //Do not show line numbers
            holder.getNumber().setVisibility(View.GONE);
        }

        if (rusmode != 1) { //Display CS text (with or w/o Russian text)
            holder.getText().setVisibility(View.VISIBLE);
            holder.getText().setTypeface(FONT_REGULAR);
            holder.getText().setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontText);
            holder.getText().setTextColor(model.isGhost() ? colorLineGhost : colorLineText);
            SpannableString str = new SpannableString(model.getText());
            if (options.chPericopes) { //Need to show pericopes
                Matcher matcher = PATTERN_PERICOPE.matcher(model.getText());
                if (matcher.find()) {
                    StringBuilder sb = new StringBuilder(model.getText());
                    sb.replace(matcher.start(), matcher.end(), sb.substring(matcher.start() + 1, matcher.end() - 2).replace('_', ' ').concat(" "));
                    str = new SpannableString(sb.toString());
                    str.setSpan(new ForegroundColorSpan(model.isGhost() ? colorLineGhost : COLOR_LINE_PERICOPE), matcher.start(), matcher.end() - 2,
                            Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
            } else { //Do not need to show pericopes
                str = new SpannableString(model.getText().replaceAll(PATTERN_PERICOPE.pattern(), ""));
            }
            if (options.dictmode) { //Dictionary mode enabled
                Matcher m = PATTERN_DICT.matcher(str);
                while (m.find()) {
                    str.setSpan(new DictClickableSpan(m.group()), m.start(), m.end(), 0);
                }
            }
            holder.getText().setText(str);
        } else { //Display only Russian text
            holder.getText().setVisibility(View.GONE);
        }

        if (rusmode != -1) { //Display russian text (instead of or in parallel)
            holder.getRustext().setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontRustext);
            holder.getRustext().setTextColor(model.isGhost() ? colorLineGhost : colorLineText);
            holder.getRustext().setText(model.getRustext());
            holder.getRustext().setVisibility(View.VISIBLE);
        } else { //Do not display russian text
            holder.getRustext().setVisibility(View.GONE);
        }

        return row;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    //------------------------------------------------------------------------------------------

    /**
     * Own implementation of ClickableSpan passing chapter index
     * @author Semeniuk A.D.
     */
    private class DictClickableSpan extends ClickableSpan {
        private String word;
        private final int COLOR = 0xffc080ff;

        public DictClickableSpan(String word) {
            this.word = word;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(COLOR);
            ds.setUnderlineText(true);
        }

        @Override
        public void onClick(View widget) {
            if (onDictClickedListener != null) {
                onDictClickedListener.dictClicked(word);
            }
        }
    }

    //------------------------------------------------------------------------------------------

    /**
     * Listener for dictionary events;
     * @author Semeniuk A.D.
     */
    public interface OnDictClickedListener {
        public void dictClicked(String word);
    }

    private OnDictClickedListener onDictClickedListener;

    public void setOnDictClickedListener(OnDictClickedListener onDictClickedListener) {
        this.onDictClickedListener = onDictClickedListener;
    }

    /**
     * Listener for dictionary events;
     * @author Semeniuk A.D.
     */
    public interface OnRustextNotFoundListener {
        public void rustextNotFound();
    }

    private OnRustextNotFoundListener onRustextNotFoundListener;

    public void setOnRustextNotFoundListener(OnRustextNotFoundListener onRustextNotFoundListener) {
        this.onRustextNotFoundListener = onRustextNotFoundListener;
    }

    //------------------------------------------------------------------------------------------

    /**
     * Temporary holder for reader lines
     * @author Semeniuk A.D.
     */
    private class SimpleHolder {

        protected View base;
        protected TextView number = null;
        private ReaderTextView text = null;
        protected TextView rustext = null;

        /**
         * Constructor
         * @param base Parent view
         */
        public SimpleHolder(View base) {
            this.base = base;
        }

        public TextView getNumber() {
            if (number == null) {
                number = (TextView) base.findViewById(R.id.reader_line_number);
            }
            return (number);
        }

        public ReaderTextView getText() {
            if (text == null) {
                text = (ReaderTextView) base.findViewById(R.id.reader_line_text);
            }
            return (text);
        }

        public TextView getRustext() {
            if (rustext == null) {
                rustext = (TextView) base.findViewById(R.id.reader_line_rustext);
            }
            return (rustext);
        }
    }

    /**
     * Temporary holder for reader lines with links
     * @author Semeniuk A.D.
     */
    private class LinkedHolder extends SimpleHolder {

        private ReaderLinkedTextView text = null;

        /**
         * Constructor
         * @param base Parent view
         */
        public LinkedHolder(View base) {
            super(base);
        }

        @Override
        public ReaderLinkedTextView getText() {
            if (text == null) {
                text = (ReaderLinkedTextView) base.findViewById(R.id.reader_line_text);
            }
            return (text);
        }

    }

}