package bibcs.android.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import bibcs.android.activity.R;
import bibcs.android.model.Location;

public class BookmarksAdapter extends ArrayAdapter<Location> {
	private LayoutInflater inflater = null;
	private OnBookmarkClickListener onItemClickListener = null;

	public BookmarksAdapter(Context context, List<Location> data, OnBookmarkClickListener listener) {
		super(context, R.layout.bookmarks_item, data);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.onItemClickListener = listener;
	}

	public void setOnItemClickListener(OnBookmarkClickListener l) {
		this.onItemClickListener = l;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder wrapper = null;

		if (row == null) {
			row = inflater.inflate(R.layout.bookmarks_item, parent, false);
			wrapper = new ViewHolder(row);
			row.setTag(R.id.bookmarks_item_name, wrapper);
		} else {
			wrapper = (ViewHolder) row.getTag(R.id.bookmarks_item_name);
		}

		Location model = this.getItem(position);
		wrapper.getName().setText(model.getTitle());
		wrapper.getDelete().setTag(position);
		wrapper.getDelete().setOnClickListener(deleteClickListener);
		row.setOnClickListener(rowClickListener);
		row.setTag(R.id.bookmarks_item_delete, position);
		row.setFocusable(false);
		return row;
	}

	@Override
	public void add(Location object) {
		super.add(object);
		this.notifyDataSetChanged();
	}

	@Override
	public void remove(Location object) {
		super.remove(object);
		this.notifyDataSetChanged();
	}

	private OnClickListener rowClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (onItemClickListener != null) {
				onItemClickListener.onItemClick(v, (Integer) v.getTag(R.id.bookmarks_item_delete));
			}
		}
	};

	private OnClickListener deleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			BookmarksAdapter.this.remove(BookmarksAdapter.this.getItem((Integer) v.getTag()));
		}
	};

	/**
	 * Interface for item click listener
	 * @author Semeniuk A.D.
	 */
	public interface OnBookmarkClickListener {
		public void onItemClick(View v, int position);
	}
	
	//--------------------------------------------------------------------------
	
	/**
	 * Temporary view holder
	 * @author Semeniuk A.D.
	 */
	private class ViewHolder {
		private View base;
		private TextView name = null;
		private ImageButton delete = null;

		/**
		 * Constructor
		 * @param base Parent view
		 */
		public ViewHolder(View base) {
			this.base = base;
		}

		public ImageButton getDelete() {
			if (delete == null) {
				delete = (ImageButton) base.findViewById(R.id.bookmarks_item_delete);
			}
			return (delete);
		}

		public TextView getName() {
			if (name == null) {
				name = (TextView) base.findViewById(R.id.bookmarks_item_name);
			}
			return (name);
		}

	}

}
