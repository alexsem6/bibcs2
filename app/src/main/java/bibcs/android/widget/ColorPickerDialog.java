package bibcs.android.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.StateSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import bibcs.android.activity.R;

public class ColorPickerDialog extends AlertDialog implements OnSeekBarChangeListener, OnClickListener {

	public interface OnColorPickerListener {
		public void onColorPicked(int color);
	}

	private SeekBar sbRed;
	private SeekBar sbGreen;
	private SeekBar sbBlue;
	private EditText etRed;
	private EditText etGreen;
	private EditText etBlue;
	private int color;
	private GradientDrawable mPreviewDrawable;
	private OnColorPickerListener listener;
	private Resources resources;

	public ColorPickerDialog(Context context, int color, OnColorPickerListener listener) {
		super(context);
		this.listener = listener;

		this.resources = context.getResources();
		setTitle(resources.getText(R.string.color_picker_title));
		setButton(BUTTON_POSITIVE, resources.getText(R.string.dialog_ok), this);
		setButton(BUTTON_NEGATIVE, resources.getText(R.string.dialog_cancel), this);
		View root = LayoutInflater.from(context).inflate(R.layout.color_picker, null);
		setView(root);

		View preview = root.findViewById(R.id.color_picker_preview);
		mPreviewDrawable = new GradientDrawable();
		// 2 pix more than color_picker_frame's radius
		mPreviewDrawable.setCornerRadius(5);
		Drawable[] layers = new Drawable[] { mPreviewDrawable, resources.getDrawable(R.drawable.color_picker_frame), };
		preview.setBackgroundDrawable(new LayerDrawable(layers));

		sbRed = (SeekBar) root.findViewById(R.id.color_picker_red_seek);
		sbGreen = (SeekBar) root.findViewById(R.id.color_picker_green_seek);
		sbBlue = (SeekBar) root.findViewById(R.id.color_picker_blue_seek);
		etRed = (EditText) root.findViewById(R.id.color_picker_red_edit);
		etGreen = (EditText) root.findViewById(R.id.color_picker_green_edit);
		etBlue = (EditText) root.findViewById(R.id.color_picker_blue_edit);
		etRed.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				int value = 0;
				try {
					value = Integer.valueOf(etRed.getText().toString());
				} catch (NumberFormatException ex) {
				}
				if (value > 255) {
					etRed.setText("255");
				} else {
					sbRed.setProgress(value);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		etGreen.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				int value = 0;
				try {
					value = Integer.valueOf(etGreen.getText().toString());
				} catch (NumberFormatException ex) {
				}
				if (value > 255) {
					etGreen.setText("255");
				} else {
					sbGreen.setProgress(value);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		etBlue.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				int value = 0;
				try {
					value = Integer.valueOf(etBlue.getText().toString());
				} catch (NumberFormatException ex) {
				}
				if (value > 255) {
					etBlue.setText("255");
				} else {
					sbBlue.setProgress(value);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		this.color = color;
		setupSeekBar(sbRed, resources.getString(R.string.color_picker_r), Color.red(color), etRed);
		setupSeekBar(sbGreen, resources.getString(R.string.color_picker_g), Color.green(color), etGreen);
		setupSeekBar(sbBlue, resources.getString(R.string.color_picker_b), Color.blue(color), etBlue);
		
		updatePreview(color);
	}

	private void setupSeekBar(SeekBar seekBar, String text, int value, EditText edit) {
		seekBar.setProgressDrawable(new TextSeekBarDrawable(text, value < seekBar.getMax() / 2));
		seekBar.setProgress(value);
		seekBar.setOnSeekBarChangeListener(this);
		seekBar.setTag(edit);
		edit.setText(Integer.toString(value));
	}

	private void update() {
		color = Color.rgb(sbRed.getProgress(), sbGreen.getProgress(), sbBlue.getProgress());
		updatePreview(color);
	}

	private void updatePreview(int color) {
		mPreviewDrawable.setColor(color);
		mPreviewDrawable.invalidateSelf();
	}

	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		update();
		if (fromUser) {
			((EditText) seekBar.getTag()).setText(Integer.toString(progress));
		}
	}

	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	public void onClick(DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			listener.onColorPicked(color);
		}
		dismiss();
	}

	private static final int[] STATE_FOCUSED = { android.R.attr.state_focused };
	private static final int[] STATE_PRESSED = { android.R.attr.state_pressed };

	/**
	 * Class for drawing custom seek bars
	 */
	private class TextSeekBarDrawable extends Drawable implements Runnable {

		private static final long DELAY = 50;
		private String text;
		private Drawable progress;
		private Paint paint;
		private Paint outlinePaint;
		private float textWidth;
		private boolean active;
		private float textScaleX;
		private int delta;
		private ScrollAnimation animation;

		public TextSeekBarDrawable(String text, boolean labelOnRight) {
			this.text = text;
			progress = resources.getDrawable(android.R.drawable.progress_horizontal);
			paint = new Paint(Paint.ANTI_ALIAS_FLAG);
			paint.setTypeface(Typeface.DEFAULT_BOLD);
			paint.setTextSize(16);
			paint.setColor(0xff000000);
			outlinePaint = new Paint(paint);
			outlinePaint.setStyle(Style.STROKE);
			outlinePaint.setStrokeWidth(3);
			outlinePaint.setColor(0xbbffc300);
			outlinePaint.setMaskFilter(new BlurMaskFilter(1, Blur.NORMAL));
			textWidth = outlinePaint.measureText(text);
			textScaleX = labelOnRight ? 1 : 0;
			animation = new ScrollAnimation();
		}

		@Override
		protected void onBoundsChange(Rect bounds) {
			progress.setBounds(bounds);
		}

		@Override
		protected boolean onStateChange(int[] state) {
			active = StateSet.stateSetMatches(STATE_FOCUSED, state) | StateSet.stateSetMatches(STATE_PRESSED, state);
			invalidateSelf();
			return false;
		}

		@Override
		public boolean isStateful() {
			return true;
		}

		@Override
		protected boolean onLevelChange(int level) {
			if (level < 4000 && delta <= 0) {
				delta = 1;
				animation.startScrolling(textScaleX, 1);
				scheduleSelf(this, SystemClock.uptimeMillis() + DELAY);
			} else if (level > 6000 && delta >= 0) {
				delta = -1;
				animation.startScrolling(textScaleX, 0);
				scheduleSelf(this, SystemClock.uptimeMillis() + DELAY);
			}
			return progress.setLevel(level);
		}

		@Override
		public void draw(Canvas canvas) {
			progress.draw(canvas);

			if (animation.hasStarted() && !animation.hasEnded()) {
				// pending animation
				animation.getTransformation(AnimationUtils.currentAnimationTimeMillis(), null);
				textScaleX = animation.getCurrent();
			}

			Rect bounds = getBounds();
			float x = 6 + textScaleX * (bounds.width() - textWidth - 6 - 6);
			float y = (bounds.height() + paint.getTextSize()) / 2;
			outlinePaint.setAlpha(active ? 255 : 255 / 2);
			paint.setAlpha(active ? 255 : 255 / 2);
			canvas.drawText(text, x, y, outlinePaint);
			canvas.drawText(text, x, y, paint);
		}

		@Override
		public int getOpacity() {
			return PixelFormat.TRANSLUCENT;
		}

		@Override
		public void setAlpha(int alpha) {
		}

		@Override
		public void setColorFilter(ColorFilter cf) {
		}

		public void run() {
			animation.getTransformation(AnimationUtils.currentAnimationTimeMillis(), null);
			// close interpolation of mTextX
			textScaleX = animation.getCurrent();
			if (!animation.hasEnded()) {
				scheduleSelf(this, SystemClock.uptimeMillis() + DELAY);
			}
			invalidateSelf();
		}
	}

	/**
	 * Animation for SeekBar titles to scroll left and right
	 */
	private class ScrollAnimation extends Animation {
		private static final long DURATION = 750;
		private float from;
		private float to;
		private float current;

		public ScrollAnimation() {
			setDuration(DURATION);
			setInterpolator(new DecelerateInterpolator());
		}

		public void startScrolling(float from, float to) {
			this.from = from;
			this.to = to;
			startNow();
		}

		@Override
		protected void applyTransformation(float interpolatedTime, Transformation t) {
			current = from + (to - from) * interpolatedTime;
		}

		public float getCurrent() {
			return current;
		}
	}
}
