package bibcs.android.widget;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;

public class ReaderLinkedTextView extends ReaderTextView {

	public ReaderLinkedTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public ReaderLinkedTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ReaderLinkedTextView(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		this.setMovementMethod(new LinkMovementMethod());
	}
}
