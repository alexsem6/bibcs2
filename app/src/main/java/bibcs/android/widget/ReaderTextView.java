package bibcs.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class ReaderTextView extends TextView {

	public ReaderTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ReaderTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ReaderTextView(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		this.setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight() + Math.round(this.getPaint().descent()));
	}

}
