package bibcs.android.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import bibcs.android.activity.R;

public class PageIndex extends LinearLayout {
	private final int INDEX_COUNT = 9;
	private final int INDEX_CENTER = INDEX_COUNT / 2;

	private boolean night = false;
	private final int COLOR_INDEX;
	private final int COLOR_INDEX_NIGHT;
	private final int COLOR_CURRENT_INDEX;
	private final float SIZE_INDEX;
	private final float SIZE_CURRENT_INDEX;
	
	private TextView[] indexes = new TextView[9];

	public PageIndex(Context context) {
		this(context, null);
	}

	public PageIndex(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.COLOR_INDEX = getResources().getColor(R.color.reader_index_plain);
		this.COLOR_INDEX_NIGHT = getResources().getColor(R.color.reader_index_plain_night);
		this.COLOR_CURRENT_INDEX = getResources().getColor(R.color.reader_index_current);
		this.SIZE_INDEX = getResources().getDimension(R.dimen.reader_index_plain);
		this.SIZE_CURRENT_INDEX = getResources().getDimension(R.dimen.reader_index_current);

		for (int i = 0; i < INDEX_COUNT; i++) {
			indexes[i] = new TextView(context);
			LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			params.weight = 1f;
			indexes[i].setGravity(Gravity.CENTER);
			indexes[i].setTextColor(COLOR_INDEX);
			indexes[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, SIZE_INDEX);
			this.addView(indexes[i], params);
		}
		indexes[INDEX_CENTER].setTextColor(COLOR_CURRENT_INDEX);
		indexes[INDEX_CENTER].setTextSize(TypedValue.COMPLEX_UNIT_PX, SIZE_CURRENT_INDEX);
		indexes[INDEX_CENTER].setTypeface(indexes[INDEX_CENTER].getTypeface(), Typeface.BOLD);
	}
	
		public void setNight(boolean night) {
		if (this.night != night) {
			this.night = night;
			this.getBackground().setLevel(night ? 1 : 0);
			for (int i = 0; i < INDEX_COUNT; i++) {
				indexes[i].setTextColor(night ? COLOR_INDEX_NIGHT : COLOR_INDEX);
			}
			indexes[INDEX_CENTER].setTextColor(COLOR_CURRENT_INDEX);
		}
	}

	/**
	 * Sets the data that should be displayed in page index
	 * @param numbers Array of index numbers
	 * @param position Current index position (relative)
	 */
	public void setData(Integer[] numbers, int position) {
		indexes[INDEX_CENTER].setText(numbers[position].toString());
		if (position > INDEX_CENTER) {
			indexes[0].setText("...");
			for (int i = 1; i < INDEX_CENTER; i++) {
				indexes[INDEX_CENTER - i].setText(numbers[position - i].toString());
			}
		} else {
			for (int i = 1; i <= INDEX_CENTER; i++) {
				indexes[INDEX_CENTER - i].setText(position >= i ? numbers[position - i].toString() : "");
			}
		}
		if (numbers.length - position > INDEX_COUNT - INDEX_CENTER) {
			indexes[INDEX_COUNT - 1].setText("...");
			for (int i = INDEX_CENTER + 1; i < INDEX_COUNT - 1; i++) {
				indexes[i].setText(numbers[position - INDEX_CENTER + i].toString());
			}
		} else {
			for (int i = INDEX_CENTER + 1; i < INDEX_COUNT; i++) {
				indexes[i].setText(position - INDEX_CENTER + i < numbers.length ? numbers[position - INDEX_CENTER + i].toString() : "");
			}
		}
	}
	
	/**
	 * Sets the data that should be displayed in page index
	 * @param size Number of pages
	 * @param position Current page
	 */
	public void setSimpleData(int size, int position) {
		indexes[INDEX_CENTER].setText(Integer.toString(position + 1));
		if (position > INDEX_CENTER) {
			indexes[0].setText("...");
			for (int i = 1; i < INDEX_CENTER; i++) {
				indexes[INDEX_CENTER - i].setText(Integer.toString(position - i + 1));
			}
		} else {
			for (int i = 1; i <= INDEX_CENTER; i++) {
				indexes[INDEX_CENTER - i].setText(position >= i ? Integer.toString(position - i + 1) : "");
			}
		}
		if (size - position > INDEX_COUNT - INDEX_CENTER) {
			indexes[INDEX_COUNT - 1].setText("...");
			for (int i = INDEX_CENTER + 1; i < INDEX_COUNT - 1; i++) {
				indexes[i].setText(Integer.toString(position - INDEX_CENTER + i + 1));
			}
		} else {
			for (int i = INDEX_CENTER + 1; i < INDEX_COUNT; i++) {
				indexes[i].setText(position - INDEX_CENTER + i < size ? Integer.toString(position - INDEX_CENTER + i + 1) : "");
			}
		}
	}
}
