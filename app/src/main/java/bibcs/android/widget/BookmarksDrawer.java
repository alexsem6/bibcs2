package bibcs.android.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.SlidingDrawer;
import bibcs.android.activity.R;

public class BookmarksDrawer extends SlidingDrawer {
	private final int LEFT_MARGIN = 2;
	private Context context;

	private int iconLeft;
	private int iconRight;
	private int openTop;
	private int openBottom;
	private int closeTop;
	private int closeBottom;
	/**
	 * Constructor
	 * @param context Context
	 * @param attrs Attributes (defined via XML)
	 */
	public BookmarksDrawer(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		calculateHandleMargins();
	}
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		calculateHandleMargins();
	}
	
	/**
	 * Performs necessary calculations
	 */
	private void calculateHandleMargins() {
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int handleSize = context.getResources().getDrawable(R.drawable.bookmarks_handle_icon).getMinimumWidth();
		int topMargin = Math.round(context.getResources().getDimension(R.dimen.bookmarks_top_margin));
		openTop = topMargin - 2;
		openBottom = topMargin + handleSize + 2;
		closeTop = display.getHeight() - handleSize - 2;
		closeBottom = display.getHeight() + 1;
		iconLeft = LEFT_MARGIN - 2;
		iconRight = LEFT_MARGIN + handleSize + 2;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (this.isOpened()) { //Drawer opened
			if (event.getRawY() > openBottom) { //Touched drawer content
				return super.onInterceptTouchEvent(event);
			}
			if (event.getRawY() > openTop && event.getRawY() < openBottom && event.getRawX() > iconLeft && event.getRawX() < iconRight) { //Touched icon
				return super.onInterceptTouchEvent(event);
			}
		} else { //Drawer closed
			if (event.getRawY() > closeTop && event.getRawY() < closeBottom && event.getRawX() > iconLeft && event.getRawX() < iconRight) { //Touched icon
				return super.onInterceptTouchEvent(event);
			}
		}
		return true; //Touched outside icon
	}
}