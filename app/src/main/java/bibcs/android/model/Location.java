package bibcs.android.model;

public class Location {

	private LocationFilter filter = null;
	private String title = "";
	private int book = 0;
	private int chapter = 0;
	private int scrollPosition = -1;
	private int scrollOffset = 0;

	/**
	 * Constructor
	 * @param book Book order
	 * @param chapter Chapter order
	 */
	public Location(int book, int chapter) {
		this.book = book;
		this.chapter = chapter;
	}

	/**
	 * Clones location to get its exact copy
	 * @param location Location to copy
	 * @return exact copy of specified location
	 */
	public static Location clone(Location location) {
		if (location != null) {
			Location result = new Location(location.getBook(), location.getChapter());
			result.setTitle(location.getTitle());
			result.setScrollPosition(location.getScrollPosition());
			result.setScrollOffset(location.getScrollOffset());
			if (location.isLimited()) {
				result.setFilter(new LocationFilter(location.getFilter().toString()));
			}
			return result;
		}
		return null;
	}

	public boolean isLimited() {
		return filter != null;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getBook() {
		return book;
	}

	public void setBook(int book) {
		this.book = book;
	}

	public int getChapter() {
		return chapter;
	}

	public void setChapter(int chapter) {
		this.chapter = chapter;
	}

	public boolean isScrollSet() {
		return scrollPosition > -1;
	}

	public int getScrollPosition() {
		if (scrollPosition < 0) {
			scrollPosition = 0;
		}
		return scrollPosition;
	}

	public void setScrollPosition(int scrollPosition) {
		this.scrollPosition = scrollPosition;
	}

	public int getScrollOffset() {
		return scrollOffset;
	}

	public void setScrollOffset(int scrollOffset) {
		this.scrollOffset = scrollOffset;
	}

	public LocationFilter getFilter() {
		return filter;
	}

	public void setFilter(LocationFilter filter) {
		this.filter = filter;
	}

}
