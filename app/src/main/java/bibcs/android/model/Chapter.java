package bibcs.android.model;

/**
 * Class that represents one specific chapter
 * @author Semeniuk A.D.
 * 
 */
public class Chapter implements Comparable<Chapter> {
	private int ord;
	private int size;

	public Chapter(int ord) {
		this.ord = ord;
	}

	public int getOrd() {
		return ord;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public int compareTo(Chapter another) {
		if (ord > another.ord) {
			return 1;
		}
		if (ord < another.ord) {
			return -1;
		}
		return 0;
	}
}
