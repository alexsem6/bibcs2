package bibcs.android.model;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class LocationFilter {
	private Map<Integer, Map<Integer, Set<Integer>>> data;

	/**
	 * Constructor
	 * @param data Filter data (3D map)
	 */
	public LocationFilter(Map<Integer, Map<Integer, Set<Integer>>> data) {
		this.data = data;
	}

	/**
	 * Constructor
	 * @param codes List of SearchCodes
	 */
	public LocationFilter(List<SearchCode> codes) {
		this.data = new TreeMap<Integer, Map<Integer, Set<Integer>>>();
		this.addSearchCodes(codes);
	}

	/**
	 * Constructor
	 * @param serialized Previously serialized string
	 */
	public LocationFilter(String serialized) {
		this.data = new TreeMap<Integer, Map<Integer, Set<Integer>>>();
		int book = -1;
		Map<Integer, Set<Integer>> bookData = null;
		int chapter = -1;
		Set<Integer> chapterData = null;
		
		for (String element : serialized.split(";")) {
			if (element.startsWith("**")) { //Book value found
				if (chapter > -1 && chapterData != null) { //Chapter end
					bookData.put(chapter, chapterData);
					chapter = -1;
					chapterData = null;
				}
				if (book > -1 && bookData != null) { //Book end
					this.data.put(book, bookData);
					book = -1;
					bookData = null;
				}
				book = Integer.valueOf(element.substring(2));
				bookData = new TreeMap<Integer, Set<Integer>>();
			} else if (element.startsWith("*")) { //Chapter value found
				if (chapter > -1 && chapterData != null) { //Chapter end;
					bookData.put(chapter, chapterData);
					chapter = -1;
					chapterData = null;
				}
				chapter = Integer.valueOf(element.substring(1));
				chapterData = new TreeSet<Integer>();
			} else { //Line value found
				chapterData.add(Integer.valueOf(element));
			}
		}
		
		if (chapter > -1 && chapterData != null) { //Chapter end
			bookData.put(chapter, chapterData);
		}
		if (book > -1 && bookData != null) { //Book end
			this.data.put(book, bookData);
		}
		
	}

	/**
	 * Adds one search code to the filter
	 * @param code Search code to add
	 */
	public void addSearchCode(SearchCode code) {
		if (!data.containsKey(code.getBook())) { //No tree for current book
			data.put(code.getBook(), new TreeMap<Integer, Set<Integer>>());
		}
		if (!data.get(code.getBook()).containsKey(code.getChapter())) {//No tree for current chapter
			data.get(code.getBook()).put(code.getChapter(), new TreeSet<Integer>());
		}
		data.get(code.getBook()).get(code.getChapter()).add(code.getLine());
	}

	/**
	 * Adds list of search codes to the filter
	 * @param codes List of search codes to add
	 */
	public void addSearchCodes(List<SearchCode> codes) {
		for (SearchCode code : codes) {
			this.addSearchCode(code);
		}
	}

	/**
	 * Removes one particular search code from the filter
	 * @param code Search code to remove
	 */
	public void removeSearchCode(SearchCode code) {
		data.get(code.getBook()).get(code.getChapter()).remove(code.getLine());
		if (data.get(code.getBook()).get(code.getChapter()).isEmpty()) {//No elements in chapter tree
			data.get(code.getBook()).remove(code.getChapter());
		}
		if (data.get(code.getBook()).isEmpty()) { //No elements in book tree
			data.remove(code.getBook());
		}
	}

	/**
	 * Returns list of chapters
	 * @param book Global book order
	 * @return Set of chapter orders or null if no book present
	 */
	public Set<Integer> getChapterList(int book) {
		if (data.containsKey(book)) {
			return data.get(book).keySet();
		}
		return null;
	}

	/**
	 * Returns filter for one specified book
	 * @param book Global book order
	 * @return Book filter or null if no book present
	 */
	public Map<Integer, Set<Integer>> getBookFilter(int book) {
		if (data.containsKey(book)) {
			return data.get(book);
		}
		return null;
	}

	/**
	 * Returns number of books in filter
	 * @return number of books
	 */
	public int getBookCount() {
		return data.size();
	}

	/**
	 * Searches list of books to find position of specific book
	 * @param ord Global book order
	 * @return or -1 if book is not contained in filter
	 */
	public int getBookIndex(int ord) {
		int index = -1;
		for (Integer bookKey : data.keySet()) {
			index++;
			if (bookKey == ord) {
				break;
			}
		}
		return index;
	}

	/**
	 * Return book global order on specified position
	 * @param position Position to look at
	 * @return Global book order or -1 if out of bounds
	 */
	public int getBookOrd(int position) {
		int ord = -1;
		int index = -1;
		for (Integer bookKey : data.keySet()) {
			index++;
			if (index == position) {
				ord = bookKey;
				break;
			}
		}
		return ord;
	}

	/**
	 * Represent LocationFilter in string format
	 * @return serialized location filter
	 */
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Integer book : data.keySet()) {
			result.append('*').append('*').append(book).append(';');
			for (Integer chapter : data.get(book).keySet()) {
				result.append('*').append(chapter).append(';');
				for (Integer line : data.get(book).get(chapter)) {
					result.append(line).append(';');
				}
			}
		}
		return result.toString();
	}

}
