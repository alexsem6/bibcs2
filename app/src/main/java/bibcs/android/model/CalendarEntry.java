package bibcs.android.model;

import java.util.ArrayList;

/**
 * Class that stores information about 1 particular calendar entry
 * @author Semeniuk A.D.
 */
public class CalendarEntry {

	private String title;
//	private int fastIndex; //0 - no fasting , 1 - full restraint, 2 - xerophagy, 3 - no oil, 4 - oil, 5 - fish, 6 - caviar, 7 - no meat, 8 - fasting cancelled
	private String fasting;
	private boolean holiday;
	private ArrayList<DailyReading> readingsApostle = new ArrayList<DailyReading>();
	private ArrayList<DailyReading> readingsGospel = new ArrayList<DailyReading>();
	private ArrayList<DailyReading> readingsOld = new ArrayList<DailyReading>();

//	public int getFastIndex() {
//		return fastIndex;
//	}
//
//	public void setFastIndex(int fastIndex) {
//		this.fastIndex = fastIndex;
//	}
//
	public String getFasting() {
		return fasting;
	}

	public void setFasting(String fasting) {
		this.fasting = fasting;
	}

	public boolean isHoliday() {
		return holiday;
	}

	public void setHoliday(boolean holiday) {
		this.holiday = holiday;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<DailyReading> getReadingsApostle() {
		return readingsApostle;
	}

	public void setReadingsApostle(ArrayList<DailyReading> readingsApostle) {
		this.readingsApostle = readingsApostle;
	}

	public ArrayList<DailyReading> getReadingsGospel() {
		return readingsGospel;
	}

	public void setReadingsGospel(ArrayList<DailyReading> readingsGospel) {
		this.readingsGospel = readingsGospel;
	}

	public ArrayList<DailyReading> getReadingsOld() {
		return readingsOld;
	}

	public void setReadingsOld(ArrayList<DailyReading> readingsOld) {
		this.readingsOld = readingsOld;
	}

	public void addApostleReading(String link, String comment) {
		this.readingsApostle.add(new DailyReading(link, comment));
	}

	public void addGospelReading(String link, String comment) {
		this.readingsGospel.add(new DailyReading(link, comment));
	}

	public void addOldReading(String link, String comment) {
		this.readingsOld.add(new DailyReading(link, comment));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("---- ").append(title).append(" ----\n");
		sb.append(isHoliday() ? "Holiday" : "Regular").append(": ").append(fasting).append("\n");
		if (readingsOld.size() > 0) { //Old Testament readings
			sb.append("-- Old: \n");
			for (DailyReading dr : readingsOld) {
				sb.append(dr.getLink());
				if (dr.getComment().length() > 0) {
					sb.append(" (").append(dr.getComment()).append(")");
				}
				sb.append("\n");
			}
		}
		if (readingsApostle.size() > 0) { //Apostle readings
			sb.append("-- Apos: \n");
			for (DailyReading dr : readingsApostle) {
				sb.append(dr.getLink());
				if (dr.getComment().length() > 0) {
					sb.append(" (").append(dr.getComment()).append(")");
				}
				sb.append("\n");
			}
		}
		if (readingsGospel.size() > 0) { //Gospel readings
			sb.append("-- Gosp: \n");
			for (DailyReading dr : readingsGospel) {
				sb.append(dr.getLink());
				if (dr.getComment().length() > 0) {
					sb.append(" (").append(dr.getComment()).append(")");
				}
				sb.append("\n");
			}
		}
		sb.append("--------\n");
		return sb.toString();
	}
}
