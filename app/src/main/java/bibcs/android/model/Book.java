package bibcs.android.model;

import java.util.ArrayList;

/**
 * Class that represents one specific book
 * @author Semeniuk A.D.
 */
public class Book implements Comparable<Book> {
	private int ord;

	private String name;
	private String name_cs;
	private String chapterName;
	private String chapterName_cs;

	private String shortestName;
	private ArrayList<String> shortNames;

	private ArrayList<Chapter> chapters;

	public Book(int ord) {
		this.ord = ord;
		this.shortNames = new ArrayList<String>();
		this.chapters = new ArrayList<Chapter>();
	}

	public int getOrd() {
		return ord;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName_cs() {
		return name_cs;
	}

	public void setName_cs(String name_cs) {
		this.name_cs = name_cs;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public String getChapterName_cs() {
		return chapterName_cs;
	}

	public void setChapterName_cs(String chapterName_cs) {
		this.chapterName_cs = chapterName_cs;
	}

	public String getShortestName() {
		return shortestName;
	}

	public void setShortestName(String shortestName) {
		this.shortestName = shortestName;
	}

	public ArrayList<String> getShortNames() {
		return shortNames;
	}

	public void setShortNames(ArrayList<String> shortNames) {
		this.shortNames = shortNames;
	}

	public ArrayList<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(ArrayList<Chapter> chapters) {
		this.chapters = chapters;
	}
	
	/**
	 * Returns list of available chapter numbers
	 * @return List of chapter numbers
	 */
	public Integer[] getChapterNumbers() {
		Integer[] result = new Integer[chapters.size()];
		for (int i = 0; i < chapters.size(); i++) {			
			result[i] = chapters.get(i).getOrd();
		}
		return result;
	}

	@Override
	public int compareTo(Book another) {
		if (ord > another.ord) {
			return 1;
		}
		if (ord < another.ord) {
			return -1;
		}
		return 0;
	}

}
