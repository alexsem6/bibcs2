package bibcs.android.model;

public class Options {

	public boolean lineNumbers; 
	public boolean chPericopes;
	public boolean fullChapter;
	public int rustextMode;
	public int backgroundColor;
	
	public boolean newTab;
	public boolean screenOn;
	public int historySize;
	public boolean fullscreenMode;
	public boolean dictionaryIntegr;

	public float fontContents;
	public float fontBook;
	public float fontText;
	public float fontRustext;
	
	public boolean fullscreen = false;
	public int daytime = 0;
	public boolean dictmode = false;
	public int rusmode = -1;
	public boolean quickrus = false;
	
}
