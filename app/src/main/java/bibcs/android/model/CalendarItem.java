package bibcs.android.model;

public class CalendarItem {

	public static enum Types {
		HEADER, REGULAR, HOLIDAY, PREVIOUS, NEXT
	};

	private Types type = Types.REGULAR;
	private String value = "";
//	private int fasting = 0;

	public CalendarItem(Types type, String value, int fasting) {
		this.type = type;
		this.value = value;
//		this.fasting = fasting;
	}

	public CalendarItem(Types type, String value) {
		this.type = type;
		this.value = value;
	}

	public Types getType() {
		return type;
	}

	public void setType(Types type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

//	public int getFasting() {
//		return fasting;
//	}
//
//	public void setFasting(int fasting) {
//		this.fasting = fasting;
//	}

}
