package bibcs.android.model;

/**
 * Class that represents a single line (number and text)
 * @author Semeniuk A.D.
 */
public class Line {
	/**
	 * String representation of line number
	 */
	private String number;
	/**
	 * Text contained within line
	 */
	private String text;
	/**
	 * Russian translation
	 */
	private String rustext = null;
	/**
	 * Shows whether current line should be half-visible
	 */
	private boolean ghost;

	/**
	 * Constructor (blank line)
	 */
	public Line() {
		this.number = "";
		this.text = "";
		this.rustext = "";
		this.ghost = false;
	}
	
	/**
	 * Constructor
	 * @param number Line number
	 * @param text Line data
	 */
	public Line(String number, String text) {
		this.number = number;
		this.text = text;
		this.ghost = false;
	}

	/**
	 * Constructor
	 * @param number Line number
	 * @param text Line data
	 * @param ghost true if drawn dimmed, false otherwise
	 */
	public Line(String number, String text, boolean ghost) {
		this.number = number;
		this.text = text;
		this.ghost = ghost;
	}

	public String getNumber() {
		return number;
	}

	public String getText() {
		return text;
	}

	public String getRustext() {
		return rustext;
	}

	public void setRustext(String rustext) {
		this.rustext = rustext;
	}

	public boolean isGhost() {
		return ghost;
	}
}
