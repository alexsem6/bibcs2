package bibcs.android.model;

import java.util.ArrayList;
import java.util.Calendar;

public class Readings {

	private Calendar date;
	private int length;

	private ArrayList<Index> index = new ArrayList<Index>();

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 * Resets date (to '1900-01-01' value)
	 */
	public void resetDate() {
		if (date == null) {
			date = Calendar.getInstance();
		}
		date.set(1900, Calendar.JANUARY, 1);
	}

	/**
	 * Defines whether date is reset
	 * @return true if date is '1900-01-01'
	 */
	public boolean isDateSet() {
		if (date == null) {
			resetDate();
		}
		return (date.get(Calendar.YEAR) != 1900 || date.get(Calendar.MONTH) != Calendar.JANUARY || date.get(Calendar.DAY_OF_MONTH) != 1);
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void addIndex(int book, int chapter) {
		index.add(new Index(book, chapter));
	}

	public void clearIndex() {
		index.clear();
	}

	public Index getIndex(int i) {
		return index.get(i);
	}

	public class Index {
		public int book;
		public int chapter;

		public Index(int book, int chapter) {
			this.book = book;
			this.chapter = chapter;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(String.format("%1$te %1$tb %1$tY:\n", date));
		for (Index i : index) {
			sb.append(String.format("  %d %d\n", i.book, i.chapter));
		}
		return sb.toString();
	}

}
