package bibcs.android.model;

/**
 * Class that stores information about 1 particular reading
 * @author Semeniuk A.D.
 */
public class DailyReading {

	private String link = "";
	private String comment = "";

	public DailyReading() {
	}

	public DailyReading(String link, String comment) {
		this.link = link;
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
