package bibcs.android.transfer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import bibcs.android.model.Book;
import bibcs.android.model.Chapter;
import bibcs.android.model.Location;
import bibcs.android.model.LocationFilter;
import bibcs.android.model.Readings;

public class DatabaseAdapter {

	//Singleton object
	private static DatabaseAdapter instance;
	//Variable to hold the database instance
	private SQLiteDatabase db;
	// Database open/upgrade helper
	private SQLiteOpenHelper dbHelper;

	private int connectionCount = 0;
	private Object lock = new Object();

	private final SimpleDateFormat ISO_DATE = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Instantiates singleton object
	 * @param context Context used by the database
	 */
	public static DatabaseAdapter getInstance(Context context) {
		if (instance == null) {
			instance = new DatabaseAdapter(context);
		}
		return instance;
	}

	/**
	 * Constructor
	 * @param context Context used by the database
	 */
	private DatabaseAdapter(Context context) {
		this.dbHelper = new DatabaseHelper(context);
	}
	
	/**
	 * Opens connection to the database for reading
	 * @throws SQLException if connection was not established
	 */
	public void openRead() throws SQLException {
		synchronized (lock) {
			connectionCount++;
			db = dbHelper.getReadableDatabase();
		}
	}

	/**
	 * Opens connection to the database for writing
	 * @throws SQLException if connection was not established
	 */
	public void openWrite() throws SQLException {
		synchronized (lock) {
			connectionCount++;
			db = dbHelper.getWritableDatabase();
		}
	}

	/**
	 * Close connection to the database
	 */
	public void close() {
		synchronized (lock) {
			connectionCount--;
			if (connectionCount <= 0) {
				connectionCount = 0;
				db.close();
			}
		}
	}

	/**
	 * Inserts new book to the local database
	 * @param book Book to insert
	 */
	public void insertBook(Book book) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			values.put("ord", book.getOrd());
			values.put("name", book.getName());
			values.put("name_cs", book.getName_cs());
			values.put("chapterName", book.getChapterName());
			values.put("chapterName_cs", book.getChapterName_cs());
			values.put("shortestName", book.getShortestName());

			long id = db.insert("Book", null, values);
			if (id > -1) {

				values = new ContentValues();
				values.put("bookID", id);
				for (String sname : book.getShortNames()) {
					values.put("sname", sname.toLowerCase());
					db.insert("ShortName", null, values);
				}

				values = new ContentValues();
				values.put("bookID", id);
				for (Chapter chapter : book.getChapters()) {
					values.put("ord", chapter.getOrd());
					values.put("size", chapter.getSize());
					db.insert("Chapter", null, values);
				}
			}
		}
	}

	/**
	 * Inserts list of books to the local database
	 * @param books Books to insert
	 */
	public void insertBookArray(ArrayList<Book> books) {
		synchronized (lock) {
			try {
				db.beginTransaction();
				db.delete("ShortName", null, null);
				db.delete("Chapter", null, null);
				db.delete("Book", null, null);
				for (Book book : books) {
					insertBook(book);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	/**
	 * Returns list of chapters from local database
	 * @param bookId Book identifier
	 * @param filter if present defines list of chapters which should be fetched
	 * @return list of chapters from the specified book
	 */
	private ArrayList<Chapter> selectChaptersList(int bookId, Set<Integer> filter) {
		synchronized (lock) {
			ArrayList<Chapter> result = new ArrayList<Chapter>();
			String[] chapterColumns = { "id", "ord", "size" };
			String[] selectionArgs = { String.valueOf(bookId) };

			Cursor chapterCursor = db.query("Chapter", chapterColumns, "bookID = ?", selectionArgs, null, null, "ord");
			if (chapterCursor.moveToFirst()) { //At least one row present		
				do {
					if (filter != null && !filter.contains(chapterCursor.getInt(1))) { //Current chapter is not needed
						continue;
					}
					Chapter chapter = new Chapter(chapterCursor.getInt(1));
					chapter.setSize(chapterCursor.getInt(2));
					result.add(chapter);
				} while (chapterCursor.moveToNext());
			}
			chapterCursor.close();
			return result;
		}
	}

	/**
	 * Returns list of books with list of chapters from local database
	 * @param from lower bound for book list
	 * @param to upper bound of book list
	 * @return list of all books
	 */
	public ArrayList<Book> selectContents(int from, int to) {
		synchronized (lock) {
			ArrayList<Book> result = new ArrayList<Book>();
			String[] bookColumns = { "id", "ord", "name", "chapterName" };
			String[] selectionArgs = { String.valueOf(from), String.valueOf(to) };

			Cursor bookCursor = db.query("Book", bookColumns, "ord between ? and ?", selectionArgs, null, null, "ord");
			if (bookCursor.moveToFirst()) { //At least one row present		
				do {
					Book book = new Book(bookCursor.getInt(1));
					book.setName(bookCursor.getString(2));
					book.setChapterName(bookCursor.getString(3));
					book.setChapters(selectChaptersList(bookCursor.getInt(0), null));
					result.add(book);
				} while (bookCursor.moveToNext());
			}
			bookCursor.close();
			return result;
		}
	}

	/**
	 * Returns list of books shortest names from local database
	 * @param from lower bound for book index
	 * @param to upper bound for book index
	 * @return list of shortest names
	 */
	public ArrayList<String> selectBookShortestNames(int from, int to) {
		synchronized (lock) {
			ArrayList<String> result = new ArrayList<String>();
			String[] bookColumns = { "shortestName" };
			String where = "ord between ? and ?";
			String[] selectionArgs = { String.valueOf(from), String.valueOf(to) };

			Cursor bookCursor = db.query("Book", bookColumns, where, selectionArgs, null, null, "ord");
			if (bookCursor.moveToFirst()) { //At least one row present		
				do {
					result.add(bookCursor.getString(0));
				} while (bookCursor.moveToNext());
			}
			bookCursor.close();
			return result;
		}
	}

	/**
	 * Returns list of books names from local database
	 * @param from lower bound for book index
	 * @param to upper bound for book index
	 * @return list of names
	 */
	public ArrayList<String> selectBookNames(int from, int to) {
		synchronized (lock) {
			ArrayList<String> result = new ArrayList<String>();
			String[] bookColumns = { "name" };
			String where = "ord between ? and ?";
			String[] selectionArgs = { String.valueOf(from), String.valueOf(to) };

			Cursor bookCursor = db.query("Book", bookColumns, where, selectionArgs, null, null, "ord");
			if (bookCursor.moveToFirst()) { //At least one row present		
				do {
					result.add(bookCursor.getString(0));
				} while (bookCursor.moveToNext());
			}
			bookCursor.close();
			return result;
		}
	}

	/**
	 * Returns list of short names from local database
	 * @return list of short names
	 */
	public ArrayList<String> selectShortNames() {
		synchronized (lock) {
			ArrayList<String> result = new ArrayList<String>();
			String[] columns = { "sname" };

			Cursor cursor = db.query("ShortName", columns, null, null, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result.add(cursor.getString(0));
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Returns Book object with list of chapters loaded from local database
	 * @param ord Book order
	 * @param filter if present defines list of chapters which should be fetched
	 * @return generated Book object
	 */
	public Book selectBookWithChapters(int ord, Set<Integer> filter) {
		synchronized (lock) {
			Book result = new Book(ord);
			String[] bookColumns = { "id", "ord", "name", "name_cs", "chapterName", "chapterName_cs", "shortestName" };
			String where = "ord = ?";
			String[] selectionArgs = { String.valueOf(ord) };
			Cursor bookCursor = db.query("Book", bookColumns, where, selectionArgs, null, null, null);
			if (bookCursor.moveToFirst()) { //At least one row present		
				do {
					result.setName(bookCursor.getString(2));
					result.setName_cs(bookCursor.getString(3));
					result.setChapterName(bookCursor.getString(4));
					result.setChapterName_cs(bookCursor.getString(5));
					result.setShortestName(bookCursor.getString(6));
					result.setChapters(selectChaptersList(bookCursor.getInt(0), filter));
				} while (bookCursor.moveToNext());
			}
			bookCursor.close();
			return result;
		}
	}

	/**
	 * Inserts new history entry to the local database
	 * @param record Entry to insert
	 * @return id of the inserted row or -1 if error happened
	 */
	public long insertHistory(String record) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			values.put("record", record);
			return db.insert("History", null, values);
		}
	}

	/**
	 * Deletes all history data from the local database
	 */
	public void deleteHistory() {
		synchronized (lock) {
			db.delete("History", null, null);
		}
	}

	/**
	 * Selects search history from the local database
	 * @return List of history entries
	 */
	public ArrayList<String> selectHistory() {
		synchronized (lock) {
			ArrayList<String> result = new ArrayList<String>();
			String[] columns = { "record" };
			Cursor cursor = db.query("History", columns, null, null, null, null, "ord");
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result.add(cursor.getString(0));
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Select currently saved set of readings
	 * @param type 0 for Gospel readings; 1 for Apostle readings
	 * @return Current readings
	 */
	public Readings selectReadings(int type) {
		synchronized (lock) {
			Readings result = new Readings();
			String[] columns = { "date", "bookIndex", "chapterIndex", "length" };
			String[] selectionArgs = { String.valueOf(type) };

			Cursor cursor = db.query("Readings", columns, "type = ?", selectionArgs, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					Calendar c = Calendar.getInstance();
					try {
						c.setTime(ISO_DATE.parse(cursor.getString(0)));
					} catch (ParseException ex) {
					}
					result.setDate(c);
					result.setLength(cursor.getInt(3));
					result.addIndex(cursor.getInt(1), cursor.getInt(2));
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Saves readings to the local database
	 * @param type 0 for Gospel readings; 1 for Apostle readings
	 * @param source Readings to save
	 */
	public void updateReadings(int type, Readings source) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			values.put("date", ISO_DATE.format(source.getDate().getTime()));
			values.put("bookIndex", source.getIndex(0).book);
			values.put("chapterIndex", source.getIndex(0).chapter);
			values.put("length", source.getLength());
			String[] selectionArgs = { String.valueOf(type) };
			db.update("Readings", values, "type = ?", selectionArgs);
		}
	}

	/**
	 * Selects number of chapters in specific book
	 * @param ord Book number
	 * @return Book size
	 */
	public int selectBookSize(int ord) {
		synchronized (lock) {
			int result = 0;
			String[] columns = { "count(*)" };
			String where = "bookID = (select id from Book where ord = ?)";
			String[] selectionArgs = { String.valueOf(ord) };
			Cursor cursor = db.query("Chapter", columns, where, selectionArgs, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Selects number of lines in specific chapter
	 * @param bookOrd Book number
	 * @param chapterOrd Chapter number (within a book)
	 * @return Chapter size
	 */
	public int selectChapterSize(int bookOrd, int chapterOrd) {
		synchronized (lock) {
			int result = 0;
			String[] columns = { "size" };
			String where = "bookID = (select id from Book where ord = ?) and ord = ?";
			String[] selectionArgs = { String.valueOf(bookOrd), String.valueOf(chapterOrd) };
			Cursor cursor = db.query("Chapter", columns, where, selectionArgs, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Selects number of chapters in specific book
	 * @param ord Book number
	 * @return Book size
	 */
	public String selectBookShortestName(int ord) {
		synchronized (lock) {
			String result = "";
			String[] columns = { "shortestName" };
			Cursor cursor = db.query("Book", columns, String.format("ord = %d", ord), null, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result = cursor.getString(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Selects book number by short name
	 * @param shortName Name to search
	 * @return Book number
	 */
	public int selectBookOrdByShortName(String shortName) {
		synchronized (lock) {
			int result = 0;
			String[] columns = { "ord" };
			String table = "Book as B inner join ShortName as SN on B.id = SN.bookID";
			Cursor cursor = db.query(table, columns, String.format("lower(Sn.sname) = lower('%s')", shortName), null, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Deletes all bookmarks from the local database
	 */
	public void deleteBookmarks() {
		synchronized (lock) {
			db.delete("Bookmark", null, null);
			db.delete("sqlite_sequence", "name = 'Bookmark'", null);
		}
	}

	/**
	 * Selects list of all bookmarks from the local database
	 * @return List of bookmarks
	 */
	public ArrayList<Location> selectBookmarks() {
		synchronized (lock) {
			ArrayList<Location> result = new ArrayList<Location>();
			String[] columns = { "title", "bookIndex", "chapterIndex", "scrollPosition", "scrollOffset", "filter" };
			Cursor cursor = db.query("Bookmark", columns, null, null, null, null, "ord");
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					Location location = null;
					if (cursor.getInt(1) > -1) { //Location is not null
						location = new Location(cursor.getInt(1), cursor.getInt(2));
						location.setTitle(cursor.getString(0));
						location.setScrollPosition(cursor.getInt(3));
						location.setScrollOffset(cursor.getInt(4));
						if (cursor.getString(5).length() > 0) { //Filter present
							location.setFilter(new LocationFilter(cursor.getString(5)));
						}
					}
					result.add(location);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Inserts new bookmark to the local database
	 * @param bookmark Bookmark to insert
	 * @return id of the inserted row or -1 if error happened
	 */
	public long insertBookmark(Location bookmark) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			if (bookmark != null) {
				values.put("title", bookmark.getTitle());
				values.put("bookIndex", bookmark.getBook());
				values.put("chapterIndex", bookmark.getChapter());
				values.put("scrollPosition", bookmark.getScrollPosition());
				values.put("scrollOffset", bookmark.getScrollOffset());
				values.put("filter", bookmark.isLimited() ? bookmark.getFilter().toString() : "");
			} else {
				values.put("bookIndex", -1);
			}
			return db.insert("Bookmark", null, values);
		}
	}

	/**
	 * Deletes all tab information from the local database
	 */
	private void deleteTabs() {
		synchronized (lock) {
			db.delete("Tab", null, null);
			db.delete("sqlite_sequence", "name = 'Tab'", null);
		}
	}

	/**
	 * Selects list of all tab information from the local database
	 * @return List of tabs
	 */
	public ArrayList<Location> selectTabs() {
		synchronized (lock) {
			ArrayList<Location> result = new ArrayList<Location>();
			String[] columns = { "title", "bookIndex", "chapterIndex", "scrollPosition", "scrollOffset", "filter" };
			if (!db.isOpen()) { //Necessary precaution
				openRead();
			}
			Cursor cursor = db.query("Tab", columns, null, null, null, null, "ord desc");
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					Location location = null;
					if (cursor.getInt(1) > -1) { //Location is not null
						location = new Location(cursor.getInt(1), cursor.getInt(2));
						location.setTitle(cursor.getString(0));
						location.setScrollPosition(cursor.getInt(3));
						location.setScrollOffset(cursor.getInt(4));
						if (cursor.getString(5).length() > 0) { //Filter present
							location.setFilter(new LocationFilter(cursor.getString(5)));
						}
					}
					result.add(location);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Inserts new tab info entry to the local database
	 * @param locationTab to insert
	 * @return id of the inserted row or -1 if error happened
	 */
	private long insertTab(Location location) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			if (location != null) {
				values.put("title", location.getTitle());
				values.put("bookIndex", location.getBook());
				values.put("chapterIndex", location.getChapter());
				values.put("scrollPosition", location.getScrollPosition());
				values.put("scrollOffset", location.getScrollOffset());
				values.put("filter", location.isLimited() ? location.getFilter().toString() : "");
			} else {
				values.put("bookIndex", -1);
			}
			return db.insert("Tab", null, values);
		}
	}

	/**
	 * Inserts new tab information to the local database
	 * @param tabs Tab information to insert
	 * @return id of the inserted row or -1 if error happened
	 */
	public void insertTabs(ArrayList<Location> tabs) {
		synchronized (lock) {
			deleteTabs();
			for (Location location : tabs) {
				insertTab(location);
			}
		}
	}

}
