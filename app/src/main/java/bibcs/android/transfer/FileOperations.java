package bibcs.android.transfer;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import bibcs.android.model.CalendarEntry;
import bibcs.android.model.DailyReading;
import bibcs.android.model.Line;
import bibcs.android.util.CsNumber;

public abstract class FileOperations {
	private static final int BUFFER_SIZE = 1024;

	/**
	 * Deletes specified file
	 * @param file File to delete
	 */
	public static void deleteFile(File file) {
		file.delete();
	}

	/**
	 * Creates directory if it is not yet created
	 * @param path Path to the folder in which directory should be created
	 * @param dirName Directory name
	 * @return Path to the newly created directory
	 */
	public static File forceMakeDir(File path, String dirName) {
		File result = new File(path, dirName);
		result.mkdirs();
		return result;
	}

	/**
	 * Deletes directory recursively
	 * @param path Directory to delete
	 * @return true if directory was successfully deleted, false otherwise
	 * @throws IOException in case deletion failed
	 */
	public static void deleteDir(File path) throws IOException {
		if (!path.exists()) {
			return;
		}
		if (path.isDirectory()) {
			for (File child : path.listFiles()) {
				deleteDir(child);
			}
		}
		// The directory is now empty so delete it
		if (!path.delete()) {
			throw new IOException("Could not delete " + path.getAbsolutePath());
		}
	}

	/**
	 * Saves contents of InputStream to the specified file
	 * @param input Input stream to load file contents from
	 * @param target Target file to save contents to
	 * @throws IOException in case file operations failed
	 */
	public static void saveFile(InputStream input, File target) throws IOException {
		BufferedInputStream is = new BufferedInputStream(input);
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			FileOutputStream output = new FileOutputStream(target);
			int length;
			while ((length = is.read(buffer)) != -1) { //Read until the end of file
				output.write(buffer, 0, length);
			}
			output.close();
		} finally {
			is.close();
		}
	}

	/**
	 * Extracts contents of ZIP file to the specified location
	 * @param source File to copy data from
	 * @param path Path to folder
	 * @param callback Class which allows to publish progress
	 * @throws IOException in case file operations failed
	 */
	public static void extractZip(File source, File path, Callback callback) throws IOException {
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(source)));
		byte[] buffer = new byte[BUFFER_SIZE];
		int entryCount = 0;
		try {
			ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				if (entry.isDirectory()) { //Entry is a directory
					(new File(path, entry.getName())).mkdirs();
				} else { //Entry is a regular file
					File file = new File(path, entry.getName());
					if (!file.getParentFile().exists()) { //Parent directory does not exist
						file.getParentFile().mkdirs();
					}
					FileOutputStream output = new FileOutputStream(new File(path, entry.getName()));
					int length;
					while ((length = zis.read(buffer)) != -1) { //Read until the end of entry
						output.write(buffer, 0, length);
					}
					output.close();
					entryCount--; //Keep progress track
					if (callback != null) {
						callback.onEvent(entryCount);
					}
				}
			}
		} finally {
			zis.close();
		}
	}

	/**
	 * Loads chapter contents from SD card
	 * @param chapterPath Path to chapter file
	 * @param rusPath path to Russian translation files or null if no need to load
	 * @param filter Additional lines filter
	 * @return Array of lines with data
	 * @throws IOException in case file operations failed
	 * @throws RustextNotFoundException in case Russian translation not found
	 */
	private static ArrayList<Line> loadChapterContents(File chapterPath, File rusPath, Set<Integer> filter) throws IOException,
			RustextNotFoundException {
		ArrayList<Line> result = new ArrayList<Line>();
		String temp = null;
		int number = 0;
		BufferedReader rusReader = null;
		if (rusPath != null) { //Russian translation
			if (!rusPath.exists()) {
				throw new RustextNotFoundException();
			}
			rusReader = new BufferedReader(new InputStreamReader(new FileInputStream(rusPath), "Windows-1251"), BUFFER_SIZE);
		}
		BufferedReader chapReader = new BufferedReader(new InputStreamReader(new FileInputStream(chapterPath), "Windows-1251"), BUFFER_SIZE);
		while ((temp = chapReader.readLine()) != null) { //All lines (either with filter or without it)
			number++;
			Line line = new Line(CsNumber.GenerateCSNumber(number), temp, filter != null && !filter.contains(number));
			if (rusReader != null) { //Russian translation
				temp = rusReader.readLine();
				line.setRustext(temp);
			}
			result.add(line);
		}
		result.add(new Line()); //Addition
		chapReader.close();
		if (rusReader != null) { //Russian translation
			rusReader.close();
		}
		return result;
	}

	/**
	 * Loads book contents from SD card
	 * @param bookPath Path to book directory
	 * @param bookSize Number of chapters to load
	 * @param loadRus true to load Russian translation as well
	 * @param filter Additional chapters filter
	 * @return Array of chapters with data
	 * @throws IOException in case file operations failed
	 * @throws RustextNotFoundException in case Russian translation not found
	 */
	public static List<List<Line>> loadBookContent(File bookPath, int bookSize, boolean loadRus, Map<Integer, Set<Integer>> filter) throws IOException,
			RustextNotFoundException {
		List<List<Line>> result = new ArrayList<List<Line>>();
		if (filter == null) { //Load all chapters
			for (int i = 1; i <= bookSize; i++) {
				File chapterPath = new File(bookPath, String.format("ch%03d.txt", i));
				File rusPath = loadRus ? new File(bookPath, String.format("rus%03d.txt", i)) : null;
				result.add(loadChapterContents(chapterPath, rusPath, null));
			}
		} else { //Load only selected chapters
			for (Integer chapterKey : filter.keySet()) {
				File chapterPath = new File(bookPath, String.format("ch%03d.txt", chapterKey));
				File rusPath = loadRus ? new File(bookPath, String.format("rus%03d.txt", chapterKey)) : null;
				result.add(loadChapterContents(chapterPath, rusPath, filter.get(chapterKey)));
			}
		}
		return result;
	}

	/**
	 * Loads information for specific (current) calendar year
	 * @param infoPath Path to info.csv file
	 * @return Array of month-wise information
	 * @throws IOException in case file operations failed
	 */
	public static List<String> loadCalendarYearInfo(File infoPath) throws IOException {
		List<String> result = new ArrayList<String>();
		String line;
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(infoPath)), BUFFER_SIZE);
		while ((line = reader.readLine()) != null) {
			result.add(line);
		}
		reader.close();
		return result;
	}

	/**
	 * Loads information for specific calendar day
	 * @param path Path to ###.xml file
	 * @return Loaded entry
	 * @throws IOException in case file operations failed
	 * @throws XmlPullParserException in case XML parsing fails
	 */
	public static CalendarEntry loadCalendarDay(File path) throws IOException, XmlPullParserException {
		CalendarEntry result = new CalendarEntry();

		XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
		InputStream is = new FileInputStream(path);
		try {
			xpp.setInput(is, "UTF-8");
			DailyReading reading = null;
			int ri = -1; //0 - Old, 1 - Apostle, 2 - Gospel
			while (/*running && */xpp.getEventType() != XmlPullParser.END_DOCUMENT) {

				if (xpp.getEventType() == XmlPullParser.START_TAG) {
					if (xpp.getName().equalsIgnoreCase("entry")) {
						result.setHoliday(Boolean.valueOf(xpp.getAttributeValue(0)));
					}
					if (xpp.getName().equalsIgnoreCase("t")) { //Title
						result.setTitle(xpp.nextText());
					} else if (xpp.getName().equalsIgnoreCase("f")) { //Fasting
						result.setFasting(xpp.nextText());
//					} else if (xpp.getName().equalsIgnoreCase("fi")) { //Fasting index
//						result.setFastIndex(Integer.valueOf(xpp.nextText()));
					} else if (xpp.getName().equalsIgnoreCase("o")) { //Old readings
						ri = 0;
					} else if (xpp.getName().equalsIgnoreCase("a")) { //Apostle readings
						ri = 1;
					} else if (xpp.getName().equalsIgnoreCase("g")) { //Gospel readings
						ri = 2;
					} else if (ri > -1 && xpp.getName().equalsIgnoreCase("r")) { //Reading
						reading = new DailyReading();
					} else if (reading != null) {
						if (xpp.getName().equalsIgnoreCase("l")) { //Link (reading)
							reading.setLink(xpp.nextText().trim());
						} else if (xpp.getName().equalsIgnoreCase("c")) { //Comment (reading)
							reading.setComment(xpp.nextText().trim());
						}
					}

				} else if (xpp.getEventType() == XmlPullParser.END_TAG) {
					if (xpp.getName().equalsIgnoreCase("r")) { //Reading
						switch (ri) {
							case 0: //Old
								result.getReadingsOld().add(reading);
								break;
							case 1: //Apostle
								result.getReadingsApostle().add(reading);
								break;
							case 2: //Gospel
								result.getReadingsGospel().add(reading);
								break;
						}
						reading = null;
					} else if (xpp.getName().equalsIgnoreCase("o")) {
						ri = -1;
					} else if (xpp.getName().equalsIgnoreCase("a")) {
						ri = -1;
					} else if (xpp.getName().equalsIgnoreCase("g")) {
						ri = -1;
					}
				}
				xpp.next();
			}
		} finally {
			is.close();
		}
		return result;
	}

	/**
	 * Callback class which can be used for various continuous operations
	 * @author Semeniuk A.D.
	 * 
	 */
	public static interface Callback {
		public void onEvent(int value);
	}

	/**
	 * Exception class to be thrown when Russian translation is not found on SD
	 * card
	 * @author Semeniuk A.D.
	 */
	public static class RustextNotFoundException extends Exception {
		private static final long serialVersionUID = 1L;
	}

}
