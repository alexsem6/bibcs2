package bibcs.android.transfer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import bibcs.android.model.Book;
import bibcs.android.model.Chapter;

public abstract class ServerConnector {

	private static final String GLOBAL_PATH = "http://www.alexsem.org/bibcs/sync.php";

	/**
	 * Generates URI for server interaction
	 * @param action Action to execute
	 * @param lang Current device language (or null for default)
	 * @return Generated URI
	 */
	private static URI constructURI(String action, String lang) {
		StringBuilder sb = new StringBuilder(GLOBAL_PATH);
		sb.append("?action=").append(action);
		if (lang != null) { //Set language
			sb.append("&lang=").append(lang);
		}
		return URI.create(sb.toString());
	}

	/**
	 * Generates URI for server interaction (calendar-related command)
	 * @param action Action to execute
	 * @param year Year in question
	 * @return Generated URI
	 */
	private static URI constructCalendarURI(String action, int year) {
		StringBuilder sb = new StringBuilder(GLOBAL_PATH);
		sb.append("?action=").append(action);
		sb.append("&year=").append(year);
		return URI.create(sb.toString());
	}

	/**
	 * Requests metadata as ZIP file
	 * @param lang Current device language
	 * @returns List of loaded issues
	 * @throws IOException in case of interaction problems
	 * @throws XmlPullParserException in case of parsing problems
	 */
	public static ArrayList<Book> getMetaData(String lang) throws IOException, XmlPullParserException {
		ArrayList<Book> result = new ArrayList<Book>();
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(constructURI("meta", lang));
		HttpResponse httpResponse = (BasicHttpResponse) client.execute(post);

		XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
		InputStream is = httpResponse.getEntity().getContent();
		try {
			xpp.setInput(is, "UTF-8");
			Book book = null;
			ArrayList<Chapter> chapters = null;
			ArrayList<String> shortNames = null;
			while (/*running && */xpp.getEventType() != XmlPullParser.END_DOCUMENT) {

				if (xpp.getEventType() == XmlPullParser.START_TAG) {
					if (xpp.getName().equalsIgnoreCase("book")) {
						book = new Book(Integer.valueOf(xpp.getAttributeValue(0)));
					} else if (book != null) {
						if (xpp.getName().equalsIgnoreCase("name")) {
							book.setName(xpp.nextText());
						} else if (xpp.getName().equalsIgnoreCase("name_cs")) {
							book.setName_cs(xpp.nextText());
						} else if (xpp.getName().equalsIgnoreCase("chapname")) {
							book.setChapterName(xpp.nextText());
						} else if (xpp.getName().equalsIgnoreCase("chapname_cs")) {
							book.setChapterName_cs(xpp.nextText());
						} else if (xpp.getName().equalsIgnoreCase("shortnames")) {
							shortNames = new ArrayList<String>();
						} else if (xpp.getName().equalsIgnoreCase("chapters")) {
							chapters = new ArrayList<Chapter>();
						} else if (shortNames != null) {
							if (xpp.getName().equalsIgnoreCase("sname")) {
								boolean shortest = Boolean.valueOf(xpp.getAttributeValue(0));
								String sname = xpp.nextText();
								shortNames.add(sname);
								if (shortest) {
									book.setShortestName(sname);
								}
							}
						} else if (chapters != null) {
							if (xpp.getName().equalsIgnoreCase("chapter")) {
								Chapter chapter = new Chapter(Integer.valueOf(xpp.getAttributeValue(0)));
								chapter.setSize(Integer.valueOf(xpp.getAttributeValue(1)));
								chapters.add(chapter);
							}
						}
					}

				} else if (xpp.getEventType() == XmlPullParser.END_TAG) {

					if (xpp.getName().equalsIgnoreCase("book")) {
						result.add(book);
						book = null;
					} else if (xpp.getName().equalsIgnoreCase("shortnames")) {
						book.setShortNames(shortNames);
						shortNames = null;
					} else if (xpp.getName().equalsIgnoreCase("chapters")) {
						Collections.sort(chapters);
						book.setChapters(chapters);
						chapters = null;
					}
				}
				xpp.next();
			}
		} finally {
			is.close();
		}
		Collections.sort(result);
		return result;
	}

	/**
	 * Requests books contents as ZIP file
	 * @return InputStream Stream to read response from
	 * @throws IOException in case of interaction problems
	 */
	public static InputStream getBooksZip() throws IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(constructURI("zip", null));
		HttpResponse httpResponse = (BasicHttpResponse) client.execute(post);
		return httpResponse.getEntity().getContent();
	}
	
	/**
	 * Requests Russian translation as ZIP file
	 * @return InputStream Stream to read response from
	 * @throws IOException in case of interaction problems
	 */
	public static InputStream getRustextZip() throws IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(constructURI("rustext", null));
		HttpResponse httpResponse = (BasicHttpResponse) client.execute(post);
		return httpResponse.getEntity().getContent();
	}

	/**
	 * Requests help contents as ZIP file
	 * @param lang Current device language
	 * @return InputStream Stream to read response from
	 * @throws IOException in case of interaction problems
	 */
	public static InputStream getHelpZip(String lang) throws IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(constructURI("help", lang));
		HttpResponse httpResponse = (BasicHttpResponse) client.execute(post);
		return httpResponse.getEntity().getContent();
	}

	/**
	 * Checks whether calendar data for the specified year exists
	 * @param year Year in question
	 * @return true if data exists and can be downloaded, false otherwise
	 * @throws IOException in case of interaction problems
	 */
	public static boolean checkCalendarExists(int year) throws IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(constructCalendarURI("calexists", year));
		HttpResponse httpResponse = (BasicHttpResponse) client.execute(post);
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()), 1024);
		boolean result = false;
		String line = reader.readLine();
		reader.close();
		try {
			result = line.contains("true");		
		} catch (Exception ex) {
			throw new IOException(ex.getMessage());
		}
		return result;
	}

	/**
	 * Requests calendar contents as ZIP file
	 * @param year Year in question
	 * @return InputStream Stream to read response from
	 * @throws IOException in case of interaction problems
	 */
	public static InputStream getCalendarZip(int year) throws IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(constructCalendarURI("calendar", year));
		HttpResponse httpResponse = (BasicHttpResponse) client.execute(post);
		return httpResponse.getEntity().getContent();
	}

}
