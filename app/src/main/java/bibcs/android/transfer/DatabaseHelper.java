package bibcs.android.transfer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	public DatabaseHelper(Context context) {
		super(context, "bible", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE Book      (id INTEGER PRIMARY KEY, ord INTEGER, name TEXT, name_cs TEXT, chapterName TEXT, chapterName_cs TEXT, shortestName TEXT);");
		db.execSQL("CREATE TABLE Chapter   (id INTEGER PRIMARY KEY, ord INTEGER, bookID INTEGER, size INTEGER);");
		db.execSQL("CREATE TABLE ShortName (id INTEGER PRIMARY KEY, bookID INTEGER, sname TEXT);");
		db.execSQL("CREATE TABLE History   (ord INTEGER PRIMARY KEY, record TEXT);");
		db.execSQL("CREATE TABLE Readings  (type INTEGER PRIMARY KEY, date TEXT, bookIndex INTEGER, chapterIndex INTGER, length INTEGER);");
		db.execSQL("CREATE TABLE Tab       (ord INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, bookIndex INTEGER, chapterIndex INTGER, scrollPosition INTEGER, scrollOffset INTEGER, filter TEXT);");
		db.execSQL("CREATE TABLE Bookmark  (ord INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, bookIndex INTEGER, chapterIndex INTGER, scrollPosition INTEGER, scrollOffset INTEGER, filter TEXT);");
		
		db.execSQL("INSERT INTO Readings (type, date, bookIndex, chapterIndex, length) VALUES (0, '1900-01-01', 51, 1, 1)");
		db.execSQL("INSERT INTO Readings (type, date, bookIndex, chapterIndex, length) VALUES (1, '1900-01-01', 55, 1, 2)");
}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS Readings");
		db.execSQL("DROP TABLE IF EXISTS History");
		db.execSQL("DROP TABLE IF EXISTS ShortName");
		db.execSQL("DROP TABLE IF EXISTS Chapter");
		db.execSQL("DROP TABLE IF EXISTS Book");
		onCreate(db);
	}

}
