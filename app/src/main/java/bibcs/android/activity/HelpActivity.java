package bibcs.android.activity;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class HelpActivity extends Activity {

	private static final int REQUEST_DOWNLOAD = 1;
	public static final String VERSION = "h_2.3";

	private WebView web;
	private File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		SharedPreferences preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

		Button back = (Button) findViewById(R.id.help_back);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HelpActivity.this.finish();
			}
		});

		web = (WebView) findViewById(R.id.help_web);
		web.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.startsWith("mailto:")) { //"mailto:" link clicked
					url = url.replaceFirst("mailto:", "").trim();
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("plain/text").putExtra(Intent.EXTRA_EMAIL, new String[] { url });
					HelpActivity.this.startActivity(i);
					return true;
				} else {
					view.loadUrl(url);
					return true;
				}
			}
		});

		String storagePath = preferences.getString("SDpath", "");
		file = new File(new File(storagePath, "help"), "index.html");

		//---------- Help fix ----------
		if (!preferences.contains(HelpActivity.VERSION)) {
			file.delete();
			preferences.edit().putBoolean(HelpActivity.VERSION, true).commit();
		}

		load();
	}

	/**
	 * Loads and displays contents of help index file
	 */
	private void load() {
		try {
			if (file.exists()) {
				web.loadUrl(file.toURL().toString());
			} else {
				error();
			}
		} catch (Exception e) { //Should never happen
			error();
		}
	}

	/**
	 * Shows error message
	 */
	private void error() {
		DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						Intent intent = new Intent(HelpActivity.this, InitActivity.class);
						intent.putExtra("HELP", true);
						startActivityForResult(intent, REQUEST_DOWNLOAD);
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						HelpActivity.this.finish();
						break;
				}
			}
		};
		new AlertDialog.Builder(this).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert).setMessage(R.string.help_error)
				.setCancelable(false).setPositiveButton(R.string.dialog_download, ocl).setNegativeButton(R.string.dialog_close, ocl).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_DOWNLOAD) {
			load();
		}
	}

}