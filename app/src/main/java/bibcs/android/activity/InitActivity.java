package bibcs.android.activity;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import bibcs.android.model.Book;
import bibcs.android.transfer.DatabaseAdapter;
import bibcs.android.transfer.FileOperations;
import bibcs.android.transfer.ServerConnector;

public class InitActivity extends Activity {

  private File rootPath;
  private File currentPath;
  private DatabaseAdapter database;
  private boolean updatingGlobal = false;
  private boolean updatingHelpFiles = false;
  private boolean updatingRustextFiles = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.init);
    updatingGlobal = getIntent().getBooleanExtra("UPDATE", false);
    updatingHelpFiles = getIntent().getBooleanExtra("HELP", false);
    updatingRustextFiles = getIntent().getBooleanExtra("RUSTEXT", false);
    if (updatingGlobal || updatingHelpFiles || updatingRustextFiles) {
      database = DatabaseAdapter.getInstance(this);
      currentPath = new File(getSharedPreferences(getPackageName(), MODE_PRIVATE).getString("SDpath", ""));
      new ImporterTask().execute();
    } else {
      start();
    }
  }

  /**
   * Starts importing process
   */
  private void start() {
    //------ Check whether SD card is available ------
    if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) { //SD card is not present
      new AlertDialog.Builder(this).setTitle(R.string.dialog_error).setMessage(R.string.init_error_nosd)
          .setPositiveButton(R.string.dialog_exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              InitActivity.this.finish();
            }
          }).setCancelable(false).show();
      return;
    }
    // ------------------------------------------------

    this.rootPath = Environment.getExternalStorageDirectory();

    // ------ Prepare path selection dialog ------
    final View pathContent = getLayoutInflater().inflate(R.layout.init_dialog_path, null);
    final RadioButton rabPathSystem = (RadioButton) pathContent.findViewById(R.id.init_path_system_radio);
    final RadioButton rabPathCustom = (RadioButton) pathContent.findViewById(R.id.init_path_custom_radio);
    final LinearLayout llPathSystem = (LinearLayout) pathContent.findViewById(R.id.init_path_system);
    final LinearLayout llPathCustom = (LinearLayout) pathContent.findViewById(R.id.init_path_custom);
    final EditText edtPathBrowse = (EditText) pathContent.findViewById(R.id.init_path_browse_result);
    final Button btnPathBrowse = (Button) pathContent.findViewById(R.id.init_path_browse);

    DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE: //Accept
            if (rabPathSystem.isChecked()) { //System folder
              currentPath = getFilesDir();
            } else { //CustomFolder
              currentPath = new File(edtPathBrowse.getText().toString());
            }
            generateDatabase();
            break;
          case DialogInterface.BUTTON_NEGATIVE: //Exit
            InitActivity.this.finish();
            break;
        }
      }
    };
    final AlertDialog pathDialog = new AlertDialog.Builder(this).setTitle(R.string.init_path_title).setView(pathContent)
        .setPositiveButton(getString(R.string.dialog_accept), ocl).setNegativeButton(getString(R.string.dialog_exit), ocl).setCancelable(false)
        .create();
    View.OnClickListener rabPathSystemOCL = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        rabPathSystem.setChecked(true);
        rabPathCustom.setChecked(false);
        edtPathBrowse.setEnabled(false);
        btnPathBrowse.setEnabled(false);
        pathDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
      }
    };
    rabPathSystem.setOnClickListener(rabPathSystemOCL);
    llPathSystem.setOnClickListener(rabPathSystemOCL);
    View.OnClickListener rabPathCustomOCL = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        rabPathCustom.setChecked(true);
        rabPathSystem.setChecked(false);
        edtPathBrowse.setEnabled(true);
        btnPathBrowse.setEnabled(true);
        pathDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(edtPathBrowse.getText().length() > 0);
      }
    };
    rabPathCustom.setOnClickListener(rabPathCustomOCL);
    llPathCustom.setOnClickListener(rabPathCustomOCL);
    // -------------------------------------------

    // ------- Browsing through SD directories ------
    btnPathBrowse.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
              edtPathBrowse.setText(currentPath.getAbsolutePath());
              pathDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
          }
        };
        final View browseContent = getLayoutInflater().inflate(R.layout.init_dialog_browse, null);
        final ListView lstBrowse = (ListView) browseContent.findViewById(R.id.init_browse_list);
        final AlertDialog browseDialog = new AlertDialog.Builder(InitActivity.this).setView(browseContent)
            .setPositiveButton(getString(R.string.dialog_accept), ocl).setNeutralButton(R.string.dialog_create, null)
            .setNegativeButton(getString(R.string.dialog_cancel), null).create();
        lstBrowse.setAdapter(generateAdapter(rootPath));
        browseDialog.setTitle(currentPath.getAbsolutePath());
        lstBrowse.setOnItemClickListener(new OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            if (position == 0 && l.getItemAtPosition(position).equals("/")) { //Root directory chosen
              lstBrowse.setAdapter(generateAdapter(new File("/")));
              browseDialog.setTitle(currentPath.getAbsolutePath());
              browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
            } else if (position == 1 && l.getItemAtPosition(position).equals("..")) { //Parent directory chosen
              lstBrowse.setAdapter(generateAdapter(currentPath.getParentFile()));
              browseDialog.setTitle(currentPath.getAbsolutePath());
              browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
            } else {
              File file = new File(currentPath, (String) l.getItemAtPosition(position));
              if (file.canRead()) { //Directory is readable
                lstBrowse.setAdapter(generateAdapter(file));
                browseDialog.setTitle(currentPath.getAbsolutePath());
                browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
              } else { //Directory can not be read
                Toast.makeText(InitActivity.this, R.string.init_error_access, Toast.LENGTH_SHORT).show();
              }
            }
          }
        });
        browseDialog.show();
        browseDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            View content = getLayoutInflater().inflate(R.layout.init_dialog_browse_create, null);
            final EditText input = (EditText) content.findViewById(R.id.init_create_edit);
            new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_create_title).setView(content)
                .setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int whichButton) {
                    File newDir = new File(currentPath, input.getText().toString());
                    if (newDir.mkdir()) { //Directory was created
                      lstBrowse.setAdapter(generateAdapter(newDir));
                      browseDialog.setTitle(newDir.getAbsolutePath());
                      browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
                    } else { //Error occurred
                      Toast.makeText(InitActivity.this, R.string.init_error_create, Toast.LENGTH_LONG).show();
                    }
                  }
                }).setNegativeButton(R.string.dialog_cancel, null).show();
          }
        });
      }
    });
    // ----------------------------------------------

    pathDialog.show();

    String path = getSharedPreferences(getPackageName(), MODE_PRIVATE).getString("SDpath", "");
    if (path.length() > 0 && !this.rootPath.getAbsolutePath().equals(path)) { //SD path already defined (not first launch)
      currentPath = new File(path);
      edtPathBrowse.setText(path);
      rabPathCustom.performClick();
    }
  }

  /**
   * Generates ArrayAdapter based on contents of the specified directory
   * @param path Directory in question
   * @return ArryAdapter around list of files
   */
  private ArrayAdapter<String> generateAdapter(File path) {
    this.currentPath = path;
    File[] files = path.listFiles();
    ArrayList<String> result = new ArrayList<String>();
    for (File file : files) {
      if (file.isDirectory() && !file.getName().startsWith(".")) {
        result.add(file.getName());
      }
    }
    Collections.sort(result);
    if (path.getParent() != null) {
      result.add(0, "/");
      result.add(1, "..");
    }
    return new ArrayAdapter<String>(this, R.layout.init_dialog_browse_item, result);
  }

  /**
   * Tries to establish connection to the local database
   * Generates DB structure in process
   */
  private void generateDatabase() {
    database = DatabaseAdapter.getInstance(this);
    try {
      database.openWrite();
      database.close();
      importData();
    } catch (SQLException ex) {
      new AlertDialog.Builder(InitActivity.this).setTitle(R.string.dialog_error).setMessage(R.string.init_error_dbconnect)
          .setIcon(android.R.drawable.ic_dialog_alert).setPositiveButton(R.string.dialog_exit, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          InitActivity.this.finish();
        }
      }).setCancelable(false).show();
    }
  }

  /**
   * Imports books from SD storage to local database
   */
  private void importData() {
    if (currentPath == null) { //Bugfix
      currentPath = getFilesDir();
    }
    if (currentPath.list().length > 0) { //Files present in the path
      DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
              new ImporterTask().execute();
              break;
            case DialogInterface.BUTTON_NEGATIVE:
              start();
              break;
          }
        }
      };
      new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_notempty_title).setMessage(R.string.init_notempty_prompt)
          .setPositiveButton(R.string.dialog_yes, ocl).setNegativeButton(R.string.dialog_no, ocl).setCancelable(false).show();
    } else {
      new ImporterTask().execute();
    }
  }

  /**
   * Successfully finalizes the import process
   */
  private void complete() {
    getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean("2.2", true).commit(); //!!! remove later
    getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean("h_2.2", true).commit(); //!!! remove later
    if (getIntent().hasExtra("RUSTEXT")) { //Explicit translation request
      setResult(RESULT_OK);
      InitActivity.this.finish();
    } else {
      if (!updatingRustextFiles) { //Regular initialization

        //----------------  Translation download ----------------
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            updatingRustextFiles = true;
            if (which == DialogInterface.BUTTON_POSITIVE) {
              new ImporterTask().execute();
            } else {
              complete();
            }
          }
        };
        new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_rustext_title).setIcon(android.R.drawable.ic_dialog_info)
            .setMessage(R.string.init_rustext_message).setPositiveButton(R.string.dialog_yes, listener)
            .setNegativeButton(R.string.dialog_no, listener).setCancelable(false).show();
      } else {

        //----------------  Regular download ----------------
        new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_complete_title).setMessage(R.string.init_complete_message)
            .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putString("SDpath", currentPath.getAbsolutePath())
                    .putBoolean(HelpActivity.VERSION, true).commit();
                InitActivity.this.finish();
                startActivity(new Intent(InitActivity.this, ReaderActivity.class));
              }
            }).setCancelable(false).show();
      }
    }
  }

  /**
   * Error during import process. Request for re-import
   * @param trace Full stack trace in case of exception
   */
  private void failed(String trace) {
    final String details = trace;
    DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE:
            new ImporterTask().execute();
            break;
          case DialogInterface.BUTTON_NEUTRAL:
            details(details);
            break;
          case DialogInterface.BUTTON_NEGATIVE:
            InitActivity.this.finish();
            break;
        }
      }
    };
    AlertDialog.Builder dialog = new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_failed_title)
        .setIcon(android.R.drawable.ic_dialog_alert).setMessage(R.string.init_failed_message).setPositiveButton(R.string.dialog_retry, ocl)
        .setNegativeButton(R.string.dialog_exit, ocl).setCancelable(false);
    if (trace != null) {
      dialog.setNeutralButton(R.string.dialog_details, ocl);
    }
    try { //Necessary precaution
      dialog.show();
    } catch (Exception ex) {
      this.finish();
    }
  }

  /**
   * Error during import process. Request for re-import
   * @param trace Full stack trace in case of exception
   */
  private void details(String trace) {
    final String details = trace;
    DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
          Intent i = new Intent(Intent.ACTION_SEND);
          i.setType("text/plain");
          i.putExtra(Intent.EXTRA_EMAIL, new String[]{"alexandr.semeniuk@gmail.com"});
          i.putExtra(Intent.EXTRA_SUBJECT, "Init error");
          i.putExtra(Intent.EXTRA_TEXT, details);
          try {
            startActivity(Intent.createChooser(i, getString(R.string.init_details_send)));
          } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(InitActivity.this, R.string.init_details_noemail, Toast.LENGTH_SHORT).show();
          }
        }
        failed(details);
      }
    };
    new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_details_title).setIcon(android.R.drawable.ic_dialog_alert).setMessage(details)
        .setPositiveButton(R.string.dialog_send, ocl).setNegativeButton(R.string.dialog_close, ocl).setCancelable(false).show();
  }

  /**
   * Class which is used for asynchronous book importing
   * @author Semeniuk A.D.
   */
  private class ImporterTask extends AsyncTask<Void, Integer, Exception> {
    private ProgressDialog progress;

    private final int BOOKS_COUNT = 1439 + 2;

    @Override
    protected void onPreExecute() {
      progress = new ProgressDialog(InitActivity.this);
      progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
      progress.setMessage("");
      progress.setOnCancelListener(new OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          ImporterTask.this.cancel(true);
        }
      });
      progress.show();
    }

    private final FileOperations.Callback callback = new FileOperations.Callback() {
      @Override
      public void onEvent(int value) {
        publishProgress(value);
      }
    };

    @Override
    protected Exception doInBackground(Void... params) {
      try {
        if (!updatingRustextFiles) {

          //----------------  Regular download ----------------
          String lang = getResources().getConfiguration().locale.getLanguage();
          if (!updatingHelpFiles) { //Downloading entire content
            this.publishProgress(0);

            FileOperations.deleteDir(currentPath);
            currentPath.mkdirs();
            this.publishProgress(1);

            ArrayList<Book> metadata = ServerConnector.getMetaData(lang);
            this.publishProgress(2);

            database.openWrite();
            database.insertBookArray(metadata);
            database.close();
            this.publishProgress(3);

            File zip = new File(currentPath, "books.zip");
            FileOperations.saveFile(ServerConnector.getBooksZip(), zip);
            this.publishProgress(4);

            this.publishProgress(999);
            FileOperations.extractZip(zip, currentPath, callback);
            FileOperations.deleteFile(zip);
          }
          this.publishProgress(5);

          File help = new File(currentPath, "help.zip");
          FileOperations.saveFile(ServerConnector.getHelpZip(lang), help);
          this.publishProgress(6);

          FileOperations.extractZip(help, new File(currentPath, "help"), null);
          FileOperations.deleteFile(help);

          this.publishProgress(7);
        } else { //Rustext download

          //----------------  Translation download ----------------
          this.publishProgress(3);

          File zip = new File(currentPath, "rustext.zip");
          FileOperations.saveFile(ServerConnector.getRustextZip(), zip);
          this.publishProgress(4);

          this.publishProgress(999);
          FileOperations.extractZip(zip, currentPath, callback);
          FileOperations.deleteFile(zip);

          this.publishProgress(7);
        }

        return null;
      } catch (Exception ex) {
        return ex;
      }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      int value = values[0];
      if (value == 999) {
        progress.setMax(BOOKS_COUNT);
        progress.setProgress(0);
        return;
      }
      if (value >= 0) {
        progress.setMax(updatingRustextFiles ? 5 : 7);
      }
      progress.setProgress(Math.abs(value));
      if (value >= 0) {
        final String message = getResources().getStringArray(R.array.init_importing_stages)[value];
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            progress.setMessage(message);
          }
        });
      }
    }

    @Override
    protected void onPostExecute(Exception result) {
      if (progress != null && progress.isShowing()) {
        progress.dismiss();
      }
      if (isFinishing()) {
        return;
      }
      if (result == null) { //Success
        if (updatingHelpFiles) {
          InitActivity.this.finish();
        } else {
          complete();
        }
      } else { //Exception happened
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        result.printStackTrace(pw);
        failed(sw.toString());
      }
    }

    @Override
    protected void onCancelled() {
      Toast.makeText(InitActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
      failed(null);
    }
  }

}