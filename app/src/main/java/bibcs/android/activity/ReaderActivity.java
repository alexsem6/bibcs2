package bibcs.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerScrollListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.alexsem.android.widget.ActionToolbar;
import org.alexsem.android.widget.ActionToolbar.OnAnimationEndListener;
import org.alexsem.android.widget.OneViewFlipper;
import org.alexsem.android.widget.OneViewFlipper.OnViewChangeListener;
import org.alexsem.android.widget.SwipeOneViewFlipper;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import bibcs.android.adapter.BookmarksAdapter;
import bibcs.android.adapter.BookmarksAdapter.OnBookmarkClickListener;
import bibcs.android.adapter.ReaderAdapter;
import bibcs.android.adapter.ReaderAdapter.OnDictClickedListener;
import bibcs.android.adapter.ReaderAdapter.OnRustextNotFoundListener;
import bibcs.android.fragment.CalendarFragment;
import bibcs.android.fragment.CalendarFragment.OnCalendarListener;
import bibcs.android.fragment.ContentsFragment;
import bibcs.android.fragment.ContentsFragment.OnContentsListener;
import bibcs.android.fragment.ReadingsFragment;
import bibcs.android.fragment.ReadingsFragment.OnReadingsListener;
import bibcs.android.fragment.SearchesFragment;
import bibcs.android.fragment.SearchesFragment.OnSearchesListener;
import bibcs.android.model.Book;
import bibcs.android.model.Line;
import bibcs.android.model.Location;
import bibcs.android.model.Options;
import bibcs.android.transfer.DatabaseAdapter;
import bibcs.android.transfer.FileOperations;
import bibcs.android.transfer.FileOperations.RustextNotFoundException;
import bibcs.android.util.CalculationLogic;
import bibcs.android.util.CsNumber;
import bibcs.android.widget.PageIndex;

public class ReaderActivity extends FragmentActivity {

  private final String FRAGMENT_CONTENTS = "contents";
  private final String FRAGMENT_SEARCHES = "searches";
  private final String FRAGMENT_READINGS = "readings";
  private final String FRAGMENT_CALENDAR = "calendar";

  private static final int REQUEST_SHOW_PREFERENCES = 1;
  private static final int REQUEST_DELETE_OLDDIC = 2;
  private static final int REQUEST_DOWNLOAD_RUSTEXT = 3;

  private View vReader;
  private View vReaderTabs;
  private HorizontalScrollView scrReaderTabs;
  private LinearLayout llReaderTabs;
  private ImageButton btnReaderTabsAdd;
  private ImageButton btnReaderTabsClose;
  private OneViewFlipper flpReaderBook;
  private View vReaderBookTitlebar;
  private TextView txtReaderBookTitle;
  private ImageView imgReaderBookNext;
  private ImageView imgReaderBookPrev;
  private View vReaderEmpty;
  private View vReaderLoader;
  private SwipeOneViewFlipper flpReaderChapter;
  private PageIndex idxReaderChapterIndex;
  private TextView txtReaderChapterTitle;
  private ListView lstReaderChapter;

  private ImageView imgFullscreenEnable;
  private View vFullScreenControls;
  private ImageButton btnFullscreenUp;
  private ImageButton btnFullScreenDown;
  private ImageButton btnFullScreenLeft;
  private ImageButton btnFullScreenRight;
  private ImageView imgFullscreenDisable;
  private ToggleButton btnFullscreenDict;

  private ToggleButton btnDictEnable;
  private Toast tstDictSplash;
  private TextView txtDictToast;

  private ActionToolbar atbAction;
  private ImageButton btnActionShow;

  private SlidingDrawer sldBookmarks;
  private View vBookmarksAdd;
  private ImageView imgBookmarksAdd;
  private TextView txtBookmarksAdd;
  private ListView lstBookmarks;
  private ActionToolbar atbBookmarksAdd;
  private EditText edtBookmarksAdd;
  private ImageButton btnBookmarksAddCancel;

  private Options options = new Options();

  private String storagePath;
  private DatabaseAdapter database;

  private Typeface regularFont;
  private Typeface headerFont;

  private Animation animFlyFromRight;
  private Animation animFlyFromLeft;
  private Animation animFlyToRight;
  private Animation animFlyToLeft;
  private Animation animIndexSplash;
  private Animation animReaderEmpty;
  private Animation animFullscreenShowoff;

  private ArrayList<ToggleButton> readerTabsButtons = new ArrayList<ToggleButton>();
  private ArrayList<Location> readerTabsLocations = new ArrayList<Location>();
  private int readerTabsSelection = -1;
  private Book readerBookCurrent;
  private List<List<Line>> readerBookCurrentData;
  private BookLoaderTask taskBookLoad = null;

  private int fullscreenPrevOrientation;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

    //-------------------- Remove fragments if any --------------------
    FragmentManager fm = getSupportFragmentManager();
    Fragment f1 = fm.findFragmentByTag(FRAGMENT_CONTENTS);
    Fragment f2 = fm.findFragmentByTag(FRAGMENT_SEARCHES);
    Fragment f3 = fm.findFragmentByTag(FRAGMENT_READINGS);
    Fragment f4 = fm.findFragmentByTag(FRAGMENT_CALENDAR);
    FragmentTransaction ft = fm.beginTransaction();
    if (f1 != null)
      ft.remove(f1);
    if (f2 != null)
      ft.remove(f2);
    if (f3 != null)
      ft.remove(f3);
    if (f4 != null)
      ft.remove(f4);
    ft.commit();

    //-------------------- First run --------------------
    final SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
    if (!prefs.contains("SDpath")) { //No book path specified yet
      DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          if (which == DialogInterface.BUTTON_POSITIVE) {
            startActivity(new Intent(ReaderActivity.this, InitActivity.class));
          }
          ReaderActivity.this.finish();
        }
      };
      new AlertDialog.Builder(this).setTitle(R.string.reader_firstrun_title).setMessage(R.string.reader_firstrun_message)
          .setIcon(android.R.drawable.ic_dialog_info).setPositiveButton(R.string.dialog_download, listener)
          .setNegativeButton(R.string.dialog_exit, listener).show();
      return;
    } else { //Need to load and store book path
      storagePath = prefs.getString("SDpath", "");
    }

    //-------------------- Set content view --------------------
    setContentView(R.layout.reader);
    database = DatabaseAdapter.getInstance(this);
    vReader = findViewById(R.id.reader_main);

    //-------------------- Loading fonts --------------------
    regularFont = Typeface.createFromAsset(getAssets(), "fonts/Orthodox.ttf");
    headerFont = Typeface.createFromAsset(getAssets(), "fonts/Kathisma.ttf");

    //-------------------- Regular Animations --------------------
    animFlyFromRight = AnimationUtils.loadAnimation(this, R.anim.fly_from_right);
    animFlyFromLeft = AnimationUtils.loadAnimation(this, R.anim.fly_from_left);
    animFlyToRight = AnimationUtils.loadAnimation(this, R.anim.fly_to_right);
    animFlyToLeft = AnimationUtils.loadAnimation(this, R.anim.fly_to_left);
    animIndexSplash = AnimationUtils.loadAnimation(this, R.anim.index_splash);
    animReaderEmpty = AnimationUtils.loadAnimation(this, R.anim.fly_from_left);
    animReaderEmpty.setAnimationListener(new AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {
      }

      @Override
      public void onAnimationRepeat(Animation animation) {
      }

      @Override
      public void onAnimationEnd(Animation animation) {
        vReaderEmpty.setVisibility(View.VISIBLE);
      }
    });

    //-------------------- Fullscreen Animation --------------------
    animFullscreenShowoff = AnimationUtils.loadAnimation(this, R.anim.fullscreen_showoff);
    animFullscreenShowoff.setAnimationListener(new AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {
        btnFullscreenUp.getDrawable().setLevel(1);
        btnFullscreenUp.getBackground().setLevel(1);
        btnFullScreenDown.getDrawable().setLevel(1);
        btnFullScreenDown.getBackground().setLevel(1);
        btnFullScreenLeft.getDrawable().setLevel(1);
        btnFullScreenLeft.getBackground().setLevel(1);
        btnFullScreenRight.getDrawable().setLevel(1);
        btnFullScreenRight.getBackground().setLevel(1);
        imgFullscreenDisable.getDrawable().setLevel(1);
        btnFullscreenDict.getBackground().setLevel(1);
      }

      @Override
      public void onAnimationRepeat(Animation animation) {
      }

      @Override
      public void onAnimationEnd(Animation animation) {
        btnFullscreenUp.getDrawable().setLevel(0);
        btnFullscreenUp.getBackground().setLevel(0);
        btnFullScreenDown.getDrawable().setLevel(0);
        btnFullScreenDown.getBackground().setLevel(0);
        btnFullScreenLeft.getDrawable().setLevel(0);
        btnFullScreenLeft.getBackground().setLevel(0);
        btnFullScreenRight.getDrawable().setLevel(0);
        btnFullScreenRight.getBackground().setLevel(0);
        imgFullscreenDisable.getDrawable().setLevel(0);
        btnFullscreenDict.getBackground().setLevel(0);
      }
    });

    //-------------------- Reader tabs --------------------
    vReaderTabs = findViewById(R.id.reader_tabs);
    scrReaderTabs = (HorizontalScrollView) findViewById(R.id.reader_tabs_scroll);
    scrReaderTabs.setOnTouchListener(actionHideTouchListener);
    llReaderTabs = (LinearLayout) findViewById(R.id.reader_tabs_list);
    btnReaderTabsAdd = (ImageButton) findViewById(R.id.reader_tabs_add);
    btnReaderTabsAdd.setOnClickListener(readerTabsAddClickListener);
    btnReaderTabsClose = (ImageButton) findViewById(R.id.reader_tabs_close);
    btnReaderTabsClose.setOnTouchListener(actionHideTouchListener);
    btnReaderTabsClose.setOnClickListener(readerTabsCloseClickListener);

    //-------------------- Reader Book --------------------
    flpReaderBook = (OneViewFlipper) findViewById(R.id.reader_book);
    flpReaderBook.setNextNewAnimation(animFlyFromRight);
    flpReaderBook.setNextOldAnimation(animFlyToLeft);
    flpReaderBook.setPrevNewAnimation(animFlyFromLeft);
    flpReaderBook.setPrevOldAnimation(animFlyToRight);
    flpReaderBook.setCyclic(true);
    flpReaderBook.setOnTouchListener(actionHideTouchListener);
    flpReaderBook.setOnViewChangeListener(new OnViewChangeListener() {
      @Override
      public View viewChanged(Integer index) {
        Location location = readerTabsLocations.get(readerTabsSelection);
        int book = index + 1;
        if (location.isLimited()) {
          book = location.getFilter().getBookOrd(index);
        }
        if (location.getBook() != book) { //Book changed
          location.setBook(book);
          location.setChapter(0);
          location.setScrollPosition(0);
          location.setScrollOffset(0);
        }

        View vReaderBook = ReaderActivity.this.getLayoutInflater().inflate(R.layout.reader_book, null);
        vReaderBookTitlebar = vReaderBook.findViewById(R.id.reader_book_titlebar);
        txtReaderBookTitle = (TextView) vReaderBook.findViewById(R.id.reader_book_title);
        txtReaderBookTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontBook);
        txtReaderBookTitle.setTypeface(headerFont);
        txtReaderBookTitle.setOnTouchListener(actionHideTouchListener);
        imgReaderBookPrev = (ImageView) vReaderBook.findViewById(R.id.reader_book_prev);
        imgReaderBookPrev.setOnClickListener(readerBookPrevClickListener);
        imgReaderBookNext = (ImageView) vReaderBook.findViewById(R.id.reader_book_next);
        imgReaderBookNext.setOnClickListener(readerBookNextClickListener);
        idxReaderChapterIndex = (PageIndex) vReaderBook.findViewById(R.id.reader_index);
        flpReaderChapter = (SwipeOneViewFlipper) vReaderBook.findViewById(R.id.reader_chapter);
        flpReaderChapter.setNextNewAnimation(animFlyFromRight);
        flpReaderChapter.setNextOldAnimation(animFlyToLeft);
        flpReaderChapter.setPrevNewAnimation(animFlyFromLeft);
        flpReaderChapter.setPrevOldAnimation(animFlyToRight);
        flpReaderChapter.setOnTouchListener(actionHideTouchListener);
        flpReaderChapter.setOnViewChangeListener(readerChapterFlipperViewChangeListener);
        imgFullscreenEnable = (ImageView) vReaderBook.findViewById(R.id.fullscreen_enable);
        imgFullscreenEnable.setVisibility(options.fullscreenMode ? View.VISIBLE : View.GONE);
        imgFullscreenEnable.setEnabled(true);
        imgFullscreenEnable.setOnTouchListener(actionHideTouchListener);
        imgFullscreenEnable.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            switchToFullScreen();
          }
        });
        btnDictEnable = (ToggleButton) vReaderBook.findViewById(R.id.dict_enable);
        btnDictEnable.setEnabled(true);
        btnDictEnable.setOnCheckedChangeListener(dictCheckedChangeListener);
        btnDictEnable.setChecked(false);
        btnDictEnable.setVisibility(options.dictionaryIntegr ? View.VISIBLE : View.GONE);
        if (taskBookLoad != null) { //Some book is already loading
          taskBookLoad.cancel(true);
        }
        taskBookLoad = new BookLoaderTask();
        taskBookLoad.execute(location);
        return vReaderBook;
      }
    });
    vReaderEmpty = findViewById(R.id.reader_empty);
    vReaderLoader = findViewById(R.id.reader_loader);

    //-------------------- Action Toolbar --------------------
    atbAction = (ActionToolbar) findViewById(R.id.action);
    atbAction.setHotPoint(10f, 0f);
    atbAction.requestHide();
    atbAction.setOnHideAnimationEndsListener(new OnAnimationEndListener() {
      @Override
      public void animationEnds() {
        btnActionShow.setVisibility(View.VISIBLE);
      }
    });
    btnActionShow = (ImageButton) findViewById(R.id.action_show);
    btnActionShow.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        btnActionShow.setVisibility(View.GONE);
        vReaderEmpty.setVisibility(View.GONE);
        atbAction.setOrientation(ActionToolbar.Orientation.BOTTOM_RIGHT);
        atbAction.show();
      }
    });

    //-------------------- Full screen mode --------------------
    vFullScreenControls = findViewById(R.id.fullscreen_controls);
    btnFullscreenUp = (ImageButton) findViewById(R.id.fullscreen_up);
    btnFullscreenUp.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (lstReaderChapter != null) {
          lstReaderChapter.smoothScrollBy(-lstReaderChapter.getHeight() * 4 / 5, 2000);
        }
      }
    });
    btnFullScreenDown = (ImageButton) findViewById(R.id.fullscreen_down);
    btnFullScreenDown.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (lstReaderChapter != null) {
          lstReaderChapter.smoothScrollBy(lstReaderChapter.getHeight() * 4 / 5, 2000);
        }
      }
    });
    btnFullScreenLeft = (ImageButton) findViewById(R.id.fullscreen_left);
    btnFullScreenLeft.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (flpReaderChapter != null) {
          flpReaderChapter.showPrevious();
        }
      }
    });
    btnFullScreenRight = (ImageButton) findViewById(R.id.fullscreen_right);
    btnFullScreenRight.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (flpReaderChapter != null) {
          flpReaderChapter.showNext();
        }
      }
    });
    imgFullscreenDisable = (ImageView) findViewById(R.id.fullscreen_disable);
    imgFullscreenDisable.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        switchToNormalScreen();
      }
    });
    btnFullscreenDict = (ToggleButton) findViewById(R.id.fullscreen_dict);
    btnFullscreenDict.setVisibility(options.dictionaryIntegr ? View.VISIBLE : View.GONE);
    btnFullscreenDict.setOnCheckedChangeListener(dictCheckedChangeListener);

    //-------------------- Dictionary integration --------------------
    txtDictToast = (TextView) getLayoutInflater().inflate(R.layout.dict_toast, null);
    txtDictToast.setTypeface(regularFont);
    tstDictSplash = Toast.makeText(this, "", Toast.LENGTH_SHORT);
    tstDictSplash.setView(txtDictToast);

    //-------------------- Bookmarks Drawer --------------------
    sldBookmarks = (SlidingDrawer) findViewById(R.id.bookmarks);
    sldBookmarks.setOnDrawerScrollListener(new OnDrawerScrollListener() {
      @Override
      public void onScrollStarted() {
        if (atbBookmarksAdd.isOpened()) {
          btnBookmarksAddCancel.performClick();
        }
      }

      @Override
      public void onScrollEnded() {
      }
    });
    lstBookmarks = (ListView) findViewById(R.id.bookmarks_list);
    lstBookmarks.setEmptyView(findViewById(R.id.bookmarks_list_empty));
    lstBookmarks.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (atbBookmarksAdd.isOpened()) {
          btnBookmarksAddCancel.performClick();
          return true;
        }
        return false;
      }
    });

    //-------------------- Bookmarks toolbar --------------------
    atbBookmarksAdd = (ActionToolbar) findViewById(R.id.bookmarks_toolbar);
    atbBookmarksAdd.setHotPoint(24f, 1f);
    atbBookmarksAdd.requestHide();
    edtBookmarksAdd = (EditText) findViewById(R.id.bookmarks_toolbar_edit);
    btnBookmarksAddCancel = (ImageButton) findViewById(R.id.bookmarks_toolbar_cancel);
    btnBookmarksAddCancel.setOnClickListener(bookmarksCancelClickListener);
    edtBookmarksAdd.setOnEditorActionListener(new OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE && vBookmarksAdd.isEnabled()) {
          vBookmarksAdd.performClick();
        }
        return true;
      }
    });

    //-------------------- Bookmarks buttons --------------------
    vBookmarksAdd = findViewById(R.id.bookmarks_add);
    imgBookmarksAdd = (ImageView) findViewById(R.id.bookmarks_add_image);
    txtBookmarksAdd = (TextView) findViewById(R.id.bookmarks_add_text);
    vBookmarksAdd.setOnClickListener(bookmarksAddClickListener);

    //-------------------- Load preferences --------------------
    updateFromPreferences();

    //-------------------- Load tabs --------------------
    new RestoreTabsTask().execute();

    //-------------------- Promo version 3 --------------------
    if (!getSharedPreferences(getPackageName(), MODE_PRIVATE).contains("2.2.5")) {
      DialogInterface.OnClickListener promoListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
              getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean("2.2.5", true).commit();
              try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=org.alexsem.bibcs")));
              } catch (Exception ex) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=org.alexsem.bibcs")));
              }
              break;
            case DialogInterface.BUTTON_NEUTRAL:
              getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean("2.2.5", true).commit();
              break;
            case DialogInterface.BUTTON_NEGATIVE:
              getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean("2.2.5-later", true).commit();
              break;
          }
        }
      };
    }

  }

  @Override
  protected void onResume() {
    super.onResume();

    if (database == null) {
      return;
    }

    //-------------------- Load bookmarks --------------------
    database.openRead();
    lstBookmarks.setAdapter(new BookmarksAdapter(this, database.selectBookmarks(), bookmarksItemClickListener));
    database.close();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    new MenuInflater(getApplication()).inflate(R.menu.main, menu);
    return (super.onCreateOptionsMenu(menu));
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    int s = menu.size();
    boolean tabExists = (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size());
    menu.getItem(0).getSubMenu().getItem(1).setEnabled(tabExists);
    menu.getItem(0).getSubMenu().getItem(2).setEnabled(tabExists);
    for (int i = 0; i < s; i++) {
      menu.getItem(i).setVisible(true);
    }
    menu.getItem(2).setVisible(options.rusmode == -1); //Show Russian text
    menu.getItem(3).setVisible(options.rusmode > -1); //Disable Кussian text
    if (options.fullscreen) { //Full screen mode
      for (int i = 0; i < s - 3; ++i) {
        menu.getItem(i).setVisible(false);
      }
      if (options.daytime < 0) { //Nigh time
        menu.findItem(R.id.menu_night).setVisible(false);
      } else { //Day time
        menu.findItem(R.id.menu_day).setVisible(false);
      }
    } else { //Normal screen mode
      for (int i = s - 3; i < s - 1; ++i) {
        menu.getItem(i).setVisible(false);
      }
    }
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (atbBookmarksAdd.isOpened()) {
      btnBookmarksAddCancel.performClick();
    }
    switch (item.getItemId()) {
      case R.id.menu_tabs_new:
        btnReaderTabsAdd.performClick();
        break;
      case R.id.menu_tabs_clone:
        if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) {
          Location location = readerTabsLocations.get(readerTabsSelection);
          saveCurrentPosition(location);
          addNewTab(Location.clone(location), true);
        }
        break;
      case R.id.menu_tabs_close:
        btnReaderTabsClose.performClick();
        break;
      case R.id.menu_tabs_closeall:
        readerTabsButtons.clear();
        readerTabsLocations.clear();
        llReaderTabs.removeAllViews();
        readerTabsSelection = -1;
        showEmptyBook();
        break;
      case R.id.menu_contents:
        actionItem(findViewById(R.id.action_item_contents));
        break;
      case R.id.menu_rustext_on:
        if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) { //Save current position
          Location location = readerTabsLocations.get(readerTabsSelection);
          saveCurrentPosition(location);
        }
        options.quickrus = true;
        setRustextEnabled(true);
        break;
      case R.id.menu_rustext_off:
        setRustextEnabled(false);
        break;
      case R.id.menu_settings:
        startActivityForResult(new Intent(this, EditPreferences.class), REQUEST_SHOW_PREFERENCES);
        break;
      case R.id.menu_help_manual:
        startActivity(new Intent(this, HelpActivity.class));
        break;
      case R.id.menu_help_about:
        View layout = getLayoutInflater().inflate(R.layout.about, null);
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle(R.string.about_title);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setView(layout);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        break;
      case R.id.menu_day:
        options.daytime = 1;
        if (idxReaderChapterIndex != null) {
          idxReaderChapterIndex.setNight(false);
        }
        vFullScreenControls.startAnimation(animFullscreenShowoff);
        updateFromPreferences();
        break;
      case R.id.menu_night:
        options.daytime = -1;
        if (idxReaderChapterIndex != null) {
          idxReaderChapterIndex.setNight(true);
        }
        vFullScreenControls.startAnimation(animFullscreenShowoff);
        updateFromPreferences();
        break;
      case R.id.menu_exit:
        if (options.fullscreen) {
          switchToNormalScreen();
        } else {
          this.finish();
        }
        break;
      default:
        return super.onOptionsItemSelected(item);
    }
    return true;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case REQUEST_SHOW_PREFERENCES:
        if (resultCode == Activity.RESULT_OK) {
          updateFromPreferences();
        }
        break;
      case REQUEST_DELETE_OLDDIC: //Old dictionary removed
        try {
          getPackageManager().getApplicationInfo("diccs.android.activity", 0);
        } catch (PackageManager.NameNotFoundException e1) {
          try {
            getPackageManager().getApplicationInfo("org.alexsem.diccs.activity", 0);
          } catch (PackageManager.NameNotFoundException e2) { //New dictionary not found
            try {
              startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=org.alexsem.diccs.activity")));
            } catch (Exception ex1) {
              try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=org.alexsem.diccs.activity")));
              } catch (Exception ex2) {
                Toast.makeText(ReaderActivity.this, R.string.dict_error_loadfail, Toast.LENGTH_LONG).show();
              }
            }
          }
        }
        break;
      case REQUEST_DOWNLOAD_RUSTEXT:
        if (resultCode == Activity.RESULT_OK) {
          setRustextEnabled(true);
        } else {
          updateFromPreferences();
        }
        if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) {
          showSpecificBook(readerTabsLocations.get(readerTabsSelection));
        } else {
          showEmptyBook();
        }
        break;
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (database == null) {
      return;
    }

    //--------------- Save tabs ---------------
    if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) { //Save current position
      Location location = readerTabsLocations.get(readerTabsSelection);
      saveCurrentPosition(location);
    }
    database.openWrite();
    database.insertTabs(readerTabsLocations);
    database.close();
    getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putInt("currentTab", readerTabsSelection).commit();

    //--------------- Save bookmarks list ---------------
    if (lstBookmarks.getAdapter() != null) {
      database.openWrite();
      database.deleteBookmarks();
      for (int i = 0; i < lstBookmarks.getAdapter().getCount(); i++) {
        database.insertBookmark(((BookmarksAdapter) lstBookmarks.getAdapter()).getItem(i));
      }
      database.close();
    }
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent event) {
    if (event.getAction() == KeyEvent.ACTION_DOWN) {
      switch (event.getKeyCode()) {
        case KeyEvent.KEYCODE_VOLUME_UP:
          if (lstReaderChapter != null && lstReaderChapter.getAdapter() != null) {
            lstReaderChapter.smoothScrollBy(-lstReaderChapter.getHeight() * 4 / 5, 2000);
          }
          return true;
        case KeyEvent.KEYCODE_VOLUME_DOWN:
          if (lstReaderChapter != null && lstReaderChapter.getAdapter() != null) {
            lstReaderChapter.smoothScrollBy(lstReaderChapter.getHeight() * 4 / 5, 2000);
          }
          return true;
        case KeyEvent.KEYCODE_MENU:
          if (atbBookmarksAdd.isOpened()) {
            btnBookmarksAddCancel.performClick();
          }
          return false;
        case KeyEvent.KEYCODE_SEARCH:
          SearchesFragment searches = (SearchesFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_SEARCHES);
          if (searches == null) {
            searches = new SearchesFragment();
            searches.setOnSearchesListener(searchesPerformListener);
            searches.setHistorySize(options.historySize);
            addFragment(FRAGMENT_SEARCHES, searches);
          } else if (searches.isVisible()) {
            searches.performSearch();
          } else {
            showFragment(searches);
          }
          hideFragment(FRAGMENT_CONTENTS);
          removeFragment(FRAGMENT_READINGS);
          hideFragment(FRAGMENT_CALENDAR);
          return true;
      }
    }
    if (event.getAction() == KeyEvent.ACTION_UP
        && (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_SEARCH)) {
      return true;
    }
    return super.dispatchKeyEvent(event);
  }

  @Override
  public void onBackPressed() {
    if (options != null && options.fullscreen) { //Fullscreen
      switchToNormalScreen();
      return;
    }
    if (atbAction != null && atbAction.isOpened()) { //Action Toolbar
      atbAction.hide();
      return;
    }
    if (sldBookmarks != null && sldBookmarks.isOpened()) { //Bookmarks Panel
      sldBookmarks.animateClose();
      return;
    }
    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment;
    if ((fragment = fm.findFragmentByTag(FRAGMENT_CONTENTS)) != null && fragment.isVisible()) {
      hideFragment(FRAGMENT_CONTENTS);
      return;
    }
    if ((fragment = fm.findFragmentByTag(FRAGMENT_SEARCHES)) != null && fragment.isVisible()) {
      if (!((SearchesFragment) fragment).hideEim()) {
        hideFragment(FRAGMENT_SEARCHES);
      }
      return;
    }
    if ((fragment = fm.findFragmentByTag(FRAGMENT_READINGS)) != null && fragment.isVisible()) {
      removeFragment(FRAGMENT_READINGS);
      return;
    }
    if ((fragment = fm.findFragmentByTag(FRAGMENT_CALENDAR)) != null && fragment.isVisible()) {
      hideFragment(FRAGMENT_CALENDAR);
      return;
    }
    super.onBackPressed();
  }

  //=========================== METHODS ===========================

  /**
   * Listener for all action buttons clicks
   * @param v View which was clicked
   */
  public void actionItem(View v) {
    if (sldBookmarks.isOpened())
      sldBookmarks.animateClose();
    switch (v.getId()) {
      case R.id.action_item_contents:
        ContentsFragment contents = (ContentsFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_CONTENTS);
        if (contents == null) {
          contents = new ContentsFragment();
          contents.setOnContentsListener(contentsItemListener);
          contents.setFontSize(options.fontContents);
          addFragment(FRAGMENT_CONTENTS, contents);
        } else {
          showFragment(contents);
        }
        break;
      case R.id.action_item_search:
        SearchesFragment searches = (SearchesFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_SEARCHES);
        if (searches == null) {
          searches = new SearchesFragment();
          searches.setOnSearchesListener(searchesPerformListener);
          searches.setHistorySize(options.historySize);
          addFragment(FRAGMENT_SEARCHES, searches);
        } else {
          showFragment(searches);
        }
        break;
      case R.id.action_item_readings:
        ReadingsFragment readings = (ReadingsFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_READINGS);
        if (readings == null) {
          readings = new ReadingsFragment();
          readings.setOnReadingsListener(readingsSearchListener);
          addFragment(FRAGMENT_READINGS, readings);
        } else {
          showFragment(readings);
        }
        break;
      case R.id.action_item_calendar:
        CalendarFragment calendar = (CalendarFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_CALENDAR);
        if (calendar == null) {
          calendar = CalendarFragment.newInstance(storagePath);
          calendar.setOnCalendarListener(calendarSearchListener);
          addFragment(FRAGMENT_CALENDAR, calendar);
        } else {
          showFragment(calendar);
        }
        break;
      case R.id.action_item_bookmarks:
        sldBookmarks.animateOpen();
        break;
    }
    atbAction.hide();
  }

  /**
   * Specify whether to show Russian translation
   * @param enabled true to enable, false to disable
   */
  private void setRustextEnabled(boolean enabled) {
    PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("rustext_enabled", enabled).commit();
    updateFromPreferences();
  }

  /**
   * Populate necessary preferences
   */
  private void updateFromPreferences() {
    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment = null;
    //-------------------- Load preferences --------------------
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    options.lineNumbers = prefs.getBoolean("line_numbers", false);
    options.fullChapter = prefs.getBoolean("full_chapter", false);
    options.chPericopes = prefs.getBoolean("ch_pericopes", false);
    options.rustextMode = Integer.valueOf(prefs.getString("rustext_mode", "0"));
    options.backgroundColor = prefs.getInt("background_color", 0xffeeeeee);
    options.newTab = prefs.getBoolean("new_tab", false);
    options.screenOn = prefs.getBoolean("screen_on", false);
    options.historySize = Integer.valueOf(prefs.getString("history_size", "10"));
    options.fullscreenMode = prefs.getBoolean("fullscreen_mode", true);
    options.dictionaryIntegr = prefs.getBoolean("dictionary_integr", true);
    options.fontText = Float.valueOf(prefs.getString("font_text", "21"));
    options.fontRustext = Float.valueOf(prefs.getString("font_rustext", "17"));
    options.fontBook = Float.valueOf(prefs.getString("font_book", "30"));
    options.fontContents = Float.valueOf(prefs.getString("font_contents", "14"));
    options.rusmode = prefs.getBoolean("rustext_enabled", false) ? options.rustextMode : -1;
    if (txtReaderChapterTitle != null) { //Chapter title object defined
      txtReaderChapterTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontText + 1f);
    }
    if (txtReaderBookTitle != null) { //Book title object defined
      txtReaderBookTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontBook);
    }
    if ((fragment = fm.findFragmentByTag(FRAGMENT_CONTENTS)) != null) { //Contents options
      ((ContentsFragment) fragment).setFontSize(options.fontContents);
    }
    if ((fragment = fm.findFragmentByTag(FRAGMENT_SEARCHES)) != null) { //Searches options
      ((SearchesFragment) fragment).setHistorySize(options.historySize);
    }
    int colorToSet = options.backgroundColor;
    if (options.fullscreen && options.daytime < 0) { //Night time
      colorToSet = getResources().getColor(R.color.reader_background_night);
    }
    vReader.setBackgroundColor(colorToSet);
    if (lstReaderChapter != null) {
      lstReaderChapter.setKeepScreenOn(options.screenOn);
      if (lstReaderChapter.getAdapter() != null) {
        lstReaderChapter.setCacheColorHint(colorToSet);
        lstReaderChapter.setBackgroundColor(colorToSet);
        ((ReaderAdapter) ((HeaderViewListAdapter) lstReaderChapter.getAdapter()).getWrappedAdapter()).setOptions(options);
        lstReaderChapter.invalidate();
      }
    }
    if (imgFullscreenEnable != null) {
      imgFullscreenEnable.setVisibility(options.fullscreenMode ? View.VISIBLE : View.GONE);
    }
    btnFullscreenDict.setVisibility(options.dictionaryIntegr ? View.VISIBLE : View.GONE);
    if (btnDictEnable != null) {
      boolean enabled = options.dictionaryIntegr && options.rusmode != 1; //Disabled in rustext mode
      btnDictEnable.setChecked(enabled ? btnDictEnable.isChecked() : false);
      btnDictEnable.setVisibility(enabled ? View.VISIBLE : View.GONE);
    }
  }

  /**
   * Saves current scroll position to the current location
   * @param location Location to save position to
   */
  private void saveCurrentPosition(Location location) {
    if (location != null && lstReaderChapter != null) {
      location.setScrollPosition(lstReaderChapter.getFirstVisiblePosition());
      if (lstReaderChapter.getChildAt(0) != null) {
        location.setScrollOffset(lstReaderChapter.getChildAt(0).getTop());
      }
    }
  }

  /**
   * Initializes book flipper by showing specific book
   */
  private void showSpecificBook(Location location) {
    if (location == null) {
      if (taskBookLoad != null) {
        taskBookLoad.cancel(true);
      }
      showEmptyBook();
    } else if (location.isLimited()) { //Limited location
      flpReaderBook.setData(location.getFilter().getBookCount(), location.getFilter().getBookIndex(location.getBook()));
    } else { //Continuous reading
      flpReaderBook.setData(77, location.getBook() - 1);
    }
  }

  /**
   * Performs actions which reset all needed fields and variables
   * in case no book is needed to be shown
   */
  private void showEmptyBook() {
    flpReaderBook.resetData();
    if (readerTabsSelection > -1 && readerTabsSelection < readerTabsButtons.size()) { //Need to change tab text
      ToggleButton button = readerTabsButtons.get(readerTabsSelection);
      String text = getString(R.string.reader_tab_empty);
      button.setText(text);
      button.setTextOn(text);
      button.setTextOff(text);
    }
    vReaderEmpty.startAnimation(animReaderEmpty);
    vReaderLoader.setVisibility(View.GONE);
    readerBookCurrent = null;
    readerBookCurrentData = null;
    setBookmarksAddEnabled(false);
  }

  /**
   * Opens new tab and load location in in
   * @param location Location to load in new tab or null for none
   * @param select   Defines whether button should be selected
   */
  private void addNewTab(Location location, boolean select) {
    final ToggleButton button = (ToggleButton) getLayoutInflater().inflate(R.layout.reader_tabs_tab, llReaderTabs, false);
    //--- Set tab title ---
    String text = "";
    if (location == null) {
      text = getString(R.string.reader_tab_empty);
    } else {
      text = location.getTitle();
    }
    button.setText(text);
    button.setTextOn(text);
    button.setTextOff(text);
    //---
    button.setOnTouchListener(actionHideTouchListener);
    button.setOnCheckedChangeListener(readerTabsCheckedChangeListener);
    readerTabsButtons.add(readerTabsSelection + 1, button);
    readerTabsLocations.add(readerTabsSelection + 1, location);
    llReaderTabs.addView(button, readerTabsSelection + 1);
    if (location != null && location.isLimited()) {
      button.getBackground().setLevel(1); //Red background
    }
    if (select) { //Need to select button
      button.postDelayed(new Runnable() {
        public void run() {
          button.setChecked(true);
        }
      }, 0);
    }
  }

  /**
   * Open location in current tab (if exists)
   * @param location Location to load in the current tab or null for none
   */
  private void updateCurrentTab(Location location) {
    if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) {
      readerTabsLocations.set(readerTabsSelection, location);
      ToggleButton button = readerTabsButtons.get(readerTabsSelection);
      //--- Set tab title ---
      String text = "";
      if (location == null) {
        text = getString(R.string.reader_tab_empty);
      } else {
        text = location.getTitle();
      }
      button.setText(text);
      button.setTextOn(text);
      button.setTextOff(text);
      //---
      button.getBackground().setLevel((location != null && location.isLimited()) ? 1 : 0);
      showSpecificBook(location);
    }
  }

  /**
   * Opens book either in new tab or in current tab (based on options)
   * @param location Location to load (or null to show empty book)
   */
  private void performNavigation(Location location) {
    if (readerTabsSelection < 0) { //No tabs are yet opened
      addNewTab(location, true);
      return;
    }
    if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size() && readerTabsLocations.get(readerTabsSelection) == null) { //Current tab is empty
      updateCurrentTab(location);
      return;
    }
    if (options.newTab) { //Open books in new tabs
      addNewTab(location, true);
    } else if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) { //Open books in current tab
      updateCurrentTab(location);
    }
  }

  ;

  /**
   * Switches representation to full screen mode
   * (with respective changes to controls)
   */
  private void switchToFullScreen() {
    options.fullscreen = true;
    if (options.daytime == 0) { //Undefined
      int hod = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
      if (hod >= 5 && hod <= 16) {
        options.daytime = 1; //Day
      } else {
        options.daytime = -1; //Night
      }
    }
    imgFullscreenEnable.setEnabled(false);
    btnDictEnable.setEnabled(false);
    closeOptionsMenu();
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    vFullScreenControls.setVisibility(View.VISIBLE);
    vFullScreenControls.startAnimation(animFullscreenShowoff);
    if (sldBookmarks.isOpened()) {
      sldBookmarks.close();
    }
    if (vReaderBookTitlebar != null) {
      vReaderBookTitlebar.setVisibility(View.GONE);
    }
    if (idxReaderChapterIndex != null) {
      idxReaderChapterIndex.setNight(options.daytime < 0);
    }
    btnActionShow.setVisibility(View.GONE);
    sldBookmarks.setVisibility(View.GONE);
    vReaderTabs.setVisibility(View.GONE);
    fullscreenPrevOrientation = getRequestedOrientation();
    switch (((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getRotation()) {
      case Surface.ROTATION_90:
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        break;
      case Surface.ROTATION_180:
        setRequestedOrientation(9/* reversePortait */);
        break;
      case Surface.ROTATION_270:
        setRequestedOrientation(8/* reverseLandscape */);
        break;
      default:
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
    updateFromPreferences();
  }

  /**
   * Switches representation back to normal mode
   * (with respective changes to controls)
   */
  private void switchToNormalScreen() {
    options.fullscreen = false;
    imgFullscreenEnable.setEnabled(true);
    btnDictEnable.setEnabled(true);
    vFullScreenControls.setVisibility(View.GONE);
    closeOptionsMenu();
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    if (vReaderBookTitlebar != null) {
      vReaderBookTitlebar.setVisibility(View.VISIBLE);
    }
    if (idxReaderChapterIndex != null) {
      idxReaderChapterIndex.setNight(false);
    }
    btnActionShow.setVisibility(View.VISIBLE);
    sldBookmarks.setVisibility(View.VISIBLE);
    vReaderTabs.setVisibility(View.VISIBLE);
    setRequestedOrientation(fullscreenPrevOrientation);
    updateFromPreferences();
  }

  /**
   * Enables or disables "Add bookmark" item
   * @param enabled true if button should be enabled, false otherwise
   */
  private void setBookmarksAddEnabled(boolean enabled) {
    vBookmarksAdd.setEnabled(enabled);
    imgBookmarksAdd.setEnabled(enabled);
    txtBookmarksAdd.setEnabled(enabled);
    txtBookmarksAdd.setTextColor(getResources().getColor(enabled ? R.color.bookmarks_add_enabled : R.color.bookmarks_add_disabled));
  }

  private void setBookPrevNextEnabled(boolean enabled) {
    if (!enabled || readerTabsSelection < 0 || readerTabsSelection > readerTabsLocations.size() - 1 || flpReaderBook.getChildCount() < 0) {
      if (imgReaderBookPrev != null)
        imgReaderBookPrev.setEnabled(false);
      if (imgReaderBookNext != null)
        imgReaderBookNext.setEnabled(false);
      return;
    }
    if (enabled) {
      if (imgReaderBookPrev == null || imgReaderBookNext == null) {
        return;
      }
      if (readerTabsLocations.get(readerTabsSelection).isLimited()) {
        imgReaderBookPrev.setEnabled(flpReaderBook.getDisplayedChild() > 0);
        imgReaderBookNext.setEnabled(flpReaderBook.getDisplayedChild() < flpReaderBook.getPageCount() - 1);
      } else {
        imgReaderBookPrev.setEnabled(true);
        imgReaderBookNext.setEnabled(true);
      }
    }
  }

  //=========================== LISTENERS ===========================

  private OnTouchListener actionHideTouchListener = new OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      atbAction.hide();
      if (sldBookmarks.isOpened()) {
        sldBookmarks.animateClose();
      }
      return false;
    }
  };

  /**
   * Listener for various Calendar events
   */
  private OnCalendarListener calendarSearchListener = new OnCalendarListener() {

    @Override
    public void onSingleLink(String link) {
      database.openRead();
      Location location = CalculationLogic.parseSearchString(database, link);
      database.close();
      performNavigation(location);
      hideFragment(FRAGMENT_CALENDAR);
    }

    @Override
    public void onMultipleLinks(List<String> links) {
      database.openRead();
      for (int i = links.size() - 1; i >= 1; i--) {
        addNewTab(CalculationLogic.parseSearchString(database, links.get(i)), false);
      }
      performNavigation(CalculationLogic.parseSearchString(database, links.get(0)));
      database.close();
      hideFragment(FRAGMENT_CALENDAR);
    }
  };

  /**
   * Listener for Readings index clicks
   */
  private OnReadingsListener readingsSearchListener = new OnReadingsListener() {
    @Override
    public void onSearch(String text) {
      new SearchTask(SearchTypeCode.READINGS).execute(text);
    }
  };

  /**
   * Listener for various Content items and buttons clicks
   */
  private OnContentsListener contentsItemListener = new OnContentsListener() {

    @Override
    public void onChapterClick(int book, int chapter) {
      Location location = new Location(book, chapter);
      database.openRead();
      location.setTitle(CalculationLogic.generateTabTitle(database, book, chapter));
      database.close();
      performNavigation(location);
      hideFragment(FRAGMENT_CONTENTS);
    }

    @Override
    public void onCancelClick() {
      hideFragment(FRAGMENT_CONTENTS);
    }

  };

  /**
   * Listener for various Search panel events
   */
  private OnSearchesListener searchesPerformListener = new OnSearchesListener() {

    @Override
    public void onSearch(String text) {
      new SearchTask(SearchTypeCode.SEARCH).execute(text);
    }

    @Override
    public void onCancel() {
      hideFragment(FRAGMENT_SEARCHES);
    }
  };

  /**
   * Listener for Bookmarks Add button click
   */
  private OnClickListener bookmarksAddClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      if (imgBookmarksAdd.isEnabled() && readerBookCurrent != null && readerTabsSelection > -1 && readerTabsSelection < readerTabsButtons.size()) {
        if (!atbBookmarksAdd.isOpened()) { //Need to show edit bar
          edtBookmarksAdd.setText(readerTabsButtons.get(readerTabsSelection).getText());
          atbBookmarksAdd.setOrientation(ActionToolbar.Orientation.BOTTOM_LEFT);
          ((RelativeLayout.LayoutParams) atbBookmarksAdd.getLayoutParams()).topMargin = Math.round((getResources()
              .getDimension(R.dimen.bookmarks_top_margin)) - atbBookmarksAdd.getHeight());
          atbBookmarksAdd.requestLayout();
          atbBookmarksAdd.show();
          atbBookmarksAdd.bringToFront();
          edtBookmarksAdd.requestFocus();
          vBookmarksAdd.getBackground().setLevel(1);
          txtBookmarksAdd.setTextColor(getResources().getColor(R.color.bookmarks_add_button));
          imgBookmarksAdd.getDrawable().setLevel(1);
          //((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(bookmarksToolbarEdit, 0);
        } else { //Need to add bookmark
          if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) {
            Location current = readerTabsLocations.get(readerTabsSelection);
            saveCurrentPosition(current);
            Location bookmark = Location.clone(current);
            bookmark.setTitle(edtBookmarksAdd.getText().toString());
            ((BookmarksAdapter) lstBookmarks.getAdapter()).add(bookmark);
            lstBookmarks.setSelection(lstBookmarks.getAdapter().getCount() - 1);
          }
          btnBookmarksAddCancel.performClick();
        }
      }
    }
  };

  /**
   * Listener for Bookmarks add Cancel button click
   */
  private OnClickListener bookmarksCancelClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      vBookmarksAdd.getBackground().setLevel(0);
      txtBookmarksAdd.setTextColor(getResources().getColor(
          txtBookmarksAdd.isEnabled() ? R.color.bookmarks_add_enabled : R.color.bookmarks_add_disabled));
      imgBookmarksAdd.getDrawable().setLevel(0);
      ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edtBookmarksAdd.getWindowToken(), 0);
      atbBookmarksAdd.hide();
    }
  };

  /**
   * Listener for Bookmarks item click
   */
  private OnBookmarkClickListener bookmarksItemClickListener = new OnBookmarkClickListener() {
    @Override
    public void onItemClick(View v, int position) {
      sldBookmarks.animateClose();
      Location bookmark = ((BookmarksAdapter) lstBookmarks.getAdapter()).getItem(position);
      performNavigation(Location.clone(bookmark));
    }
  };

  /**
   * Listener called when Tabs Add ("+") button is clicked
   */
  private OnClickListener readerTabsAddClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      addNewTab(null, true);
    }
  };

  /**
   * Listener called when Tabs are selected or unselected
   */
  private OnCheckedChangeListener readerTabsCheckedChangeListener = new OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      int position = -1;
      for (int i = 0; i < readerTabsButtons.size(); i++) {
        if (readerTabsButtons.get(i).equals(buttonView)) {
          position = i;
          break;
        }
      }
      if (position < 0) {
        return;
      }
      if (isChecked && readerTabsSelection != position) { //Checked other button manually
        int oldSelection = readerTabsSelection;
        readerTabsSelection = position;
        if (oldSelection > -1 && oldSelection < readerTabsButtons.size()) {
          readerTabsButtons.get(oldSelection).setChecked(false);
        }
        //--- Scroll to appropriate position ---
        int l = buttonView.getLeft();
        int w = buttonView.getWidth();
        int sx = scrReaderTabs.getScrollX();
        int sw = scrReaderTabs.getWidth();
        if (l - 10 < sx) {
          scrReaderTabs.scrollTo(l - 10, 0);
        } else if (l + w > sx + sw) {
          scrReaderTabs.scrollTo(l + w + 10 - sw, 0);
        }
        //---
        showSpecificBook(readerTabsLocations.get(readerTabsSelection));
      } else if (!isChecked && position != readerTabsSelection) { //Unchecking currently selected tab on tab change
        Location location = readerTabsLocations.get(position);
        if (location != null) {
          location.setTitle(buttonView.getText().toString());
        }
        saveCurrentPosition(location);
      } else if (!isChecked && position == readerTabsSelection) { //Trying to uncheck selected button manually
        buttonView.setChecked(true);
      }
    }
  };

  /**
   * Listener called when Tabs Close ("x") button is clicked
   */
  private OnClickListener readerTabsCloseClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      int oldSelection = readerTabsSelection;
      readerTabsSelection = -1;
      if (oldSelection > -1 && oldSelection < readerTabsButtons.size()) {
        readerTabsButtons.get(oldSelection).setChecked(false);
        llReaderTabs.removeViewAt(oldSelection);
        readerTabsButtons.remove(oldSelection);
        readerTabsLocations.remove(oldSelection);
      }
      if (oldSelection >= readerTabsButtons.size()) { //Closed last tab
        oldSelection--;
      }
      if (oldSelection <= 0) { //Closed first tab
        if (readerTabsButtons.size() > 0) { //Any tab left
          readerTabsButtons.get(0).setChecked(true);
        } else {
          readerTabsSelection = -1;
          showSpecificBook(null);
        }
      } else { //Closed some other tab
        readerTabsButtons.get(oldSelection).setChecked(true);
      }
    }
  };

  /**
   * Listener called on Previous book "<" button touch
   */
  private OnClickListener readerBookPrevClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      if (sldBookmarks.isOpened()) {
        sldBookmarks.animateClose();
      }
      flpReaderBook.showPrevious();
    }
  };

  /**
   * Listener called on Previous book "<" button touch
   */
  private OnClickListener readerBookNextClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      if (sldBookmarks.isOpened()) {
        sldBookmarks.animateClose();
      }
      flpReaderBook.showNext();
    }
  };

  /**
   * Listener for chapter flipper view change
   */
  private OnViewChangeListener readerChapterFlipperViewChangeListener = new OnViewChangeListener() {
    @Override
    public View viewChanged(Integer index) {
      idxReaderChapterIndex.setData(readerBookCurrent.getChapterNumbers(), index);
      idxReaderChapterIndex.startAnimation(animIndexSplash);
      LayoutInflater inflater = ReaderActivity.this.getLayoutInflater();
      txtReaderChapterTitle = (TextView) inflater.inflate(R.layout.reader_chapter_title, null);
      txtReaderChapterTitle.setOnTouchListener(actionHideTouchListener);
      int chapter = readerBookCurrent.getChapters().get(index).getOrd();
      String tabTitle = String.format(Locale.getDefault(), "%s.%d", readerBookCurrent.getShortestName(), chapter);
      String chapterTitle = String.format("%s %s", readerBookCurrent.getChapterName_cs(), CsNumber.GenerateCSNumber(chapter));
      ToggleButton button = readerTabsButtons.get(readerTabsSelection);
      button.setText(tabTitle);
      button.setTextOn(tabTitle);
      button.setTextOff(tabTitle);
      txtReaderChapterTitle.setText(chapterTitle);
      txtReaderChapterTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, options.fontText + 1f);
      txtReaderChapterTitle.setTypeface(regularFont);
      int colorToSet = options.backgroundColor;
      if (options.fullscreen && options.daytime < 0) {
        colorToSet = getResources().getColor(R.color.reader_background_night);
      }
      lstReaderChapter = (ListView) inflater.inflate(R.layout.reader_chapter, null);
      lstReaderChapter.setKeepScreenOn(options.screenOn);
      lstReaderChapter.setCacheColorHint(colorToSet);
      lstReaderChapter.setBackgroundColor(colorToSet);
      lstReaderChapter.addHeaderView(txtReaderChapterTitle);
      if (index < 0 || index >= readerBookCurrentData.size()) { //Important bugfix
        try {
          Thread.sleep(1000);
        } catch (Exception ex) {
        }
      }
      ReaderAdapter adapter = new ReaderAdapter(ReaderActivity.this, readerBookCurrentData.get(index), options, regularFont,
          flpReaderChapter.getOnTouchListener());
      adapter.setOnDictClickedListener(readerListOnDictClickedListener);
      adapter.setOnRustextNotFoundListener(readerListOnRustextNotFoundListener);
      lstReaderChapter.setAdapter(adapter);
      lstReaderChapter.setOnTouchListener(readerChapterDataOnTouchListener);
      setBookmarksAddEnabled(lstReaderChapter.getAdapter().getCount() > 0);
      Location location = readerTabsLocations.get(readerTabsSelection);
      if (location.isLimited() && !location.isScrollSet()) //Define first line to show in limited location
        if (!options.fullChapter) { //Show only selected text (starting from the very top)
          location.setScrollPosition(0);
          location.setScrollOffset(0);
        } else { //Search for the beginning of the text part
          for (int i = 0; i < readerBookCurrentData.get(index).size(); i++) {
            if (!readerBookCurrentData.get(index).get(i).isGhost()) {
              location.setScrollPosition((i > 0) ? i + 1 : 0);
              location.setScrollOffset(0);
              break;
            }
          }
        }
      if ((location.getScrollPosition() > 0 && options.fullChapter) || location.isScrollSet()) { //Need to scroll
        lstReaderChapter.setSelectionFromTop(location.getScrollPosition(), location.getScrollOffset());
      }
      location.setChapter(chapter); //Change current location chapter
      location.setTitle(tabTitle);
      location.setScrollPosition(-1);
      location.setScrollOffset(0);
      btnDictEnable.setChecked(false);
      return lstReaderChapter;
    }
  };

  /**
   * Listener for Dictionary button click
   */
  private OnCheckedChangeListener dictCheckedChangeListener = new OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      if (isChecked != options.dictmode) {
        options.dictmode = isChecked;
        if (lstReaderChapter != null) {
          ((ReaderAdapter) ((HeaderViewListAdapter) lstReaderChapter.getAdapter()).getWrappedAdapter()).setOptions(options);
        }
        if (btnDictEnable != null) {
          btnDictEnable.setChecked(isChecked);
        }
        btnFullscreenDict.setChecked(isChecked);
      }
    }
  };

  /**
   * Listener for dictionary links click
   */
  private OnDictClickedListener readerListOnDictClickedListener = new OnDictClickedListener() {
    @Override
    public void dictClicked(String word) {
      txtDictToast.setText(word);
      tstDictSplash.show();
      Intent intent = new Intent("org.alexsem.diccs.SEARCH_WORD");
      intent.putExtra("word", word);
      try {
        startActivity(intent);
        btnDictEnable.setChecked(false);
      } catch (ActivityNotFoundException ex) {
        DialogInterface.OnClickListener l = new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            switch (which) {
              case DialogInterface.BUTTON_POSITIVE:
                try {
                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=org.alexsem.diccs.activity")));
                } catch (Exception ex1) {
                  try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=org.alexsem.diccs.activity")));
                  } catch (Exception ex2) {
                    Toast.makeText(ReaderActivity.this, R.string.dict_error_loadfail, Toast.LENGTH_LONG).show();
                  }
                }
                break;
              case DialogInterface.BUTTON_NEGATIVE:
                btnDictEnable.setChecked(false);
                break;
            }
          }
        };
        new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.dict_notfound_title).setIcon(android.R.drawable.ic_dialog_alert)
            .setMessage(R.string.dict_notfound_message).setPositiveButton(R.string.dialog_load, l).setNegativeButton(R.string.dialog_cancel, l)
            .show();
      }
    }
  };

  /**
   * Callback to indicate that Russian translation is not present (if needed)
   */
  private OnRustextNotFoundListener readerListOnRustextNotFoundListener = new OnRustextNotFoundListener() {
    @Override
    public void rustextNotFound() {
      showSpecificBook(readerTabsLocations.get(readerTabsSelection));
    }
  };

  /**
   * Complex Touch listener for data list
   */
  private OnTouchListener readerChapterDataOnTouchListener = new OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      return actionHideTouchListener.onTouch(v, event) || flpReaderChapter.getOnTouchListener().onTouch(v, event);
    }
  };

  //----------------------------------------------------------------------------

  /**
   * Add specific fragment
   * @param tag      Fragment tag
   * @param fragment Fragment instance
   */
  private void addFragment(String tag, Fragment fragment) {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.setCustomAnimations(R.anim.fly_from_right, R.anim.fly_to_left);
    ft.add(R.id.reader_frame, fragment, tag);
    ft.commit();
  }

  /**
   * Remove specific fragment
   * @param tag Fragment tag
   */
  private void removeFragment(String tag) {
    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment = fm.findFragmentByTag(tag);
    if (fragment != null) {
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.setCustomAnimations(R.anim.fly_from_left, R.anim.fly_to_right);
      ft.remove(fragment);
      ft.commit();
    }
  }

  /**
   * Show specific fragment
   * @param fragment Fragment instance
   */
  private void showFragment(Fragment fragment) {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.setCustomAnimations(R.anim.fly_from_right, R.anim.fly_to_left);
    ft.show(fragment);
    ft.commit();
  }

  /**
   * Hide specific fragment
   * @param tag Fragment tag
   */
  private void hideFragment(String tag) {
    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment = fm.findFragmentByTag(tag);
    if (fragment != null) {
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.setCustomAnimations(R.anim.fly_from_left, R.anim.fly_to_right);
      ft.hide(fragment);
      ft.commit();
    }
  }

  //=========================== ASYNCHRONOUS TASKS ===========================

  /**
   * Codes used to report BookLoaderTask result
   */
  private enum LoaderResultCode {
    SUCCESS, CLEAR, ERROR, NOT_FOUND, NO_RUSTEXT
  }

  /**
   * Task which is used for book loading
   * @author Semeniuk A.D.
   */
  private class BookLoaderTask extends AsyncTask<Location, Void, LoaderResultCode> {
    private Location location;

    @Override
    protected void onPreExecute() {
      vReaderLoader.setVisibility(View.VISIBLE);
      setBookPrevNextEnabled(false);
      if (imgFullscreenEnable != null) {
        imgFullscreenEnable.setEnabled(false);
      }
      if (btnDictEnable != null) {
        btnDictEnable.setEnabled(false);
      }
    }

    @Override
    protected LoaderResultCode doInBackground(Location... params) {
      this.location = params[0];
      if (location == null) { //No data to display
        return LoaderResultCode.CLEAR;
      }
      database.openRead();
      if (location.isLimited()) { //Limit present
        readerBookCurrent = database.selectBookWithChapters(location.getBook(), location.getFilter().getChapterList(location.getBook()));
      } else { //Continuous reading
        readerBookCurrent = database.selectBookWithChapters(location.getBook(), null);
      }
      database.close();

      try {
        if (location.isLimited()) {
          readerBookCurrentData = FileOperations.loadBookContent(new File(storagePath, String.format("book%03d", readerBookCurrent.getOrd())), -1,
              (options.rusmode > -1 || options.quickrus), location.getFilter().getBookFilter(readerBookCurrent.getOrd()));
        } else {
          readerBookCurrentData = FileOperations.loadBookContent(new File(storagePath, String.format("book%03d", readerBookCurrent.getOrd())),
              readerBookCurrent.getChapters().size(), (options.rusmode > -1 || options.quickrus), null);
        }
      } catch (RustextNotFoundException ex) {
        return LoaderResultCode.NO_RUSTEXT;
      } catch (Exception ex) {
        return LoaderResultCode.ERROR;
      }
      return LoaderResultCode.SUCCESS;
    }

    @Override
    protected void onPostExecute(LoaderResultCode result) {
      vReaderLoader.setVisibility(View.GONE);
      switch (result) {
        case SUCCESS:
          vReaderEmpty.setVisibility(View.GONE);
          setBookPrevNextEnabled(true);
          if (txtReaderBookTitle != null && readerBookCurrent != null) {
            txtReaderBookTitle.setText(readerBookCurrent.getName_cs());
          }
          if (imgFullscreenEnable != null) {
            imgFullscreenEnable.setEnabled(true);
          }
          if (btnDictEnable != null) {
            btnDictEnable.setEnabled(true);
          }

          // -- Trying to load specific chapter --
          if (location.getChapter() > 0) { //Need to load specific chapter
            boolean success = false;
            for (int i = 0; i < readerBookCurrent.getChapters().size(); i++) {
              if (readerBookCurrent.getChapters().get(i).getOrd() == location.getChapter()) {
                flpReaderChapter.setData(readerBookCurrent.getChapters().size(), i);
                success = true;
                break;
              }
            }
            if (!success) { //Chapter was not loaded, just show the first one
              flpReaderChapter.setData(readerBookCurrent.getChapters().size(), 0);
            }
          } else { //Do not need to load specific chapter (just display the first one)
            flpReaderChapter.setData(readerBookCurrent.getChapters().size(), 0);
          }
          // -------------------------------------*/

          break;
        case CLEAR: //Need to unload book
          showEmptyBook();
          break;
        case NOT_FOUND: //Book was not found on SD card
        case ERROR: //Error happened
          new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.dialog_error).setMessage(R.string.reader_error_loadbook)
              .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  showEmptyBook();
                  ReaderActivity.this.finish();
                  startActivity(new Intent(ReaderActivity.this, InitActivity.class));
                }
              }).setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              showEmptyBook();
            }
          }).setCancelable(false).show();
          break;
        case NO_RUSTEXT:
          options.quickrus = false;
          setRustextEnabled(false);
          new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.dialog_error).setMessage(R.string.reader_error_rustext)
              .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  Intent intent = new Intent(ReaderActivity.this, InitActivity.class);
                  intent.putExtra("RUSTEXT", true);
                  startActivityForResult(intent, REQUEST_DOWNLOAD_RUSTEXT);
                }
              }).setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              if (readerTabsSelection > -1 && readerTabsSelection < readerTabsLocations.size()) {
                showSpecificBook(readerTabsLocations.get(readerTabsSelection));
              } else {
                showEmptyBook();
              }
            }
          }).setCancelable(false).show();
          break;
      }
    }
  }

  /**
   * Codes used to perform SearchTask post-execution actions
   */
  private enum SearchTypeCode {
    SEARCH, READINGS;
  }

  /**
   * Task which is used for string searching
   * @author Semeniuk A.D.
   */
  private class SearchTask extends AsyncTask<String, Void, Location> {
    private ProgressDialog dialog;
    private SearchTypeCode type = null;

    public SearchTask(SearchTypeCode type) {
      this.type = type;
    }

    @Override
    protected void onPreExecute() {
      dialog = new ProgressDialog(ReaderActivity.this);
      dialog.setMessage(getString(R.string.dialog_searching));
      dialog.setCancelable(true);
      dialog.setOnCancelListener(new OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          SearchTask.this.cancel(true);

        }
      });
      dialog.show();
    }

    @Override
    protected Location doInBackground(String... params) {
      database.openRead();
      Location result = CalculationLogic.parseSearchString(database, params[0]);
      database.close();
      return result;
    }

    @Override
    protected void onCancelled() {
      dialog.dismiss();
    }

    @Override
    protected void onPostExecute(Location result) {
      dialog.dismiss();
      switch (type) {
        case SEARCH: //Search button pressed
          hideFragment(FRAGMENT_SEARCHES);
          break;
        case READINGS: //Readings navigation item clicked
          removeFragment(FRAGMENT_READINGS);
          break;
      }
      performNavigation(result);
    }

  }

  /**
   * Task which is used for string searching
   * @author Semeniuk A.D.
   */
  private class RestoreTabsTask extends AsyncTask<Void, Location, Integer> {
    private ProgressDialog dialog;

    @Override
    protected void onPreExecute() {
      dialog = new ProgressDialog(ReaderActivity.this);
      dialog.setMessage(getString(R.string.dialog_restoring));
      dialog.setCancelable(false);
      dialog.show();
    }

    @Override
    protected Integer doInBackground(Void... params) {
      database.openRead();
      ArrayList<Location> tabs = database.selectTabs();
      database.close();
      for (Location location : tabs) {
        publishProgress(location);
      }
      return getSharedPreferences(getPackageName(), MODE_PRIVATE).getInt("currentTab", -1);
    }

    @Override
    protected void onProgressUpdate(Location... values) {
      addNewTab(values[0], false);
    }

    @Override
    protected void onPostExecute(Integer result) {
      dialog.dismiss();
      if (result > -1 && result < readerTabsButtons.size()) { //Need to select tab
        final ToggleButton button = readerTabsButtons.get(result);
        button.postDelayed(new Runnable() {
          public void run() {
            button.setChecked(true);
          }
        }, 0);
      } else { //No tab should be selected
        showEmptyBook();
      }
    }
  }

}