package bibcs.android.activity;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import bibcs.android.widget.ColorPickerDialog;
import bibcs.android.widget.ColorPickerDialog.OnColorPickerListener;

public class EditPreferences extends PreferenceActivity implements OnSharedPreferenceChangeListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		updateSummary(getPreferenceScreen().findPreference("font_text"));
		updateSummary(getPreferenceScreen().findPreference("font_rustext"));
		updateSummary(getPreferenceScreen().findPreference("font_book"));
		updateSummary(getPreferenceScreen().findPreference("font_contents"));
		updateSummary(getPreferenceScreen().findPreference("history_size"));
		updateSummary(getPreferenceScreen().findPreference("rustext_mode"));

		getPreferenceScreen().findPreference("background_color").setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				new ColorPickerDialog(EditPreferences.this, PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).getInt("background_color", Color.parseColor("#EEEEEE")), new OnColorPickerListener() {
					
					@Override
					public void onColorPicked(int color) {
						PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).edit().putInt("background_color", color).commit();
					}
				}).show();
				return true;
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		setResult(RESULT_OK);
		updateSummary(findPreference(key));
	}

	/**
	 * Sets list preference summary to its current value
	 * @param p Preference in question
	 */
	private void updateSummary(Preference p) {
		if (p instanceof ListPreference) {
			p.setSummary(((ListPreference) p).getEntry());
		}
	}

}