package bibcs.android.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.util.MonthDisplayHelper;
import bibcs.android.model.CalendarItem;
import bibcs.android.model.CalendarItem.Types;

/**
 * Class which helps to handle calendar views
 * @author Semeniuk A.D.
 * 
 */
public class CalendarCalculation {

	/**
	 * Generates list of calendar item which can be used in adapter
	 * @param year Year in question
	 * @param month Month in question
	 * @param info Comma-separated info for current month
	 * @return Generated list of calendars
	 */
	public static List<CalendarItem> getCalendarList(int year, int month, String info) {
		List<CalendarItem> result = new ArrayList<CalendarItem>();
		MonthDisplayHelper helper = new MonthDisplayHelper(year, month, Calendar.MONDAY);

		int number = 0;
		int[] offset = helper.getDigitsForRow(0);
		for (int i = 0; i < helper.getOffset(); i++) {
			result.add(new CalendarItem(Types.PREVIOUS, String.valueOf(offset[i])));
			number++;
		}

		String[] information = info.split(",");
		if (information.length != helper.getNumberOfDaysInMonth()) { //Error in file info.csv
			information = new String[helper.getNumberOfDaysInMonth()];
			for (int i = 0; i < information.length; i++) {
				information[i] = "00";
			}
		}
		for (int i = 1; i <= helper.getNumberOfDaysInMonth(); i++) {
			String ci = information[i - 1];
			result.add(new CalendarItem(ci.charAt(0) == '1' ? Types.HOLIDAY : Types.REGULAR, String.valueOf(i), Character.digit(ci.charAt(1), 10)));
			number++;
		}

		for (int i = 0; i < 42 - number; i++) {
			result.add(new CalendarItem(Types.NEXT, String.valueOf(i + 1)));
		}

		return result;
	}

	/**
	 * If current day exceeds maximum number of days in current month then returns
	 * fixed day
	 * @param year Current year
	 * @param month Current month
	 * @param day Current day
	 * @return Fixed day
	 */
	public static int fixCurrentDay(int year, int month, int day) {
		MonthDisplayHelper helper = new MonthDisplayHelper(year, month, Calendar.MONDAY);
		return Math.max(1, Math.min(day, helper.getNumberOfDaysInMonth()));
	}

}
