package bibcs.android.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import bibcs.android.model.Location;
import bibcs.android.model.LocationFilter;
import bibcs.android.model.Readings;
import bibcs.android.model.SearchCode;
import bibcs.android.transfer.DatabaseAdapter;

public class CalculationLogic {

	/**
	 * Generates complete regular expression pattern for input validation
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @return Generated pattern
	 */
	public static Pattern generateValidationPattern(DatabaseAdapter adapter) {
		StringBuilder pChapBlank = new StringBuilder().append('(').append("\\.?").append(')'); //No chapters

		StringBuilder pVerseOne = new StringBuilder().append('(').append("[0-9]{1,3}").append(')'); //One verse
		StringBuilder pVerseInt = new StringBuilder().append('(').append(pVerseOne).append("\\-").append(pVerseOne).append(')'); //Verse interval
		StringBuilder pVerseOneOrInt = new StringBuilder().append('(').append(pVerseOne).append('|').append(pVerseInt).append(')'); //Either one verse or verse interval
		StringBuilder pVerseCombo = new StringBuilder().append('(').append(" *: *").append(pVerseOneOrInt).append("(,").append(pVerseOneOrInt)
				.append(")*").append(')'); //Combination of verses and verse intervals
		StringBuilder pVerseOneSpecial = new StringBuilder().append('(').append(" *: *").append(pVerseOne).append(')'); //One verse (special)

		StringBuilder pChapOneSimp = new StringBuilder().append('(').append(" *[0-9]{1,3}").append(')'); //One chapter (simple)
		StringBuilder pChapOneExt = new StringBuilder().append('(').append(" *[0-9]{1,3}").append(pVerseCombo).append(')'); //One chapter (extended)
		StringBuilder pChapOneSimpOrExt = new StringBuilder().append('(').append(pChapOneSimp).append('|').append(pChapOneExt).append(')'); //One chapter (simple or extended)
		StringBuilder pChapOneSpecial = new StringBuilder().append('(').append(" *[0-9]{1,3}").append(pVerseOneSpecial).append(')'); //One chapter (special)

		StringBuilder pChapIntSimp = new StringBuilder().append('(').append(pChapOneSimp).append(" * \\- ").append(pChapOneSimp).append(')'); //Chapter interval (simple)
		StringBuilder pChapIntSpecial = new StringBuilder().append('(').append(pChapOneSpecial).append(" * \\- ").append(pChapOneSpecial).append(')'); //Chapter interval (special)
		StringBuilder pChapIntSimpOrSpecial = new StringBuilder().append('(').append(pChapIntSimp).append('|').append(pChapIntSpecial).append(')'); //Chapter interval (simple or special)

		StringBuilder pChapOneOrInt = new StringBuilder().append('(').append(pChapOneSimpOrExt).append('|').append(pChapIntSimpOrSpecial).append(')'); //Either one chapter (simple or extended) or chapter interval (simple or special)
		StringBuilder pChapCombo = new StringBuilder().append('(').append("[\\. ]").append(pChapOneOrInt).append("( *,  *").append(pChapOneOrInt)
				.append(")*").append(')'); //Combination of chapters and chapter intervals

		StringBuilder pBook = new StringBuilder().append("( *").append(generateBookShortNamesRegExp(adapter)).append('(').append(pChapBlank).append('|')
				.append(pChapCombo).append(")?").append(" *;)+");
		return Pattern.compile(pBook.toString());
	}

	/**
	 * Transforms Search code that contains zero-values into a list of non-zero
	 * valued search codes
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param code Search code
	 * @return List of respective search codes
	 */
	private static List<SearchCode> transformCode(DatabaseAdapter adapter, SearchCode code) {
		List<SearchCode> result = new ArrayList<SearchCode>();
		if (code.getBook() == 0) { //All available books
			return result;
		}
		if (code.getBook() < 1 || code.getBook() > 77) { //Book index out of bounds
			return result;
		}
		if (code.getChapter() == 0) { //All chapters of the specific book
			for (int j = 1; j <= adapter.selectBookSize(code.getBook()); j++) {
				for (int k = 1; k <= adapter.selectChapterSize(code.getBook(), j); k++) {
					result.add(new SearchCode(code.getBook(), j, k));
				}
			}
			return result;
		}
		if (code.getChapter() < 1 || code.getChapter() > adapter.selectBookSize(code.getBook())) { //Chapter index out of bounds
			return result;
		}
		if (code.getLine() == 0) { //All lines of the specific chapter
			for (int k = 1; k <= adapter.selectChapterSize(code.getBook(), code.getChapter()); k++) {
				result.add(new SearchCode(code.getBook(), code.getChapter(), k));
			}
			return result;
		}
		if (code.getLine() < 1 || code.getLine() > adapter.selectChapterSize(code.getBook(), code.getChapter())) { //Line index out of bounds
			return result;
		}
		result.add(new SearchCode(code.getBook(), code.getChapter(), code.getLine()));
		return result;
	}

	/**
	 * Transforms search codes with zero values into search codes interval
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param code1 Interval beginning
	 * @param code2 Interval end
	 * @return List of search codes
	 */
	private static List<SearchCode> transformCodeInterval(DatabaseAdapter adapter, SearchCode code1, SearchCode code2) {
		List<SearchCode> result = new ArrayList<SearchCode>();
		if (code1.getBook() == 0 || code2.getBook() == 0 || code1.getBook() != code2.getBook()) { //Book not specified
			return result;
		}
		if (code1.getBook() < 1 || code1.getBook() > 77 || code2.getBook() < 1 || code2.getBook() > 77) { //Book index is out of bounds
			return result;
		}
		if (code1.getChapter() == 0 || code2.getChapter() == 0) { //Chapter not specified
			return result;
		}
		if (code1.getLine() == 0 && code2.getLine() == 0) { //Chapter interval
			for (int cInd = code1.getChapter(); cInd <= code2.getChapter(); cInd++) {
				result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(code1.getBook(), cInd, 0)));
			}
			return result;
		}
		switch (code1.compareTo(code2)) {
			case 1: //First code exceeds second code
				return result;
			case 0: //First code equals to second code
				result.addAll(CalculationLogic.transformCode(adapter, code1));
				return result;
			case -1:
				if (code1.getChapter() > adapter.selectBookSize(code1.getBook()) || code1.getChapter() < 1) { //Chapter index is out of bounds
					return result;
				}
				int cInd = code1.getChapter();
				int lInd = code1.getLine();
				while (true) {
					result.addAll(transformCode(adapter, new SearchCode(code1.getBook(), cInd, lInd)));
					lInd++;
					if (lInd > adapter.selectChapterSize(code1.getBook(), cInd)) { //Line index exceds line count
						lInd = 1;
						cInd++;
						if (cInd > adapter.selectBookSize(code1.getBook())) { //Chapter index exceeds chapter count
							break;
						}
					}
					if (cInd > code2.getChapter() || (cInd == code2.getChapter() && lInd > code2.getLine())) { //Interval end reached
						break;
					}
				}
				break;
		}
		return result;
	}

	/**
	 * Returns list of codes parsed from current substring
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param subInput Substring to parse
	 * @return List of search codes
	 */
	private static List<SearchCode> parseSearchSubString(DatabaseAdapter adapter, String subInput) {
		List<SearchCode> result = new ArrayList<SearchCode>();
		final String letters1 = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
		final String letters2 = "abcdefghijklmnopqrstuvwxyz";
		final String numbers = "0123456789";
		String bookName = "";
		String chapName = "";
		String lineName = "";
		int book = -1;
		int chap1 = -1;
		int chap2 = -1;
		int line1 = -1;
		int line2 = -1;
		/**
		 * 0 - beginning of text (no book specified yet)
		 * 1 - entering book name
		 * 2 - finished entering book name
		 * 3 - entering first chapter number
		 * 4 - finished entering first chapter number
		 * 5 - found '-' after first chapter number (chapter interval)
		 * 6 - entering second chapter number
		 * 7 - finished entering second chapter number
		 * 8 - found ':' (preparing to fill line number)
		 * 9 - entering first verse number
		 * 10 - entered first verse number
		 * 11 - found ',' after entering verse number (not sure what's up next)
		 * 12 - found '-' after first verse name (verse interval)
		 * 13 - entering second verse number
		 * 14 - finished entering second verse number
		 * 15 - found ',' after entering verse number (not sure what's up next)
		 * 16 - found '-' after first verse name (extended interval)
		 * 17 - entering second chapter number of extended interval
		 * 18 - finished entering second chapter number of extended interval
		 * 19 - found ':' after second chapter number of extended interval
		 * 20 - entering second verse number of extended interval
		 * 21 - finished entering second verse number of extended interval
		 * 22 - found ',' after extended interval
		 */
		int mode = 0;
		for (char c : (subInput.toLowerCase() + ";").toCharArray()) {
			if (c == ' ') { //Space
				if (mode == 1) { //Entering book name
					mode = 2;
					book = adapter.selectBookOrdByShortName(bookName);
				} else if (mode == 3) { //Entering first chapter number
					mode = 4;
					chap1 = Integer.valueOf(chapName);
				} else if (mode == 6) { //Entering second chapter number
					mode = 7;
					chap2 = Integer.valueOf(chapName);
				} else if (mode == 9) { //Entering first verse number
					mode = 10;
					line1 = Integer.valueOf(lineName);
				} else if (mode == 11) { //Entered comma after first verse number
					mode = 2;
					result.addAll(transformCode(adapter, new SearchCode(book, chap1, line1)));
				} else if (mode == 13) { //Entering second verse interval
					mode = 14;
					line2 = Integer.valueOf(lineName);
				} else if (mode == 15) { //Entered comma after second verse number
					mode = 2;
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap1, line2)));
				} else if (mode == 17) { //Entering second chapter number of extended interval
					mode = 18;
					chap2 = Integer.valueOf(chapName);
				} else if (mode == 20) { //Entering second verse number of extended interval
					mode = 21;
					line2 = Integer.valueOf(lineName);
				} else if (mode == 22) { //Found comma after extended interval
					mode = 2;
				}
			} else if (letters1.indexOf(c) != -1 || letters2.indexOf(c) != -1) { //Letter
				if (mode == 0) { //Beginning of text
					mode = 1;
					bookName = "";
					bookName += c;
				} else if (mode == 1) { //Entering book name
					bookName += c;
				}
			} else if (numbers.indexOf(c) != -1) { //Number
				if (mode == 0) { //Beginning of text
					mode = 1;
					bookName = "";
					bookName += c;
				} else if (mode == 2) { //Entered book name
					mode = 3;
					chapName = "";
					chapName += c;
				} else if (mode == 3) { //Entering first chapter number
					chapName += c;
				} else if (mode == 5) { //Second part of chapter interval
					mode = 6;
					chapName = "";
					chapName += c;
				} else if (mode == 6) { //Entering second chapter interval
					chapName += c;
				} else if (mode == 8) { //Prepared to enter verse number
					mode = 9;
					lineName = "";
					lineName += c;
				} else if (mode == 9) { //Entering first verse number
					lineName += c;
				} else if (mode == 11) { //Comma after first verse number
					mode = 9;
					lineName = "";
					lineName += c;
				} else if (mode == 12) { //Second part of verse interval
					mode = 13;
					lineName = "";
					lineName += c;
				} else if (mode == 13) { //Entering second verse number
					lineName += c;
				} else if (mode == 15) { //Comma after second verse number
					mode = 9;
					lineName = "";
					lineName += c;
				} else if (mode == 16) { //Second part of extended verse interval
					mode = 17;
					chapName = "";
					chapName += c;
				} else if (mode == 17) { //Entering second chapter number of extended interval
					chapName += c;
				} else if (mode == 19) { //Prepared to enter second verse number of extended interval
					mode = 20;
					lineName = "";
					lineName += c;
				} else if (mode == 20) { //Entering second verse number of extended interval
					lineName += c;
				}
			} else if (c == '.') { //Dot
				if (mode == 1) {//Entering book name
					mode = 2;
					book = adapter.selectBookOrdByShortName(bookName);
				}
			} else if (c == ',') { //Comma
				if (mode == 3) { //Entering first chapter number
					mode = 2;
					chap1 = Integer.valueOf(chapName);
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, chap1, 0)));
				} else if (mode == 4) { //Entered first chapter number
					mode = 2;
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, chap1, 0)));
				} else if (mode == 6) { //Entering second chapter number
					mode = 2;
					chap2 = Integer.valueOf(chapName);
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, 0), new SearchCode(book, chap2, 0)));
				} else if (mode == 7) { //Entered second chapter number
					mode = 2;
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, 0), new SearchCode(book, chap2, 0)));
				} else if (mode == 9) { //Entering first verse number
					mode = 11;
					line1 = Integer.valueOf(lineName);
					result.addAll(transformCode(adapter, new SearchCode(book, chap1, line1)));
				} else if (mode == 10) { //Entered first verse number
					mode = 11;
					result.addAll(transformCode(adapter, new SearchCode(book, chap1, line1)));
				} else if (mode == 13) { //Entering second verse number
					mode = 15;
					line2 = Integer.valueOf(lineName);
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap1, line2)));
				} else if (mode == 20) { //Entering second verse number of extended interval
					mode = 22;
					line2 = Integer.valueOf(lineName);
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap2, line2)));
				} else if (mode == 21) { //Entered second verse number of extended interval
					mode = 22;
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap2, line2)));
				}
			} else if (c == '-') { //Interval divisor
				if (mode == 4) { //Entered first chapter number
					mode = 5;
				} else if (mode == 9) { //Entering first verse number
					line1 = Integer.valueOf(lineName);
					mode = 12;
				} else if (mode == 10) { //Entered first verse number
					mode = 16;
				}
			} else if (c == ':') { //Verse divisor
				if (mode == 3) { //Entering first chapter number
					mode = 8;
					chap1 = Integer.valueOf(chapName);
				} else if (mode == 4) { //Entered first chapter number
					mode = 8;
				} else if (mode == 17) { //Entering second chapter number of extended interval
					mode = 19;
					chap2 = Integer.valueOf(chapName);
				} else if (mode == 18) { //Entered second chapter number of extended interval
					mode = 19;
				}
			} else if (c == ';') { //End of the text
				if (mode == 1) { //Entering book name
					book = adapter.selectBookOrdByShortName(bookName);
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, 0, 0)));
				} else if (mode == 2) { //Entered book name
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, 0, 0)));
				} else if (mode == 3) { //Entering first chapter number
					chap1 = Integer.valueOf(chapName);
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, chap1, 0)));
				} else if (mode == 4) { //Entered first chapter number
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, chap1, 0)));
				} else if (mode == 6) { //Entering second chapter number
					chap2 = Integer.valueOf(chapName);
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, 0), new SearchCode(book, chap2, 0)));
				} else if (mode == 7) { //Entered second chapter number
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, 0), new SearchCode(book, chap2, 0)));
				} else if (mode == 9) { //Entering first verse number
					line1 = Integer.valueOf(lineName);
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, chap1, line1)));
				} else if (mode == 10) { //Entered first verse number
					result.addAll(CalculationLogic.transformCode(adapter, new SearchCode(book, chap1, line1)));
				} else if (mode == 13) { //Entering second verse number
					line2 = Integer.valueOf(lineName);
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap1, line2)));
				} else if (mode == 14) { //Entered second verse number
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap1, line2)));
				} else if (mode == 20) { //Entering second verse number of extended interval
					line2 = Integer.valueOf(lineName);
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap2, line2)));
				} else if (mode == 21) { //Entered second verse number of extended interval
					result.addAll(CalculationLogic.transformCodeInterval(adapter, new SearchCode(book, chap1, line1), new SearchCode(book, chap2, line2)));
				}
			}
		}
		return result;
	}
	
	/**
	 * Returns list of codes parsed from the given string
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param input String to parse
	 * @return List of search codes
	 */
	public static Location parseSearchString(DatabaseAdapter adapter, String input) {
		Location result = null;
		List<SearchCode> codes = new ArrayList<SearchCode>();
		for (String part : input.split(";")) {
			codes.addAll(parseSearchSubString(adapter, part));
		}
		Collections.sort(codes);
		if (codes.size() <= 0) { //No codes found
			return null;
		}
		result = new Location(codes.get(0).getBook(), codes.get(0).getChapter());
		result.setTitle(generateTabTitle(adapter, result.getBook(), result.getChapter()));
		result.setFilter(new LocationFilter(codes));
		return result;
	}

	/**
	 * Constructs the part of general RegExp pattern which defines the book name
	 * options
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @return Regular expressions pattern
	 */
	private static String generateBookShortNamesRegExp(DatabaseAdapter adapter) {
		StringBuilder result = new StringBuilder();
		result.append('(');
		boolean f2 = true;
		for (String sn : adapter.selectShortNames()) {
			if (!f2) {
				result.append('|');
			}
			result.append(sn);
			f2 = false;
		}
		result.append(')');
		return result.toString().toLowerCase();
	}

	/**
	 * Generates text for specified readings index
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param source Source readings
	 * @return Generated index
	 */
	public static String generateReadingsIndexText(DatabaseAdapter adapter, Readings source) {
		StringBuilder result = new StringBuilder();
		int i = 0;
		int stage = 0; //0 - nothing entered
		int curBook = 0;
		int curChap = 0;

		// --- Building String ---
		while (i < source.getLength()) {
			switch (stage) {
				case 0: //Nothing entered
					curBook = source.getIndex(i).book;
					result.append(adapter.selectBookShortestName(curBook));
					result.append(". ");
					stage = 1; //Book name entered
					break;
				case 1: //Book name entered
					curChap = source.getIndex(i).chapter;
					result.append(curChap);
					stage = 2; //First chapter number entered
					i++; //Advance
					break;
				case 2: //First chapter number entered
					if (source.getIndex(i).book == curBook) {
						curChap = source.getIndex(i).chapter;
						stage = 3;
						i++; //Advance
					} else { //Another book started
						result.append("; ");
						stage = 0;
					}
					break;
				case 3: //First chapter number entered (but has one intermediate chapter)
					if (source.getIndex(i).book == curBook) {
						curChap = source.getIndex(i).chapter;
						stage = 4;
						i++; //Advance
					} else { //Another book started
						result.append(", ");
						result.append(curChap);
						result.append("; ");
						stage = 0;
					}
					break;
				case 4: //First chapter number entered (but has more than one intermediate chapters)
					if (source.getIndex(i).book == curBook) {
						curChap = source.getIndex(i).chapter;
						stage = 4;
						i++; //Advance
					} else { //Another book started
						result.append(" - ");
						result.append(curChap);
						result.append("; ");
						stage = 0;
					}
					break;
			}
		}

		// --- Finalization ---
		switch (stage) {
			case 3: //First chapter number entered (but has one intermediate chapter)
				result.append(", ");
				result.append(curChap);
				break;
			case 4: //First chapter number entered (but has more than one intermediate chapters)
				result.append(" - ");
				result.append(curChap);
				break;
		}

		return result.toString();
	}

	/**
	 * Generates list of indexes for specified readings
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param source Source readings to begin from
	 * @param offset Chapter offset
	 * @return Readings with calculated list
	 */
	public static Readings generateReadingsIndexList(DatabaseAdapter adapter, Readings source, int offset) {
		int book = source.getIndex(0).book;
		int bookSize = adapter.selectBookSize(book);
		int chap = source.getIndex(0).chapter;
		source.clearIndex();

		// --- Moving to offset ---
		while (offset != 0) {
			if (offset > 0) { //Going forward
				chap++;
				if (chap > bookSize) { //Book size exceeded
					book++;
					if (book == 55) {
						book = 51;
					} else if (book == 77) {
						book = 55;
					}
					bookSize = adapter.selectBookSize(book);
					chap = 1;
				}
				offset--;
			} else if (offset < 0) { //Going backward
				chap--;
				if (chap < 1) { //Book size exceeded
					book--;
					if (book == 54) {
						book = 76;
					} else if (book == 50) {
						book = 54;
					}
					bookSize = adapter.selectBookSize(book);
					chap = bookSize;
				}
				offset++;
			}
		}

		// --- Adding index ---
		for (int i = 0; i < source.getLength(); i++) {
			if (chap > bookSize) { //Book size exceeded
				book++;
				if (book == 55) {
					book = 51;
				} else if (book == 77) {
					book = 55;
				}
				bookSize = adapter.selectBookSize(book);
				chap = 1;
			}
			source.addIndex(book, chap);
			chap++;
		}
		return source;
	}

	/**
	 * Generates chapter link name applicable for use as tab title
	 * @param adapter DatabaseAdapter used to connect to local database
	 * @param book Book order
	 * @param book Chapter order
	 * @return Generated name
	 */
	public static String generateTabTitle(DatabaseAdapter adapter, int book, int chapter) {
		return String.format("%s.%d", adapter.selectBookShortestName(book), chapter);
	}

}
