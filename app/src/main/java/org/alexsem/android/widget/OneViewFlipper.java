package org.alexsem.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * ViewFlipper which can contain unlimited amount of view while using as low
 * memory as possible. Each view is generated right before being shown while
 * invisible views are being disposed of
 * @author Semeniuk A.D.
 */
public class OneViewFlipper extends DoubleViewFlipper {

	private int currentPage = -1;
	private int totalPages = 0;
	private boolean cyclic = true;
	private OnViewChangeListener listener;

	public OneViewFlipper(Context context) {
		super(context);
	}

	public OneViewFlipper(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Adds views to the view flipper
	 * @param totalPages Number of pages within flipper
	 * @param startingPage Page to start viewing from
	 */
	public void setData(int totalPages, int startingPage) {
		this.resetData();
		this.totalPages = totalPages;
		this.currentPage = startingPage;
		if (totalPages > 0) {
			generateView();
		}
	}

	/**
	 * Sets the listener which processes view changing routines
	 * @param l Listener to set
	 */
	public void setOnViewChangeListener(OnViewChangeListener l) {
		this.listener = l;
	}

	/**
	 * Deletes all views from the view flipper
	 */
	public void resetData() {
		this.totalPages = 0;
		this.currentPage = -1;
		this.removeAllViews();
	}

	public boolean isCyclic() {
		return cyclic;
	}

	public void setCyclic(boolean cyclic) {
		this.cyclic = cyclic;
	}

	@Override
	public int getDisplayedChild() {
		return currentPage;
	}
	
	public int getPageCount() {
		return totalPages;
	}

	public void gotoSpecific(int index) {
		if (index < 0 || index > totalPages - 1) {
			return;
		}
		if (index >= currentPage) {
			currentPage = index;
			generateView();
			super.showNext();
		} else {
			currentPage = index;
			generateView();
			super.showPrevious();
		}
		this.removeViewAt(0);
	}

	@Override
	public void showNext() {
		if (currentPage >= totalPages - 1) { //Last view in list
			if (cyclic) {
				currentPage = 0;
			} else {
				return;
			}
		} else { //Can move
			currentPage++;
		}
		generateView();
		super.showNext();
		this.removeViewAt(0);
	}

	@Override
	public void showPrevious() {
		if (currentPage <= 0) { //First view in list
			if (cyclic) {
				currentPage = totalPages - 1;
			} else {
				return;
			}
		} else { //Can move 
			currentPage--;
		}
		generateView();
		super.showPrevious();
		this.removeViewAt(0);
	}

	@Override
	@Deprecated
	public void showNext(int step) {
		if (currentPage >= totalPages - step) { //Can not move
			if (cyclic) {
				currentPage -= (totalPages + 1 - step);
			} else {
				return;
			}
		} else { //Can move
			currentPage += step;
		}
		generateView();
		super.showNext();
		this.removeViewAt(0);
	}

	@Override
	@Deprecated
	public void showPrevious(int step) {
		if (currentPage <= step - 1) { //Can not move
			if (cyclic) {
				currentPage += (totalPages + 1 - step);
			} else {
				return;
			}
		} else { //Can move 
			currentPage -= step;
		}
		generateView();
		super.showPrevious();
		this.removeViewAt(0);
	}

	@Override
	public void showFirst() {
		this.currentPage = 0;
		generateView();
		super.showPrevious();
		this.removeViewAt(0);
	}

	@Override
	public void showNextWithoutAnimation() {
		if (currentPage >= totalPages - 1) { //Last view in list
			if (cyclic) {
				currentPage = 0;
			} else {
				return;
			}
		} else { //Can move
			currentPage++;
		}
		generateView();
		super.showNextWithoutAnimation();
		this.removeViewAt(0);
	}

	@Override
	public void showPreviousWithoutAnimation() {
		if (currentPage <= 0) { //First view in list
			if (cyclic) {
				currentPage = totalPages - 1;
			} else {
				return;
			}
		} else { //Can move 
			currentPage--;
		}
		generateView();
		super.showPreviousWithoutAnimation();
		this.removeViewAt(0);
	}

	/**
	 * Add new view to the view list
	 */
	private void generateView() {
		if (listener != null) {
			this.addView(listener.viewChanged(this.currentPage));
		}
	}

	/**
	 * Interface used to listen to ViewChanged events
	 * @author Semeniuk A.D.
	 */
	public interface OnViewChangeListener {
		public View viewChanged(Integer index);
	}

}
