package org.alexsem.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.ViewFlipper;

/**
 * Own implementation of ViewFlipper class
 * @author Semeniuk A.D.
 */
public class DoubleViewFlipper extends ViewFlipper {

	private Animation nextOldAnimation;
	private Animation nextNewAnimation;
	private Animation prevOldAnimation;
	private Animation prevNewAnimation;

	public void setNextOldAnimation(Animation nextOld) {
		this.nextOldAnimation = nextOld;
	}

	public void setNextNewAnimation(Animation nextNew) {
		this.nextNewAnimation = nextNew;
	}

	public void setPrevOldAnimation(Animation prevOld) {
		this.prevOldAnimation = prevOld;
	}

	public void setPrevNewAnimation(Animation prevNew) {
		this.prevNewAnimation = prevNew;
	}

	public DoubleViewFlipper(Context context) {
		super(context);
	}

	public DoubleViewFlipper(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void showNext() {
		setInAnimation(nextNewAnimation);
		setOutAnimation(nextOldAnimation);
		super.showNext();
	}

	public void showNext(int step) {
		setInAnimation(nextNewAnimation);
		setOutAnimation(nextOldAnimation);
		super.setDisplayedChild(super.getDisplayedChild() + step);
	}

	@Override
	public void showPrevious() {
		setInAnimation(prevNewAnimation);
		setOutAnimation(prevOldAnimation);
		super.showPrevious();
	}

	public void showPrevious(int step) {
		setInAnimation(prevNewAnimation);
		setOutAnimation(prevOldAnimation);
		super.setDisplayedChild(super.getDisplayedChild() - step);
	}

	public void showFirst() {
		setInAnimation(prevNewAnimation);
		setOutAnimation(prevOldAnimation);
		super.setDisplayedChild(0);
	}

	public void showNextWithoutAnimation() {
		setInAnimation(null);
		setOutAnimation(null);
		super.showNext();
	}

	public void showPreviousWithoutAnimation() {
		setInAnimation(null);
		setOutAnimation(null);
		super.showPrevious();
	}

	@Override
	protected void onDetachedFromWindow() {
		try {
			super.onDetachedFromWindow();
		} catch (IllegalArgumentException e) {
			stopFlipping();
		}
	}
}