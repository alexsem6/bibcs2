package org.alexsem.android.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;

public class HidingPanel extends FrameLayout {
	private final int HIDDEN_HEIGHT = 0;
	private int duration = 250;
	private Button button;
	private boolean opened = true;
	OnStateChangedListener listener = null;

	/**
	 * Initializes necessary components
	 * @param context Context
	 */
	private void init(Context context) {
		button = new Button(context);
		button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, HIDDEN_HEIGHT));
		button.setVisibility(View.GONE);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HidingPanel.this.show();
			}
		});
		this.addView(button);
	}

	public HidingPanel(Context context) {
		super(context);
		init(context);
	}

	public HidingPanel(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public HidingPanel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	/**
	 * Shows panel by performing the animation
	 */
	public void show() {
		if (!opened) { //Panel is hidden
			if (listener != null) {
				listener.beforeChange(false);
			}
			changeStateToShown();
			this.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			TranslateAnimation anim = new TranslateAnimation(0F, 0F, this.getMeasuredHeight() - HIDDEN_HEIGHT, 0F);
			anim.setDuration(duration);
			anim.setInterpolator(new AccelerateInterpolator(1.0f));
			this.startAnimation(anim);
			opened = true;
		}
	}

	/**
	 * Hides panel by performing the animation
	 */
	public void hide() {
		if (opened) { //If panel is shown
			if (listener != null) {
				listener.beforeChange(true);
			}
			TranslateAnimation anim = new TranslateAnimation(0F, 0F, 0F, this.getHeight() - HIDDEN_HEIGHT);
			anim.setAnimationListener(hideAnimationListener);
			anim.setDuration(duration);
			anim.setInterpolator(new AccelerateInterpolator(1.0f));
			this.startAnimation(anim);
			opened = false;
		}
	}

	/**
	 * Hides panel without animation
	 */
	public void showWithoutAnimation() {
		if (!opened) { //If panel is hidden
			if (listener != null) {
				listener.beforeChange(false);
			}
			changeStateToShown();
			opened = true;
		}
	}

	/**
	 * Hides panel without animation
	 */
	public void hideWithoutAnimation() {
		if (opened) { //If panel is shown
			if (listener != null) {
				listener.beforeChange(true);
			}
			changeStateToHidden();
			opened = false;
		}
	}

	/**
	 * Hide panel if it visible or show if it is hidden
	 */
	public void toggle() {
		if (opened) {
			hide();
		} else {
			show();
		}
	}

	/**
	 * Shows whether the panel is currently shown
	 * @return true if shown, false if hidden
	 */
	public boolean isOpened() {
		return opened;
	}

	/**
	 * Sets animation end listener for state change events
	 * @param l Listener object to assign
	 */
	public void setOnStateChangeListener(OnStateChangedListener l) {
		this.listener = l;
	}

	/**
	 * Sets the "speed" of hiding/showing animations
	 * @param duration Animation duration in milliseconds
	 */
	public void setAnimationDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * Sets drawable which is shown when panel is hidden
	 * @param background Drawable resource for the background
	 */
	public void setHiddenBackground(Drawable background) {
		this.button.setBackgroundDrawable(background);
	}

	/**
	 * Performs all necessary layout and view transformations when panel is about
	 * to be hidden
	 */
	private void changeStateToHidden() {
		ViewGroup.LayoutParams params = HidingPanel.this.getLayoutParams();
		params.height = HIDDEN_HEIGHT;
		HidingPanel.this.setLayoutParams(params);
		for (int i = 0; i < HidingPanel.this.getChildCount(); i++) {
			HidingPanel.this.getChildAt(i).setVisibility(View.INVISIBLE);
		}
		button.setVisibility(View.VISIBLE);
		if (listener != null) {
			listener.afterChange(false);
		}
	}

	/**
	 * Performs all necessary layout and view transformations when panel is about
	 * to be shown
	 */
	private void changeStateToShown() {
		ViewGroup.LayoutParams params = this.getLayoutParams();
		for (int i = 0; i < HidingPanel.this.getChildCount(); i++) {
			HidingPanel.this.getChildAt(i).setVisibility(View.VISIBLE);
		}
		button.setVisibility(View.GONE);
		this.bringToFront();
		params.height = LayoutParams.WRAP_CONTENT;
		this.setLayoutParams(params);
		if (listener != null) {
			listener.afterChange(true);
		}
	}

	/**
	 * Class used to trigger events after hiding animation happened
	 * @author Semeniuk A.D.
	 */
	private class HideAnimationListener implements AnimationListener {
		@Override
		public void onAnimationStart(Animation animation) {
			//Do nothing
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			//Do nothing
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			changeStateToHidden();
		}
	}
	private HideAnimationListener hideAnimationListener = new HideAnimationListener(); 

	/**
	 * Interface that is used for animation end callbacks
	 * @author Semeniuk A.D.
	 */
	public interface OnStateChangedListener {
		/**
		 * Raised when panel has just been shown
		 * @param opened Defines whether panel was opened before state was changed
		 */
		public void beforeChange(boolean opened);

		/**
		 * Raised when panel has just been hidden
		 * @param opened Defines whether panel becomes opened after state is changed
		 */
		public void afterChange(boolean opened);
	}

}
