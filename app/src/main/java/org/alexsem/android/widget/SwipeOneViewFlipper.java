package org.alexsem.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

public class SwipeOneViewFlipper extends OneViewFlipper {
	private GestureDetector detector;
	private OnTouchListener listener;

	public SwipeOneViewFlipper(Context context) {
		super(context);
		init(context);
	}

	public SwipeOneViewFlipper(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	/**
	 * Initializes current instance after creation
	 * @param context Context object
	 */
	private void init(Context context) {
		this.detector = new GestureDetector(context, new SwipeGestureListener());
		listener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return detector.onTouchEvent(event);
			}
		};
		this.setOnTouchListener(listener);
	}

	/**
	 * Returns registered onTouchListener (to pass it to focusable objects within
	 * the contents of ViewFlipper)
	 * @return OnTouchListener object
	 */
	public OnTouchListener getOnTouchListener() {
		return this.listener;
	}

	/**
	 * Gesture detector for fling effects
	 * @author Semeniuk A.D.
	 */
	private class SwipeGestureListener extends SimpleOnGestureListener {
		private static final int SWIPE_MIN_DISTANCE = 100;
		private static final int SWIPE_MAX_OFF_PATH = 250;
		private static final int SWIPE_THRESHOLD_VELOCITY = 200;

		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			if (SwipeOneViewFlipper.this.getChildCount() == 0 || e1 == null || e2 == null) {
				return false;
			}
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				SwipeOneViewFlipper.this.showNext();
			} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				SwipeOneViewFlipper.this.showPrevious();
			}
			return super.onFling(e1, e2, velocityX, velocityY);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return detector.onTouchEvent(event);
	}

}
