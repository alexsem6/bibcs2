package org.alexsem.android.adapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

public abstract class Tree2SimpleAdapter<T, Q> extends BaseAdapter {
	public final Map<T, Section> sections = new LinkedHashMap<T, Section>();
	public final ArrayAdapter<T> headers;

	/**
	 * Constructor
	 * @param context Context
	 * @param parentLayout Parent layout resource identifier
	 * @param childLayout Child layout resource identifier
	 * @param data Map structure which contains adapter data
	 */
	public Tree2SimpleAdapter(Context context, int parentLayout, int childLayout, Map<T, List<Q>> data) {
		headers = new ArrayAdapter<T>(context, parentLayout, new ArrayList<T>(data.keySet()));
		for (T section : data.keySet()) {
			this.sections.put(section, new Section(new ArrayAdapter<Q>(context, childLayout, data.get(section))));
		}
	}

	@Override
	public final int getCount() {
		int total = 0;
		for (Section section : this.sections.values()) {
			total += section.size();
		}
		return total;
	}

	/**
	 * Finds parent index of the specified parent object
	 * @param parent Parent object to search
	 * @return index of parent or -1 if not found
	 */
	public int getIndexOfParent(T parent) {
		return headers.getPosition(parent);
	}

	/**
	 * Finds parent index of the specified child object
	 * @param child Child object to search
	 * @return parent index of child or -1 if not found
	 */
	public int getParentIndexOfChild(Q child) {
		int result = -1;
		for (T sect : this.sections.keySet()) {
			result++;
			if (sections.get(sect).adapter.getPosition(child) > -1) {
				break;
			}
		}
		return result;
	}

	/**
	 * Finds child index of the specified parent object
	 * @param child Child object to search
	 * @return child index of parent or -1 if not found
	 */
	public int getChildIndexOfChild(Q child) {
		for (T sect : this.sections.keySet()) {
			int index = (sections.get(sect).adapter.getPosition(child));
			if (index > -1) {
				return index;
			}
		}
		return -1;
	}

	/**
	 * Returns parent object of the specified child object
	 * @param child Child object whose parent to look for
	 * @return Parent object
	 */
	public T getParentOfChild(Q child) {
		for (T sect : this.sections.keySet()) {
			if (sections.get(sect).adapter.getPosition(child) > -1) {
				return sect;
			}
		}
		return null;
	}

	/**
	 * Returns absolute position of the specified parent
	 * @param parentIndex Index of parent
	 * @return position of parent
	 */
	public int getParentPosition(int parentIndex) {
		if (parentIndex < 0 || parentIndex > headers.getCount()) {
			return -1;
		}
		int result = 0;
		for (int i = 0; i < parentIndex; i++) {
			result += sections.get(headers.getItem(i)).size();
		}
		return result;
	}

	@Override
	public final Object getItem(int position) {
		for (T sect : this.sections.keySet()) {
			Section section = sections.get(sect);
			int size = section.size();

			if (position == 0) { //Header found
				return sect;
			} else if (position < size) { //Section data found
				return section.adapter.getItem(position - 1);
			} else { //Next section
				position -= size;
			}
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public abstract View getParentView(int parentIndex, View convertView, ViewGroup parent);

	public abstract View getChildView(int parentIndex, int childIndex, View convertView, ViewGroup parent);

	@Override
	public final View getView(int position, View convertView, ViewGroup parent) {
		int sectIndex = 0;
		for (T sect : this.sections.keySet()) {
			Section section = sections.get(sect);
			int size = section.size();
			if (position == 0) { //Header found
				return getParentView(sectIndex, convertView, parent);
			} else if (position < size) { //Section data found
				return getChildView(sectIndex, position - 1, convertView, parent);
			} else { //Next section
				position -= size;
				sectIndex++;
			}
		}
		return null;
	}

	/**
	 * Returns header of the specified section
	 * @param parentIndex Number of section
	 * @return section header
	 */
	public T getParentItem(int parentIndex) {
		return headers.getItem(parentIndex);
	}

	/**
	 * Returns specified item of the specified section
	 * @param parentIndex Number of section
	 * @param childIndex Number of item within section
	 * @return item data
	 */
	public Q getChildItem(int parentIndex, int childIndex) {
		return sections.get(headers.getItem(parentIndex)).adapter.getItem(childIndex);
	}

	/**
	 * Shows whether specified section is expanded
	 * @param parentIndex Number of section
	 * @return true if section is expanded, false otherwise
	 */
	public boolean isParentExpanded(int parentIndex) {
		return sections.get(headers.getItem(parentIndex)).expanded;
	}

	/**
	 * Expands section if it is collapsed and collapses if it is expanded
	 * @param parentIndex Number of section
	 */
	public void toggleParentExpansion(int parentIndex) {
		sections.get(headers.getItem(parentIndex)).toggleExpansion();
		notifyDataSetChanged();
	}

	/**
	 * Expands all sections of the tree
	 */
	public void expandAll() {
		for (Section section : sections.values()) {
			section.expanded = true;
		}
		notifyDataSetChanged();
	}

	/**
	 * Collapses all sections of the tree
	 */
	public void collapseAll() {
		for (Section section : sections.values()) {
			section.expanded = false;
		}
		notifyDataSetChanged();
	}

	/**
	 * Special class that wraps around section list to provide selection and
	 * expanding functionality
	 * @author Semeniuk A.D.
	 */
	private class Section {
		private ArrayAdapter<Q> adapter;
		private boolean expanded;

		public Section(ArrayAdapter<Q> adapter) {
			this.adapter = adapter;
			this.expanded = false;
		}

		public int size() {
			return 1 + (this.expanded ? this.adapter.getCount() : 0);
		}

		public void toggleExpansion() {
			this.expanded = !this.expanded;
		}

	}

}
